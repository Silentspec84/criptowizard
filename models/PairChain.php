<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\Json;

/**
 * @property integer $id
 * @property integer $exchange_id
 * @property integer $type
 * @property string $pattern
 * @property array $base
 * @property array $dir
 * @property string $first_currency
 * @property integer $status
 * @property integer $date_modified
 * @property integer $date_created
 */
class PairChain extends ActiveRecord
{
    public const TYPE_TRIPPLES = 3;
    public const TYPE_QUADRIPPLES = 4;

    public const STATUS_OFF = 0;
    public const STATUS_ON = 1;

    public static function tableName()
    {
        return 'pair_chains';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->date_created = time();
        }
        $this->date_modified = time();
        $this->base = Json::encode($this->base);
        $this->dir = Json::encode($this->dir);

        return parent::beforeValidate();
    }

    public function afterFind()
    {
        $this->base = Json::decode($this->base);
        $this->dir = Json::decode($this->dir);
        return parent::afterFind();
    }

    public static function saveChains($data)
    {
        $fields_array = [
            'exchange_id',
            'type',
            'pattern',
            'base',
            'dir',
            'first_currency',
            'status',
            'date_created',
            'date_modified',
        ];

        try {
            Yii::$app->db->createCommand()->batchInsert(self::tableName(), $fields_array, $data)->execute();
        } catch (Exception $e) {
            echo $e;
            return false;
        }
        return true;
    }

    public static function getPairChains($exchange_id) {
        $chains = self::find()->where(['exchange_id' => $exchange_id])->all();
        $result = [];

        foreach ($chains as $chain) {
            if ($chain->type === self::TYPE_TRIPPLES) {
                $result['tripples'][] = [
                    'pattern' => $chain->pattern,
                    'base' => $chain->base,
                    'dir' => $chain->dir,
                    'first_currency' => $chain->first_pair
                ];
            }
            if ($chain->type === self::TYPE_QUADRIPPLES) {
                $result['quadripples'][] = [
                    'pattern' => $chain->pattern,
                    'base' => $chain->base,
                    'dir' => $chain->dir,
                    'first_currency' => $chain->first_pair
                ];
            }
        }

        return $result;
    }
}
<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $name
 * @property integer $date_modified
 * @property integer $date_created
 */
class TopCurrency extends ActiveRecord
{

    public static function tableName()
    {
        return 'top_currencies';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->date_created = time();
        }
        $this->date_modified = time();

        return parent::beforeValidate();
    }
}
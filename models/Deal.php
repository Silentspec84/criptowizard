<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\Json;

/**
 * @property integer $id
 * @property string $hash
 * @property integer $exchange_id
 * @property string $strategy
 * @property integer $type
 * @property string $deal
 * @property string $pattern
 * @property float $profit_perc
 * @property float $profit
 * @property string $log
 * @property integer $date_modified
 * @property integer $date_created
 */
class Deal extends ActiveRecord
{

    public const TYPE_REAL = 10;

    public const TYPE_TEST = 20;

    public static function tableName()
    {
        return 'deals';
    }

    public function rules()
    {
        return [
            [['deal', 'pattern', 'hash'], 'required'],
            [['date_modified', 'date_created', 'exchange_id', 'type'], 'integer'],
            [['profit_perc', 'profit'], 'double'],
            [['deal', 'pattern', 'hash', 'log', 'strategy'], 'string'],
        ];
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->date_created = time();
        }
        $this->date_modified = time();
        $this->deal = Json::encode($this->deal);
        $this->pattern = Json::encode($this->pattern);

        return parent::beforeValidate();
    }

    public function afterFind()
    {
        $this->deal = Json::decode($this->deal);
        $this->pattern = Json::decode($this->pattern);

        return parent::afterFind();
    }

    public static function saveDeals($data)
    {
        $fields_array = [
            'hash',
            'exchange_id',
            'strategy',
            'type',
            'deal',
            'pattern',
            'profit_perc',
            'profit',
            'date_modified',
            'date_created',
        ];

        try {
            $rows = Yii::$app->db->createCommand()->batchInsert(self::tableName(), $fields_array, $data)->execute();
        } catch (Exception $e) {
            return $e;
        }
        return $rows;
    }
}

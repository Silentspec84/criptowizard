<?php

namespace app\models\queries;

use app\models\User;
use yii\db\ActiveQuery;

class UserQuery extends ActiveQuery
{
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function one($db = null)
    {
        return parent::one($db);
    }

    public function orderByLoginName($sort = SORT_DESC)
    {
        return $this->orderBy(['login_name' => $sort]);
    }

    public function orderByEmail($sort = SORT_DESC)
    {
        return $this->orderBy(['email' => $sort]);
    }

    public function orderByDateCreated($sort = SORT_DESC)
    {
        return $this->orderBy(['date_created' => $sort]);
    }

    /**
     * Жадная загрузка всех аккаунтов пользователя
     * @return UserQuery
     */
    public function withAccounts()
    {
        return $this->with('userAccounts');
    }

    public function active()
    {
        return $this->andWhere(['status' => User::STATUS_ACTIVE]);
    }

    public function inactive()
    {
        return $this->andWhere(['status' => User::STATUS_INACTIVE]);
    }

    public function unconfirmed()
    {
        return $this->andWhere(['status' => User::STATUS_UNCONFIRMED]);
    }

    public function admin()
    {
        return $this->andWhere(['role' => User::ROLE_ADMIN]);
    }

    public function moderator()
    {
        return $this->andWhere(['role' => User::ROLE_MODERATOR]);
    }

    public function user()
    {
        return $this->andWhere(['role' => User::ROLE_USER]);
    }
}
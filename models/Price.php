<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\Json;

/**
 * @property integer $id
 * @property integer $instrument_id
 * @property string $ask
 * @property string $bid
 * @property integer $date_modified
 * @property integer $date_created
 */
class Price extends ActiveRecord
{
    public static function tableName()
    {
        return 'prices';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->date_created = time();
        }
        $this->date_modified = time();
        $this->ask = Json::encode($this->ask);
        $this->bid = Json::encode($this->bid);

        return parent::beforeValidate();
    }

    public function afterFind()
    {
        $this->ask = Json::decode($this->ask);
        $this->bid = Json::decode($this->bid);
        return parent::afterFind();
    }

    public static function savePrices($data)
    {
        $fields_array = [
            'instrument_id',
            'ask',
            'bid',
            'date_modified',
            'date_created',
        ];

        $rows = 0;
        try {
            $rows = Yii::$app->db->createCommand()->batchInsert(self::tableName(), $fields_array, $data)->execute();
        } catch (Exception $e) {
            return $e;
        }
        return $rows;
    }
}
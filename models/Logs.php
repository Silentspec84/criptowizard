<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $exchange_id
 * @property string $procedure
 * @property string $message
 * @property integer $date_modified
 * @property integer $date_created
 */
class Logs extends ActiveRecord
{

    public const LOAD_PAIRS = 'LoadPairs';
    public const CALC_CHAINS = 'CalculateChains';
    public const CALC_BALANCES = 'CalculateBalances';

    public static function tableName()
    {
        return 'logs';
    }

    public function rules()
    {
        return [
            [['exchange_id', 'procedure'], 'required'],
            [['id', 'date_modified', 'date_created'], 'integer'],
            [['procedure', 'message'], 'string'],
        ];
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->date_created = time();
        }
        $this->date_modified = time();

        return parent::beforeValidate();
    }

    public static function getLogger($exchange_id, $procedure, $first_start = false)
    {
        $logger = Logs::find()->where(['exchange_id' => $exchange_id, 'procedure' => $procedure])->one();
        if (!$logger) {
            $logger = new Logs();
        }
        if ($first_start) {
            $logger->message = '';
        }
        $logger->exchange_id = $exchange_id;
        $logger->procedure = $procedure;
        return $logger;
    }

    public function saveMessage(string $message)
    {
        $this->message = $this->message . $message . '<br>';
        $this->save();
    }

    public function collectMessage(string $message)
    {
        $this->message = $this->message . $message . '<br>';
    }

    public function clearMessage()
    {
        $this->message = '';
    }
}
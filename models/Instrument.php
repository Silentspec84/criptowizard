<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;

/**
 * @property integer $id
 * @property integer $exchange_id
 * @property string $name
 * @property integer $status        //включать или нет пару в построение цепочек
 * @property integer $date_modified
 * @property integer $date_created
 */
class Instrument extends ActiveRecord
{

    public const STATUS_OFF = 0;
    public const STATUS_ON = 1;

    public static function tableName()
    {
        return 'instruments';
    }

    public function rules()
    {
        return [
            [['name', 'exchange_id'], 'required'],
            [['id', 'exchange_id', 'date_modified', 'date_created', 'status'], 'integer'],
            [['name'], 'string'],
        ];
    }

    public static function saveInstrumentsSettings($data)
    {
        $fields_array = [
            'exchange_id',
            'name',
            'status',
            'date_created',
            'date_modified',
        ];

        try {
            Yii::$app->db->createCommand()->batchInsert(self::tableName(), $fields_array, $data)->execute();
        } catch (Exception $e) {
            return false;
        }
        return true;
    }
}
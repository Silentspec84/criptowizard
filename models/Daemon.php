<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $exchange_id
 * @property string $command
 * @property string $strategy
 * @property integer $job_type
 * @property integer $enabled
 * @property integer $timer
 * @property string $log
 * @property integer $date_modified
 * @property integer $date_created
 * @property integer $date_last_connected
 */
class Daemon extends ActiveRecord
{
    public const TYPE_TEST = 10;
    public const TYPE_TRADE = 20;

    public const ENABLED_ON = 1;
    public const ENABLED_OFF = 0;

    public static $daemon_types = [
        self::TYPE_TEST => 'Тестовый',
        self::TYPE_TRADE => 'Реал',
    ];

    public static function tableName()
    {
        return 'daemons';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->date_created = time();
        }

        return parent::beforeValidate();
    }
}
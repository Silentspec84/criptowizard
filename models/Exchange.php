<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property string $name
 * @property string $class
 * @property string $descr
 * @property string $img
 * @property string $link
 * @property float $commission
 * @property integer $date_modified
 * @property integer $date_created
 */
class Exchange extends ActiveRecord
{

    public static function tableName()
    {
        return 'exchanges';
    }

    public function rules()
    {
        return [
            [['name', 'class'], 'required'],
            [['id', 'date_modified', 'date_created'], 'integer'],
            [['commission'], 'double'],
            [['name', 'class', 'descr', 'img', 'link'], 'string'],
        ];
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->date_created = time();
        }
        $this->date_modified = time();

        return parent::beforeValidate();
    }
}
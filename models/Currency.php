<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $exchange_id
 * @property string $currency
 * @property float $min_volume
 * @property float $deposit
 * @property integer $status
 * @property integer $date_modified
 * @property integer $date_created
 * @property integer $date_last_connected
 */
class Currency extends ActiveRecord
{
    public const STATUS_OFF = 0;
    public const STATUS_ON = 1;

    public static function tableName()
    {
        return 'currencies';
    }

    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->date_created = time();
        }
        $this->date_modified = time();

        return parent::beforeValidate();
    }

    public static function getBalancesForTest()
    {
        return  [
            'EXM' => ['min_volume' => 10000],
            'USD' => ['min_volume' => 1],
            'EUR' => ['min_volume' => 1],
            'RUB' => ['min_volume' => 60],
            'PLN' => ['min_volume' => 5],
            'TRY' => ['min_volume' => 10],
            'UAH' => ['min_volume' => 30],
            'BTC' => ['min_volume' => 0.0001],
            'LTC' => ['min_volume' => 0.017],
            'DOGE' => ['min_volume' => 300],
            'DASH' => ['min_volume' => 0.001],
            'ETH' => ['min_volume' => 0.005],
            'WAVES' => ['min_volume' => 1],
            'ZEC' => ['min_volume' => 0.03],
            'USDT' => ['min_volume' => 1],
            'XMR' => ['min_volume' => 0.015],
            'XRP' => ['min_volume' => 4],
            'KICK' => ['min_volume' => 5000],
            'ETC' => ['min_volume' => 0.2],
            'BCH' => ['min_volume' => 0.004],
            'BTG' => ['min_volume' => 0.13],
            'EOS' => ['min_volume' => 0.3],
            'BTCZ' => ['min_volume' => 9000],
            'DXT' => ['min_volume' => 1000],
            'XLM' => ['min_volume' => 15],
            'MNX' => ['min_volume' => 15],
            'OMG' => ['min_volume' => 1],
            'TRX' => ['min_volume' => 50],
            'ADA' => ['min_volume' => 30],
            'INK' => ['min_volume' => 300],
            'NEO' => ['min_volume' => 0.08],
            'GAS' => ['min_volume' => 0.8],
            'ZRX' => ['min_volume' => 4],
            'GNT' => ['min_volume' => 20],
            'GUSD' => ['min_volume' => 1],
            'LSK' => ['min_volume' => 1],
            'XEM' => ['min_volume' => 25],
            'SMART' => ['min_volume' => 330],
            'QTUM' => ['min_volume' => 0.5],
            'HB' => ['min_volume' => 330],
            'DAI' => ['min_volume' => 1],
            'MKR' => ['min_volume' => 0.001],
            'MNC' => ['min_volume' => 50],
            'PTI' => ['min_volume' => 200],
            'ATMCASH' => ['min_volume' => 6000],
            'ETZ' => ['min_volume' => 40],
            'USDC' => ['min_volume' => 1],
            'ROOBEE' => ['min_volume' => 250],
            'DCR' => ['min_volume' => 0.05],
            'XTZ' => ['min_volume' => 1],
            'ZAG' => ['min_volume' => 0.2],
            'BTT' => ['min_volume' => 2500],
            'VLX' => ['min_volume' => 30],
        ];
    }
}
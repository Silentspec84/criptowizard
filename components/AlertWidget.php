<?php

namespace app\components;

use yii\base\Widget;

/**
 * Class AlertWidget
 * Виджет алертов.
 * Наиболее частое применение - сообщение об ошибке.
 * Пример: AlertWidget::widget(['message' => 'Какая-то ошибочка', 'type' => AlertWidget::TYPE_DANDER, 'close_button' => true]);
 * @package app\components
 */
class AlertWidget extends Widget
{

    const TYPE_DANDER = 'danger';
    const TYPE_WARNING = 'warning';
    const TYPE_SUCCESS = 'success';
    const TYPE_PRIMARY = 'primary';
    const TYPE_SECONDARY = 'secondary';
    const TYPE_LIGHT = 'light';

    /** @var string */
    public $message;
    /** @var string */
    public $type;
    /** @var bool */
    public $close_button;

    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = 'Hello World';
        }
    }

    public function run()
    {
        $message =  '
        <div class="alert alert-'.$this->type.' alert-dismissible fade show" role="alert">
            '.$this->message;
        if ($this->close_button) {
            $message .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                         </button>';
        }
        $message .= '</div>';

        return $message;
    }
}
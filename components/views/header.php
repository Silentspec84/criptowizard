<?php

use yii\helpers\Url;
?>


<div class="navbar-custom shadow-sm" style="position: static;">

    <!-- LOGO -->
    <div class="logo-box mt-2">
        <a href="/" class="logo">
            <h2 class="text-light">CriptoWizard</h2>
        </a>
    </div>

    <ul v-show="user.role === $config.ROLE_ADMIN" class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <a href="/exchanges/index" class="nav-link<?php if(Yii::$app->controller->id === 'exchanges'):?> active-link<?php endif;?>">Биржи</a>
        </li>
    </ul>

    <ul v-show="user.role === $config.ROLE_ADMIN" class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <a href="/deals/" class="nav-link<?php if(Yii::$app->controller->id === 'deals'):?> active-link<?php endif;?>">Сделки</a>
        </li>
    </ul>

    <ul v-show="user.role === $config.ROLE_ADMIN" class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li class="dropdown d-none d-lg-block">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                Настройки
                <i class="mdi mdi-chevron-down"></i>
            </a>
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 70px, 0px);">
                <a class="dropdown-item<?php if(Yii::$app->controller->id === 'instruments' && Yii::$app->controller->action === 'index'):?> active<?php endif;?>" href="/instruments/index">Пары</a>
                <a class="dropdown-item<?php if(Yii::$app->controller->id === 'chains' && Yii::$app->controller->action === 'index'):?> active<?php endif;?>" href="/chains/index">Цепочки</a>
                <a class="dropdown-item<?php if(Yii::$app->controller->id === 'balances' && Yii::$app->controller->action === 'index'):?> active<?php endif;?>" href="/balances/index">Балансы</a>
                <a class="dropdown-item<?php if(Yii::$app->controller->id === 'daemons' && Yii::$app->controller->action === 'index'):?> active<?php endif;?>" href="/daemons/index">Демоны</a>
            </div>
        </li>
    </ul>

    <ul v-show="user.role === $config.ROLE_ADMIN" class="list-unstyled topnav-menu topnav-menu-left m-0">
        <li>
            <a href="/strategy-builder/" class="nav-link<?php if(Yii::$app->controller->id === 'strategy-builder'):?> active-link<?php endif;?>">Конструктор стратегий</a>
        </li>
    </ul>

    <ul v-show="is_guest" class="list-unstyled topnav-menu topnav-menu-left m-0 pull-right">
        <li>
            <button class="nav-link btn btn-link" data-toggle="modal" data-target="#login" v-on:click="show_modal=true">Вход</button>
        </li>
        <li>
            <button class="nav-link btn btn-link" data-toggle="modal" data-target="#register" v-on:click="show_modal=true">Регистрация</button>
        </li>
    </ul>

    <ul v-show="!is_guest" class="list-unstyled topnav-menu topnav-menu-left m-0 pull-right">
        <li class="dropdown d-none d-lg-block">
            <div class="dropdown mb-2">
                <a class="nav-link text-light dropdown-toggle" href="#"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="/images/user-icon.png" class="rounded-circle" style="width: 25px;" alt="">
                    <span class="pro-user-name ml-1">{{user.login_name}}</span>
                    <i class="mdi mdi-chevron-down ml-1"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    <div class="d-flex align-items-center px-4 mb-2">
                        <div class="mr-2">
                            <img :src="user.avatar" class="rounded-circle" style="width: 50px;" alt="">
                        </div>
                        <div>
                            <div>{{user.login_name}}</div>
                            <div class="text-secondary small">
                                {{user.email}}
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item" href="/config/user-profile">Лента</a>
                    <a class="dropdown-item" href="/config/company">Настройки</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/site/logout">Выйти</a>
                </div>
            </div>
        </li>
    </ul>

    <div class="modal fade mt-4" id="login" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Вход</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" v-on:click="closeModal()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-1">
                        <ul class="nav nav-tabs mb-2">
                            <li :class="is_login_auth ? 'active' : ''"><a v-on:click="is_login_auth=true">По логину</a></li>
                            <li :class="!is_login_auth ? 'active' : ''"><a v-on:click="is_login_auth=false">По почте</a></li>
                        </ul>

                        <div v-show="is_login_auth" class="row mr-2">
                            <div class="container-fluid">
                                <div class="form-group">
                                    <label class="text-nowrap mb-0 pull-left">Логин</label>
                                    <input class="form-control" name="username" id="username" type="text"
                                           data-toggle="tooltip" data-placement="right" data-original-title="" title=""
                                           v-model="login_form.username"
                                           v-bind:data-original-title="getError('username')"
                                           v-bind:class="getErrorClass('username')"
                                    >
                                </div>
                                <div class="form-group">
                                    <label class="text-nowrap mb-0 pull-left">Пароль</label>
                                    <input class="form-control" name="password" id="password" type="password"
                                           data-toggle="tooltip" data-placement="right" data-original-title="" title=""
                                           v-model="login_form.password"
                                           v-bind:data-original-title="getError('password')"
                                           v-bind:class="getErrorClass('password')"
                                    >
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label ml-0 text-nowrap pull-left mr-1" for="checkbox">Запомнить меня</label>
                                    <input type="checkbox" class="form-check-input" id="checkbox" v-model="login_form.remember">
                                </div>
                            </div>
                        </div>

                        <div v-show="!is_login_auth" class="row mr-2">
                            <div class="container-fluid">
                                <div class="form-group">
                                    <label class="text-nowrap mb-0 pull-left">Почта</label>
                                    <input class="form-control" name="email" id="email" type="email"
                                           data-toggle="tooltip" data-placement="right" data-original-title="" title=""
                                           v-model="login_form.email"
                                           v-bind:data-original-title="getError('email')"
                                           v-bind:class="getErrorClass('email')"
                                    >
                                </div>
                                <div class="form-group">
                                    <label class="text-nowrap mb-0 pull-left">Пароль</label>
                                    <input class="form-control" name="password" id="password" type="password"
                                           data-toggle="tooltip" data-placement="right" data-original-title="" title=""
                                           v-model="login_form.password"
                                           v-bind:data-original-title="getError('password')"
                                           v-bind:class="getErrorClass('password')"
                                    >
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label ml-0 text-nowrap pull-left mr-1" for="checkbox">Запомнить меня</label>
                                    <input type="checkbox" class="form-check-input" id="checkbox" v-model="login_form.remember">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-primary btn-block" data-dismiss="modal" v-on:click="login()">Вход</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="register" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title">Регистрация</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" v-on:click="closeModal()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="small text-nowrap mb-0 pull-left">Логин</label>
                                <input  type="text" data-toggle="tooltip" data-placement="right" placeholder="Логин" data-original-title="" title=""
                                        class="form-control col-12"
                                        v-bind:data-original-title="getError('login_name')"
                                        v-bind:class="getError('login_name') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                        v-model="register_form.login_name"/>
                            </div>
                            <div class="form-group">
                                <label class="small text-nowrap mb-0 pull-left">email</label>
                                <input  type="text" data-toggle="tooltip" data-placement="right" placeholder="email" data-original-title="" title=""
                                        class="form-control col-12"
                                        v-bind:data-original-title="getError('email')"
                                        v-bind:class="getError('email') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                        v-model="register_form.email"/>
                            </div>
                        </div>

                        <div class="col-6">
                            <div class="form-group">
                                <label class="small text-nowrap mb-0 pull-left">Пароль</label>
                                <input  type="password" data-toggle="tooltip" data-placement="right" placeholder="Пароль" data-original-title="" title=""
                                        class="form-control col-12"
                                        v-bind:data-original-title="getError('password')"
                                        v-bind:class="getError('password') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                        v-model="register_form.password"/>
                            </div>
                            <div class="form-group">
                                <label class="small text-nowrap mb-0 pull-left">Повторите пароль</label>
                                <input  type="password" data-toggle="tooltip" data-placement="right" placeholder="Повторите пароль" data-original-title="" title=""
                                        class="form-control col-12"
                                        v-bind:data-original-title="getError('repeat_password')"
                                        v-bind:class="getError('repeat_password') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                        v-model="register_form.repeat_password"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-secondary btn-sm" v-on:click="closeModal()">Отменить</button>
                    <button  v-on:click="register()" type="button" data-dismiss="modal" class="btn btn-primary btn-sm">
                        Сохранить
                    </button>
                </div>
            </div>
        </div>
    </div>


</div>


"use strict";

document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        el: "#exchanges",
        mixins: [listMixins, validatorMixins],
        data: {
            _csrf: null,
            get_exchanges_link: '/exchanges-api/get-exchanges',
            load_pairs_link: '/exchanges-api/load-pairs',
            load_chains_link: '/exchanges-api/load-chains',
            load_balances_link: '/exchanges-api/load-balances',
            loading: true,
            exchanges: {},
            error_list: {}
        },
        methods: {
            load: function() {
                let self = this;
                return axios.post(this.get_exchanges_link, {_csrf: this._csrf})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.exchanges = response.data.content.exchanges;
                            self.error_list = {};
                        }
                        self.loading = false;
                    });
            },
            saveCommission: function (exchange) {
                exchange.commission = exchange.new_commission;
            },
            loadPairs: function (index) {
                this.createSwal("Идет загрузка данных...", "info", 6000);
                let self = this;
                let id = this.exchanges[index]['id'];
                return axios.post(this.load_pairs_link, {_csrf: this._csrf, id: id})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.error_list = {};
                        }
                        self.closeSwal();
                    });
            },
            loadBalances: function (index) {
                this.createSwal("Идет загрузка данных...", "info", 6000);
                let self = this;
                let id = this.exchanges[index]['id'];
                return axios.post(this.load_balances_link, {_csrf: this._csrf, id: id})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.error_list = {};
                        }
                        self.closeSwal();
                    });
            },
            loadChains: function (index) {
                this.createSwal("Идет загрузка данных...", "info", 6000);
                let self = this;
                let id = this.exchanges[index]['id'];
                return axios.post(this.load_chains_link, {_csrf: this._csrf, id: id})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.error_list = {};
                        }
                        self.closeSwal();
                    });
            }
        },
        mounted: function () {
            this._csrf = this.$refs.config.dataset.csrf;
            this.load();
            let timerId = setInterval(() => this.load(), 30000);
        }

    });
});
"use strict";

document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        el: "#daemons",
        mixins: [listMixins, validatorMixins],
        data: {
            _csrf: null,
            get_daemons_link: '/daemons-api/get-daemons',
            switch_daemon_link: '/daemons-api/switch-daemon',
            set_timer_link: '/daemons-api/set-timer',
            loading: true,
            daemons: {},
            error_list: {}
        },
        methods: {
            load: function() {
                let self = this;
                return axios.post(this.get_daemons_link, {_csrf: this._csrf})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.daemons = response.data.content.daemons;
                            self.error_list = {};
                        }
                        self.loading = false;
                    });
            },
            switchDaemon: function (index) {
                this.createSwal("Идет загрузка данных...", "info", 6000);
                this.daemons[index]['daemon_data']['enabled'] = this.daemons[index]['daemon_data']['enabled'] == 1 ? 0 : 1;
                let self = this;
                let id = this.daemons[index]['daemon_data']['id'];
                return axios.post(this.switch_daemon_link, {_csrf: this._csrf, id: id})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.daemons[index]['daemon_data']['enabled'] = response.data.content.enabled;
                            self.error_list = {};
                        }
                        self.closeSwal();
                    });
            },
            formatDate: function(date) {
                return moment.unix(date).format('DD.MM.YYYY');
            },
            formatDatetime: function(date) {
                return moment.unix(date).format('DD.MM.YYYY HH:mm:ss');
            },
            setTimer: function (index, time) {
                this.createSwal("Идет загрузка данных...", "info", 6000);
                let self = this;
                let id = this.daemons[index]['daemon_data']['id'];
                return axios.post(this.set_timer_link, {_csrf: this._csrf, id: id, timer: time})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.daemons[index]['daemon_data']['timer'] = response.data.content.timer;
                            self.error_list = {};
                        }
                        self.closeSwal();
                    });
            }
        },
        mounted: function () {
            this._csrf = this.$refs.config.dataset.csrf;
            this.load();
            let timerId = setInterval(() => this.load(), 10000);
        }

    });
});
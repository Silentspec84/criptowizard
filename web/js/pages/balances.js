"use strict";

document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        el: "#balances",
        mixins: [listMixins, validatorMixins],
        data: {
            _csrf: null,
            id: 0,
            get_balances_link: '/balances-api/get-balances',
            switch_currency_link: '/balances-api/switch-currency',
            set_min_volume_link: '/balances-api/set-min-volume',
            loading: true,
            balances: {},
            new_min_volume: null,
            error_list: {}
        },
        methods: {
            load: function() {
                this.createSwal("Идет загрузка данных...", "info", 6000);
                let self = this;
                return axios.post(this.get_balances_link, {_csrf: this._csrf, id: this.id})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.balances = response.data.content.balances;
                            self.error_list = {};
                        }
                        self.closeSwal();
                        self.loading = false;
                    });
            },
            saveMinVolume: function (balance, index) {
                this.balances[index].balance_data.min_volume = this.new_min_volume;
                let self = this;
                return axios.post(this.set_min_volume_link, {_csrf: this._csrf, balance: balance})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.error_list = {};
                        }
                        self.closeSwal();
                    });
            },
            switchCurrency: function (index) {
                this.createSwal("Идет загрузка данных...", "info", 6000);
                let self = this;
                let id = this.balances[index]['balance_data']['id'];
                return axios.post(this.switch_currency_link, {_csrf: this._csrf, id: id})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.balances[index]['balance_data']['status'] = response.data.content.status;
                            self.error_list = {};
                        }
                        self.closeSwal();
                    });
            },
        },
        mounted: function () {
            this._csrf = this.$refs.config.dataset.csrf;
            this.id = Number(JSON.parse(this.$refs.config.dataset.id));
            this.load();
        }

    });
});
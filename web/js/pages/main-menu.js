"use strict";

document.addEventListener("DOMContentLoaded", function() {

    new Vue({
        el: "#main_menu",
        mixins: [listMixins, validatorMixins],
        data: {
            _csrf: null,
            user: {
                id: 0,
                role: null,
            },
            is_guest: true,
            is_admin: false,
            is_login_auth: true,
            show_modal: false,
            login_form: {
                username: '',
                email: '',
                login: '',
                password: '',
                remember: false,
            },
            register_form: {
                login_name: null,
                email: null,
                password: null,
                repeat_password: null,
            },
            logout_link: '/site-api/logout',
            login_link: '/site-api/login',
            register_link: '/site-api/register',
            get_menu_data_link: '/site-api/get-menu-data',
            error_list: {},
        },
        methods: {
            load: function() {
                let self = this;
                return axios.post(this.get_menu_data_link, {_csrf: this._csrf})
                    .then(function (response) {
                        if (!response.data.is_success) {
                        } else {
                            self.user = response.data.content.user;
                            if (self.user.id > 0) {
                                self.is_guest = false;
                            }
                            self.error_list = {};
                        }
                    });
            },
            closeModal: function() {
                this.register_form = {
                        login_name: null,
                        email: null,
                        password: null,
                        repeat_password: null
                };
                this.show_modal = false;
                this.error_list = {};
            },
            register: function() {
                if (!this.validateRegisterForm()) {
                    this.createSwal("Невозможно сохранить! Исправьте ошибки в форме", "error", 3000);
                    return;
                }
                let self = this;
                this.createSwal("Идет сохранение...", "info", 60000);
                return axios.post(this.register_link, {_csrf: this._csrf, register_form: this.register_form})
                    .then(function (response) {
                        if (!response.data.is_success) {
                            self.error_list = {};
                            for (let key in response.data.errors[0]) {
                                self.error_list[key] = response.data.errors[0][key];
                            }
                            return this.createSwal("Невозможно сохранить! Исправьте ошибки в форме", "error", 3000);
                        } else {
                            self.error_list = {};
                            self.new_item_form = {};
                            self.closeModal();
                            swal("Регистрация прошла успешно!", "Мы выслали вам письмо на указанный электронный адрес. " +
                                "Для продолжения регистрации пройдите по ссылке из этого письма.", "success");
                        }
                    });
            },
            login: function () {
                if (!this.validateLoginForm()) {
                    this.createSwal("Заполните выделенные красным поля", "error", 3000);
                    return;
                }
                let self = this;
                return axios.post(this.login_link, {_csrf: this._csrf, login_form: this.login_form})
                    .then(function (response) {
                        if (!response.data.is_success) {
                            self.error_list = response.data.errors;
                            self.createSwal("Неверный логин лил пароль. Попробуйте снова", "error", 3000);
                        } else {
                            self.user = response.data.content.user;
                            self.is_guest = false;
                            self.error_list = {};
                        }
                    });
            },
            isNeedValidate: function (field_name, field_value, params) {
                if (field_name === 'username' && this.login_form.login === this.login_form.email) {
                    return false;
                }
                if (field_name === 'email' && this.login_form.login === this.login_form.username) {
                    return false;
                }
                return true;
            },
            validateLoginForm: function() {
                this.error_list = {};
                if (this.login_form.username !== '') {
                    this.login_form.login = this.login_form.username;
                } else {
                    this.login_form.login = this.login_form.email;
                }
                let config = {
                    required: {
                        fields: ['username', 'password', 'email'],
                        message: 'Данное поле обязательно к заполнению!',
                        needValidate: 'isNeedValidate',
                    },
                    email: {
                        fields: ['email'],
                        message: 'Неправильный формат почты',
                        needValidate: 'isNeedValidate',
                    },
                    userName: {
                        fields: ['username'],
                        message: 'Данное поле должно содержать только буквы латинского алфавита и цифры',
                        needValidate: 'isNeedValidate',
                    },
                };
                return this.validateForm(config, this.login_form);
            },
            validateRegisterForm: function() {
                this.error_list = {};

                if (this.register_form.password !== this.register_form.repeat_password) {
                    this.error_list['password'] = 'Введенные пароли различаются!';
                    return false;
                }

                let config = {
                    required: {
                        fields: ['login_name', 'email', 'password', 'repeat_password'],
                        message: 'Данное поле обязательно к заполнению!',
                    },
                    min: {
                        fields: ['login_name', 'password', 'repeat_password', 'email'],
                        min: {login_name: 3, password: 6, repeat_password: 5, email: 5}
                    },
                    max: {
                        fields: ['login_name', 'password', 'repeat_password', 'email'],
                        max: {login_name: 50, password: 50, repeat_password: 50, email: 50}
                    },
                    email: {
                        fields: ['email'],
                        message: 'Неправильный формат почты',
                    },
                    userName: {
                        fields: ['login_name'],
                        message: 'Данное поле должно содержать только буквы латинского алфавита и цифры',
                    },
                };
                return this.validateForm(config, this.register_form);
            }
        },
        mounted: function () {
            this._csrf = this.$refs.config.dataset.csrf;
            this.show_modal = false;
            this.load();
        }

    });
});

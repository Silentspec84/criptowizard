"use strict";

document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        el: "#builder",
        mixins: [listMixins, validatorMixins],
        data: {
            _csrf: null,
            id: 0,
            get_chains_link: '/chains-api/get-chains',
            switch_chain_link: '/chains-api/switch-chain',
            save_xml_link: '/strategy-builder-api/save-xml',
            loading: true,
            workspace: null,
            strategy_name: 'strategy name',
            strategy_file: null,
            current_theme: Blockly.Themes.Classic,
            error_list: {},
            code: null
        },
        methods: {
            load: function() {
                // this.createSwal("Идет загрузка данных...", "info", 6000);
                // let self = this;
                // return axios.post(this.get_chains_link, {_csrf: this._csrf, id: this.id})
                //     .then(function (response) {
                //         if (response.data.is_success) {
                //             self.chains = response.data.content.chains;
                //             self.error_list = {};
                //         }
                //         self.closeSwal();
                //         self.loading = false;
                //     });
            },
            createBlockly: function () {
                this.workspace = Blockly.inject('blocklyDiv',
                    {
                        toolbox: document.getElementById('toolbox'),
                        zoom:
                            {controls: true,
                                wheel: true,
                                startScale: 1.0,
                                maxScale: 3,
                                minScale: 0.3,
                                scaleSpeed: 1.2},
                        grid:
                            {spacing: 20,
                                length: 3,
                                colour: '#ccc',
                                snap: true},
                        trashcan: true
                    });
            },
            getCode: function () {
                this.code = Blockly.PHP.workspaceToCode(this.workspace);
            },
            saveToFile: function () {
                let dom = Blockly.Xml.workspaceToDom(this.workspace);
                const oSerializer = new XMLSerializer();
                const sXML = oSerializer.serializeToString(dom);
                const file = new Blob([sXML], {type: 'text/xml'});
                saveAs(file, this.strategy_name + '.xml');
            },
            loadFromFile: function () {
                let target = document.getElementById("strategy_file");
                let file = target.files[0];
                var reader = new FileReader();
                let workspace = this.workspace;
                reader.onload = function(event) {
                    var contents = event.target.result;
                    var dom = Blockly.Xml.textToDom(contents);
                    Blockly.Xml.clearWorkspaceAndLoadFromXml(dom, workspace);
                };
                reader.onerror = function(event) {
                    this.createSwal("Невозможно загрузить файл! Возможно, файл поврежден!", "error", 3000);
                };
                reader.readAsText(file);
            },
            clearWorkspace: function () {
                this.workspace.clear();
            },
            undoRedo: function (redo) {
                this.workspace.undo(redo);
            },
            setTheme: function () {
                this.workspace.setTheme(this.current_theme);
            },
            getThemes: function () {
                return {
                    classic: {
                        name: 'Классическая',
                        value: Blockly.Themes.Classic
                    },
                    dark: {
                        name: 'Темная',
                        value: Blockly.Themes.Dark
                    },
                    modern: {
                        name: 'Модерн',
                        value: Blockly.Themes.Modern
                    },
                    highcontrast: {
                        name: 'Контрастная',
                        value: Blockly.Themes.HighContrast
                    }
                };
            }
        },
        mounted: function () {
            this.createBlockly();
            this._csrf = this.$refs.config.dataset.csrf;
            this.load();
        }

    });
});

// Blockly.Workspace
// undo
// undo(redo)
//
// Undo or redo the previous action.


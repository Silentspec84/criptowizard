"use strict";

document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        el: "#deals",
        mixins: [listMixins, validatorMixins],
        components: {
            apexchart: VueApexCharts,
        },
        data: {
            _csrf: null,
            get_deals_link: '/deals-api/get-deals',
            loading: true,
            filter_form: false,
            no_results: false,
            sorted_by_profit: 0,
            sorted_by_profit_per_deal: 0,
            sorted_by_currency: 0,
            sorted_by_count: 0,
            deals: {},
            exchanges: {},
            currencies: {},
            deals_filtered: {},
            error_list: {},
            is_loaded: false,
            filters: {
                selected_exchange: null,
                selected_currency: null,
            },
            chartOptions: {
                chart: {
                    sparkline: {
                        enabled: true
                    }
                },
            },
        },
        methods: {
            load: function() {
                this.createSwal("Идет загрузка данных...", "info", 6000);
                let self = this;
                return axios.post(this.get_deals_link, {_csrf: this._csrf})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.deals = response.data.content.deals;
                            self.deals_filtered = response.data.content.deals;
                            self.exchanges = response.data.content.exchanges;
                            self.currencies = response.data.content.currencies;
                            self.error_list = {};
                        }
                        self.closeSwal();
                        self.loading = false;
                        self.is_loaded = true;
                    });
            },
            clearForm: function() {
                this.deals_filtered = Object.assign(this.deals);
                this.filters = {
                    selected_exchange: null,
                    selected_currency: null,
                };
                this.filter_form = false;
                this.no_results = false;
            },
            makeFiltration: function() {
                this.filter_form = true;
                if (this.filters.selected_exchange) {
                    let filtered = {};
                    let count = 0;
                    for (let deal in this.deals) {
                        if (this.deals[deal]['exchange'] === this.filters.selected_exchange) {
                            filtered[count] = this.deals[deal];
                            count++;
                        }
                    }
                    this.deals_filtered = {};
                    this.deals_filtered = Object.assign(filtered);
                }
                if (this.filters.selected_currency) {
                    let filtered = {};
                    let count = 0;
                    for (let deal in this.deals) {
                        if (this.deals[deal]['currency'] === this.filters.selected_currency) {
                            filtered[count] = this.deals[deal];
                            count++;
                        }
                    }
                    this.deals_filtered = {};
                    this.deals_filtered = Object.assign(filtered);
                }
                if (Object.keys(this.deals_filtered).length === 0) {
                    this.no_results = true;
                }
            },
            sortDeals: function(field, order) {
                if (field === 'profit_perc') {
                    this.sorted_by_profit = order;
                    this.sorted_by_profit_per_deal = 0;
                    this.sorted_by_currency = 0;
                    this.sorted_by_count = 0;
                }
                if (field === 'currency') {
                    this.sorted_by_currency = order;
                    this.sorted_by_profit = 0;
                    this.sorted_by_profit_per_deal = 0;
                    this.sorted_by_count = 0;
                }
                if (field === 'count') {
                    this.sorted_by_count = order;
                    this.sorted_by_profit = 0;
                    this.sorted_by_profit_per_deal = 0;
                    this.sorted_by_currency = 0;
                }
                if (field === 'av_deal_profit_perc') {
                    this.sorted_by_count = 0;
                    this.sorted_by_profit = 0;
                    this.sorted_by_profit_per_deal = order;
                    this.sorted_by_currency = 0;
                }
                let sortable = [];
                for (let item in this.deals_filtered) {
                    sortable.push([this.deals_filtered[item][field], this.deals_filtered[item]]);
                }
                sortable.sort((a, b) => {
                    a = a[0];
                    b = b[0];

                    if (!a || !b) {
                        return;
                    }
                    if (isNaN(a) || isNaN(b)) {  // Если один из сортируемых элементов не является числом, то сортируем как строки
                        return (a === b ? 0 : a > b ? 1 : -1) * order; // если сравниваемые значения равны return 0 - не нужно сортировать, return 1 - сортировать по возрастанию return - 1 - сортировать по убыванию
                    }
                    return order === 1 ? (a - b) : (b - a);   // Если строка является числом, то сортируем как числа
                });
                this.deals_filtered = {};
                let count = 0;
                for (let key in sortable) {
                    this.deals_filtered[count] = sortable[key][1];
                    count++;
                }
            }
        },
        mounted: function () {
            this._csrf = this.$refs.config.dataset.csrf;
            this.load();
        }

    });
});

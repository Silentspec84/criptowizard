"use strict";

document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        el: "#chains",
        mixins: [listMixins, validatorMixins],
        data: {
            _csrf: null,
            id: 0,
            get_chains_link: '/chains-api/get-chains',
            switch_chain_link: '/chains-api/switch-chain',
            loading: true,
            chains: {},
            error_list: {}
        },
        methods: {
            load: function() {
                this.createSwal("Идет загрузка данных...", "info", 6000);
                let self = this;
                return axios.post(this.get_chains_link, {_csrf: this._csrf, id: this.id})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.chains = response.data.content.chains;
                            self.error_list = {};
                        }
                        self.closeSwal();
                        self.loading = false;
                    });
            },
            switchChain: function (index) {
                this.createSwal("Идет загрузка данных...", "info", 6000);
                let self = this;
                let id = this.chains[index]['chain_data']['id'];
                return axios.post(this.switch_chain_link, {_csrf: this._csrf, id: id})
                    .then(function (response) {
                        if (response.data.is_success) {
                            self.chains[index]['chain_data']['status'] = response.data.content.status;
                            self.error_list = {};
                        }
                        self.closeSwal();
                    });
            },
        },
        mounted: function () {
            this._csrf = this.$refs.config.dataset.csrf;
            this.id = Number(JSON.parse(this.$refs.config.dataset.id));
            this.load();
        }

    });
});
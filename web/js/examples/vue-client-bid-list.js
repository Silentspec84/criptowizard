"use strict";

document.addEventListener("DOMContentLoaded", function() {

    Vue.component('bid-list', {
        mixins: [listMixins, dateMixins, filtersCheckboxesMixins],
        template: '#bid-list-template',
        data: function () {
            return {
                bids: [],
                query: '',
                show_modal: false,
                archive_candidates: [],
                warning_list: {},
                error_list: {},
                status_list: [],
                status_modes: [],
                filters_checkboxes: null,
            };
        },
        methods: {
            getTableHeads: function() {
                return {
                    id: '№ заявки',
                    from: 'Откуда',
                    to: 'Куда',
                    cargo: 'Груз',
                    manager_name: 'Менеджер',
                    status: 'Статус'
                }
            },
            load() {
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.post('/client-bid-api/list', {
                    _csrf: this.$refs.configs.dataset.csrf,
                    filters_checkboxes: this.filters_checkboxes
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                        if (!response.data.is_success) {
                            this.error_list = {};
                            for (var key in response.data.errors[0]) {
                                this.error_list[key] = response.data.errors[0][key];
                            }
                        } else {
                            this.warning_list = {};
                            this.warning_list = response.data.warnings;
                            this.bids = response.data.content;
                            for(let i = 0; i < this.bids.length; i++) {
                                this.bids[i]['shown'] = true;
                            }
                        }
                    })
                    .catch(e => {
                        this.error_list = {};
                        for (var key in response.data.errors[0]) {
                            this.error_list[key] = response.data.errors[0][key];
                        }
                    });
            },
            getStatusClass: function(status) {
                let css_style = status ? this.status_modes[status] : '';
                let mode = status ? 'badge-' + css_style  : '';
                return 'badge ' + mode + ' text-white small py-1 px-2';
            },
            getStatusTitle: function(status) {
                return status ? this.status_list[status] : '';
            }
        },
        mounted: function () {
            this.load();
            this._csrf = this.$refs.configs.dataset.csrf;
            this.status_list = JSON.parse(this.$refs.configs.dataset.status_list);
            this.status_modes = JSON.parse(this.$refs.configs.dataset.status_modes);
            this.filters_checkboxes = JSON.parse(this.$refs.configs.dataset.filters_checkboxes);
        }
    });


    //index page
    new Vue({
        el: '#bid'
    });
});

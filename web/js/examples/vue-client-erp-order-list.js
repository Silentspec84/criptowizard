"use strict";

document.addEventListener("DOMContentLoaded", function() {

    Vue.component('erp-order-list', {
        mixins: [listMixins, erpMixins, filtersCheckboxesMixins],
        template: '#erp-order-list-template',
    });

    //index page
    new Vue({
        el: '#erp-order'
    });
});

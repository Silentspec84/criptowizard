"use strict";

document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        components: {
            Multiselect: window.VueMultiselect.default
        },
        mixins: [listMixins, searchFilterMixins],
        el: "#client_search_filter",
        data: {
            _csrf: null,
            link_get_filter_keys_and_values: '/client-search-filter-api/get-filter-keys-and-values',
            link_get_options_filter_keys: '/client-search-filter-api/get-options-filter-keys',
            link_get_options_filter_values: '/client-search-filter-api/get-options-filter-values',
            link_add_filter_key: '/client-search-filter-api/add-filter-key',
            link_add_filter_row: '/client-search-filter-api/add-filter-row',
            link_safe_delete_filter_key: '/client-search-filter-api/safe-delete-filter-key',
            link_delete_filter_key: '/client-search-filter-api/delete-filter-key',
            link_delete_filter_row: '/client-search-filter-api/delete-filter-row',
            link_view_search_filter_rows: '/client-search-filter/view-rows',
        },
    });
});
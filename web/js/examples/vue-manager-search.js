"use strict";
document.addEventListener("DOMContentLoaded", function() {
    Vue.component('manager-search', {
        template: '#manager-search-template',
        mixins: [listMixins, searchMixins, calendarSettingsMixin],
        data: function () {
            return {
                border_for_block: [], // массив описывающий где ставить границы блоков (Фрахт, ЖД, Авто) имеет вид: left => [ Номер столбца => Имя столбца , ...] ...
                orders_by_blocks: {}, // порядки столбцов в каждом блоке (поля-ключи, фрахт, жд, авто, итоги)
                cols_checked: [], // отмеченные колонки - их нужно показывать, остальные - скрывать
                are_hidden_columns: true, // если true - будет показано сэндвич-меню, скрывающее/показывающее столбцы
                is_cart_not_empty: false, // есть ли строки в корзине?
                is_registered_client_company: false,
                selected_client_company_data: {},
                // дополнительные параметры, которые не являются поисковыми фильтрами, но влияют на отображение данных в таблице
                additional_params: {
                    weight: '', // если < 24т, отображаем столбец RWAF, иначе - RWBE - не отображается
                    client_company_id: '', // просто сохраяем данные о клиенте в корзину - отображается в ссылке
                    custom_type: '', // если порт - показываем столбец VTTT, иначе - нули вместо него
                    is_home_consignment: 0,
                    is_security: 0, // если отмечена - показываем столбец SECU, иначе - нули вместо него
                    is_danger_cargo: 0,  // если отмечена - показываем столбец "опасный груз" (в филдах такого нет как "да"), иначе - "нет" - не отображается
                    is_special_tariff: 0,  // если отмечена - показываем столбец "специальный груз" (в филдах такого нет как "да"), иначе - "нет" - не отображается
                    client_company_title: '',
                },
                html_elements: { // Переопределение от autoHeightForGridMixins
                    'main': 'tagName',
                    'manager_search': 'id',
                    'search_result': 'id',
                    'grid_table': 'id'
                },
                get_result_table_link: "/manager-search-api/get-result-table",
                save_filters_state_link: "/manager-search-api/menu-filters-state-save",
            };
        },
        mounted: function () {
            this.additional_params.is_home_consignment = parseInt(this.$refs.config.dataset.is_home_consignment);
            this.additional_params.is_danger_cargo = parseInt(this.$refs.config.dataset.is_danger_cargo);
            this.additional_params.is_special_tariff = parseInt(this.$refs.config.dataset.is_special_tariff);
        },
        methods: {
            /** Сетим необщие(для менеджерской и клиентской поисковой форм) свойства при получение поисковой выдачи */
            setNotCommonProperties(response) {
                if (response.data.content.is_cart_not_empty === true) {
                    this.is_cart_not_empty = true;
                }
                this.border_for_block = response.data.content.border_for_block;
                this.orders_by_blocks = response.data.content.orders_by_blocks;
                this.cols_checked = response.data.content.cols_checked;
            },
            /** В менеджерском поиске, если ты выбрал компанию из dadata, нужно прервать метод searchByButton*/
            async isCompanyRegisterNeeded() {
                if (!this.selected_client_company_data.is_company_registered && this.selected_client_company_data.inn) {
                    try {
                        await this.fastCompanyRegister();
                        if (!this.additional_params.client_company_id) {
                            throw 'При выполнениии запроса произошла ошибка';
                        }
                        return true;
                    } catch(error) {
                        this.invalid_inputs_for_highlight.push('client_company');
                        this.highLightInvalidInputs();
                        return true;
                    }
                }

                return false;
            },
            /** Возвращает ссылку для поисковой выдачи по переданному поисковому запросу составленному из get-параметров */
            getSearchLinkBySearchQuery(search_query) {
                return '/manager-search/?client_company_id='
                    + this.additional_params.client_company_id
                    +'&client_company_title='
                    + encodeURIComponent(this.additional_params.client_company_title)
                    + '&' +  search_query.substring(0, search_query.length - 1);
            },
            /** Проверяем что фильтр попадает под значения полей скрытой-строки (которая появляется при нажатии выпадашки на строке)*/
            isMatchFilterValueWithHiddenRow(row, filter_key, filter_value) {
                for (let index_hidden_field in this.rows_result_table[row].additional_params.hidden_row) {
                    if (this.rows_result_table[row].additional_params.hidden_row[index_hidden_field].id == filter_key
                        && this.rows_result_table[row].additional_params.hidden_row[index_hidden_field].field_value == filter_value) {

                        return true;
                    }
                }

                return false;
            },
            /** Регистрация компании из поисковой формы (данные компаний взяты из dadata)*/
            async fastCompanyRegister() {
                const response = await axios.post('/client-api/quick-add-client', {_csrf:this._csrf, company_data:this.selected_client_company_data});
                if(response.data.is_success) {
                    this.additional_params.client_company_id = response.data.content.client_company_id;
                    swal("Зарегистрирвоана компания " + response.data.content.client_company_name, {
                        buttons: false,
                        icon: "success",
                        timer: 3000,
                    }).then(() => {
                        window.location.href = this.getSearchParamsLink(); // делаем редирект на поиск с get-параметрами
                    });
                } else {
                    this.additional_params.client_company_id = 0;
                }
            },
            isRegisteredCompany() {
                return this.is_registered_client_company;
            },
            hasAddInCart() {
                return this.additional_params.client_company_id && this.isRegisteredCompany();
            },
            sortByPrice(direction) {
                this.rows_result_table.sort((a, b) =>{
                    const x = parseInt(a[22]);
                    const y = parseInt(b[22]);

                    let comparison = 0;
                    if (x > y) {
                        comparison = 1;
                    } else if (x < y) {
                        comparison = -1;
                    }

                    if (direction === 'desc') {
                        return comparison * -1;
                    }

                    return comparison;
                });
            },
            saveCart() {
                this.error_list = {};
                this.selected_rows = this.rows_result_table.filter(item => item['checked'] && !item['disabled']);

                axios.post('/manager-search-api/save-cart', {_csrf:this._csrf, rows: this.selected_rows, additional_params: this.additional_params}).then(response => {
                    if (response.data.is_success) {
                        for (let key_selected_row in this.selected_rows) {
                            for (let key in this.rows_result_table) {
                                if (this.selected_rows[key_selected_row].additional_params.hash_row === this.rows_result_table[key].additional_params.hash_row) {
                                    this.$set(this.rows_result_table[key], 'disabled', true);
                                }
                            }
                        }
                        this.is_cart_not_empty = true;
                        swal("Сохранено", {
                            buttons: false,
                            timer: 1500,
                        });
                    } else {
                        this.error_list = {};
                        for (var key in response.data.errors) {
                            this.error_list[key] = response.data.errors[key];
                        }
                    }
                }).catch(e => {
                    this.error_list['all_fields'] = "<div class=\"alert alert-danger alert-dismissible fade show\">" + "Произошла ошибка. Попробуйте повторить позже</div>";
                    this.is_response_error = true;
                });
            },
            /**
             * Возвращает url-encoded строку параметров поиска, полученных из поисковой формы.
             * Это нужно, чтобы из корзины можно было вернуться на поиск с предзаполненной из get-параметров формой
             * @returns {string}
             */
            getSearchUrlParams() {
                let search_query = '?';
                search_query += this.getSearchQueryFromForm();
                search_query += this.getSearchQueryFromAdditionalParams();

                if (search_query === '?') {
                    search_query = '';
                } else {
                    search_query = search_query.substring(0, search_query.length - 1); // удаляем последний символ - &
                }

                return encodeURIComponent(search_query);
            },
            toggleTooltip() {
                $('[data-toggle="tooltip"]').tooltip();
            },
            /** Сохраняет изменения в настройках: какие столбцы показывать */
            columnStateChange(cols_checked) {
                this.cols_checked = cols_checked;
                axios.post('/manager-search-api/column-state-save', {_csrf:this._csrf, cols_checked: cols_checked}).then(response => {
                    if (!response.data.is_success) {
                        this.error_list['all_fields'] = response.data.errors.all_fields;
                        this.is_response_error = true;
                    }
                }).catch(e => {
                    this.error_list['all_fields'] = "<div class=\"alert alert-danger alert-dismissible fade show\">" + "Произошла ошибка. Попробуйте повторить позже</div>";
                    this.is_response_error = true;
                });
            },
        },
    });

    //index page
    new Vue({
        el: '#manager_search'
    });
});

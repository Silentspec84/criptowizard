document.addEventListener("DOMContentLoaded", function(event) {
    vm = new Vue({
        mixins: [listMixins],
        el: "#main",
        data: {
            get_link: '',
            save_field_link: '',
            delete_field_link: '',
            error_list: [],
            data_list: [],
            form: {
                _csrf:  null,
                data_list: [],
            },
        },
        methods: {
            save: function (id, is_key) {
                let field = {
                    id:  id,
                    is_key: is_key
                };
                this.form.data_list.push(field);
                return axios.post(this.save_field_link, this.form)
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = [];
                            for (var key in response.data.errors) {
                                this.error_list[key] = response.data.errors[key];
                            }
                        } else {
                            console.log('Сохранено');
                        }
                        this.form.data_list = [];
                    });
            },

            delete: function (id) {
                this.form.data_list.push(id);
                return axios.post(this.delete_field_link, this.form)
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = [];
                            for (var key in response.data.errors) {
                                this.error_list[key] = response.data.errors[key];
                            }
                        } else {
                            console.log('Сохранено');
                        }
                        this.form.data_list = [];
                    });
            },

            saveFields: function (evt) {
                if ( typeof evt.added !== 'undefined') {
                    this.save(evt.added.element.id, 0);
                }
                else if ( typeof evt.removed !== 'undefined') {
                    console.log('removed from fields');
                }

            },

            saveKeys: function (evt) {
                if ( typeof evt.added !== 'undefined') {
                    this.save(evt.added.element.id, 1);
                }
                else if ( typeof evt.removed !== 'undefined') {
                    console.log('removed from keys');
                }
            },

            saveAll: function (evt) {
                if ( typeof evt.added !== 'undefined') {
                    this.delete(evt.added.element.id);
                }
                else if ( typeof evt.removed !== 'undefined') {
                    console.log('removed from all');
                }
            }
        },

        mounted() {
            this.get_link = this.$refs.config.dataset.get_link;
            this.save_field_link = this.$refs.config.dataset.save_field_link;
            this.delete_field_link = this.$refs.config.dataset.delete_field_link;
            this.onLoad();
        }
    });

});
document.addEventListener("DOMContentLoaded", function() {
    const formula = new Vue ({
        el: '#formula',
        mixins: [listMixins],
        data: {
            _csrf: null,
            formula: '',
            titles_types: null,
            id: '',
            error_list: [],
            data_list: [],
            form: {
                _csrf: null
            },
            item: {
                _csrf: null,
                data: {
                    formula: null,
                    type: null,
                    is_consider_currency: null,
                    is_convert_result_to_rub: null,
                }
            },
            new_item_form: {
                _csrf: this._csrf,
                formula: null,
                type: null,
                is_consider_currency: false,
                is_convert_result_to_rub: false,
            },
            is_hide_formula_list: false,
            add_link: null,
            get_link: null,
            drop_link: null,
            update_link: null
        },

        mounted: async function () {
            let dataset = this.$refs.config.dataset;
            this.form._csrf = dataset.csrf;
            this.add_link = dataset.add_link;
            this._csrf = dataset.csrf;
            this.get_link = dataset.get_link;
            this.drop_link = dataset.drop_link;
            this.update_link = dataset.update_link;
            this.titles_types = JSON.parse(dataset.titles_types);
            const response = await this.onLoad();
            $('[data-toggle="tooltip"]').tooltip();
        },
        methods: {
            updateItem(item) {
                if (item.edit === true) {
                    return axios.post(this.update_link, {_csrf: this.form._csrf, data: item.data})
                        .then(response => {
                            this.error_list = {};
                            if (!response.data.is_success) {
                                for (var key in response.data.errors[0]) {
                                    this.error_list[key] = response.data.errors[0][key];
                                }
                            } else {
                                for (var key in item) {
                                    item[key] = null;
                                }
                                swal("Формула обновлена", {
                                    buttons: false,
                                    timer: 3000,
                                });
                                item.edit = false;
                                this.onLoad();
                            }
                        });
                }
            },
            startEdit: function(item) {
                item.old_data = Object.assign({}, item.data);
                item.edit = true;
            },
            cancelEdit: function(item) {
                item.edit = false;
                this.error_list = {};
                if(item.hasOwnProperty('old_data')) {
                    item.data = item.old_data;
                }
            },
            getTitleByType(type) {
                return this.titles_types[type] ? this.titles_types[type] : '';
            },
            collapseFormulaList() {
                return this.is_hide_formula_list = !this.is_hide_formula_list;
            },
            onClickCheckBoxIsConsiderCurrency(item) {
                item.is_consider_currency = !item.is_consider_currency;
                if (!item.is_consider_currency) {
                    item.is_convert_result_to_rub = false;
                }
            },
            async onLoad() {
                this.form._csrf = this.$refs.config.dataset.csrf;
                await this.load();
            },
            load: async function () {
                const response = await axios.get(this.get_link);
                if (response.data.is_success) {
                    this.data_list = response.data.content;
                } else {
                    this.error_list = {};
                    this.error_list = response.data.errors;
                }
            },
        }
    });
});
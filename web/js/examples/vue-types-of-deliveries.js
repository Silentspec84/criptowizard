"use strict";

document.addEventListener("DOMContentLoaded", function() {
    const types_of_deliveries = new Vue({
        mixins: [listMixins],
        el: "#types-of-deliveries",
        data: {
            _csrf: null,
            default_types_of_deliveries: [], //TypesOfDeliveries::getDefaultTypesOfDeliveries(), массив вида code => title
            types_of_deliveries: [],
            currencies: [],
            new_type_of_delivery: {
                code: null,
                currency_id: null,
            },
            error_list: [],
            warning_list: [],
            get_link: '/types-of-deliveries-api/get-types-of-deliveries',
            get_link_currencies: '/types-of-deliveries-api/get-currencies',
            delete_link: '/types-of-deliveries-api/delete-type-of-delivery',
            add_link: '/types-of-deliveries-api/add-type-of-delivery',
            check_delete_link: '/types-of-deliveries-api/check-delete-type-of-delivery'
        },
        mounted() {
            let dataset = this.$refs.config.dataset;
            this._csrf = dataset.csrf;
            this.default_types_of_deliveries = JSON.parse(dataset.default_types_of_deliveries);
            this.onLoad();
        },
        methods: {
            async onLoad() {
                this.error_list = {};
                this.warning_list = {};
                this.cleanNewTypeOfDelivery();
                await this.getCurrencies();
                if (!this.error_list['all_fields']) {
                    await this.getTypesOfDeliveries();
                }
            },
            async getTypesOfDeliveries() {
                const response = await  axios.get(this.get_link);
                if (!response.data.is_success) {
                    this.error_list = response.data.errors;
                }
                this.types_of_deliveries = response.data.content;
                for (let index_type_of_delivery in this.types_of_deliveries) {
                    this.$set(this.types_of_deliveries[index_type_of_delivery], 'edit', false);
                }
            },
            async getCurrencies() {
                const response = await  axios.get(this.get_link_currencies);
                if (!response.data.is_success) {
                    this.error_list = response.data.errors;
                }
                this.currencies = response.data.content;
            },
            addTypeOfDelivery: function(edit_type_of_delivery = null) {
                let type_of_delivery = edit_type_of_delivery ? edit_type_of_delivery : this.new_type_of_delivery;
                type_of_delivery.currency_id = parseInt(type_of_delivery.currency_id);
                this.createSwal("Идет добавление вида доставки", "info", 300000);
                axios.post(this.add_link, {_csrf:this._csrf, type_of_delivery: type_of_delivery})
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = response.data.errors;
                            this.warning_list = response.data.warnings;
                            this.closeSwal(response);
                            this.createSwal('Ошибка добавление вида доставки!', "error", 2000);
                            return true;

                        }
                        this.onLoad();
                        this.closeSwal(response);
                        this.createSwal("Вид доставки успешно сохранен", "success", 3000);
                    });
            },
            updateTypeOfDelivery: function(index_type_of_delivery) {
                let edit_type_of_delivery = {id: this.types_of_deliveries[index_type_of_delivery].id, code: this.types_of_deliveries[index_type_of_delivery].code, currency_id: this.types_of_deliveries[index_type_of_delivery].currency_id};
                this.addTypeOfDelivery(edit_type_of_delivery);
            },
            deleteTypeOfDelivery: function(id) {
                axios.post(this.delete_link, {_csrf:this._csrf, id: id})
                    .then(response => {
                        if (!response.data.is_success) {
                            this.error_list = response.data.errors;
                        } else {
                            this.onLoad();
                        }
                    });
            },
            cleanNewTypeOfDelivery() {
                this.new_type_of_delivery.code = null;
                this.new_type_of_delivery.currency_id = null;
            },
            isEditableTypeOfDelivery(index_type_of_delivery) {
                return (this.types_of_deliveries[index_type_of_delivery] && this.types_of_deliveries[index_type_of_delivery].edit);
            },
            editTypeOfDelivery: function (index_type_of_delivery) {
                this.$set(this.types_of_deliveries[index_type_of_delivery], 'edit', true);
                this.types_of_deliveries[index_type_of_delivery].old_data = [];
                this.types_of_deliveries[index_type_of_delivery].old_data.currency_id = this.types_of_deliveries[index_type_of_delivery].currency_id;
                this.types_of_deliveries[index_type_of_delivery].old_data.code = this.types_of_deliveries[index_type_of_delivery].code;
            },
            cancelEditTypeOfDelivery: function (index_type_of_delivery) {
                this.$set(this.types_of_deliveries[index_type_of_delivery], 'edit', false);
                this.error_list = {};
                this.types_of_deliveries[index_type_of_delivery].currency_id = this.types_of_deliveries[index_type_of_delivery].old_data.currency_id;
                this.types_of_deliveries[index_type_of_delivery].code = this.types_of_deliveries[index_type_of_delivery].old_data.code;
                this.types_of_deliveries[index_type_of_delivery].old_data = [];
            },
            getErrorText: function(field, attribute_field) {
                $('[data-toggle="tooltip"]').tooltip();

                if (!this.error_list[field]) {
                    return '';
                }
                if (typeof(this.error_list[field]) === 'string') {
                    return this.error_list[field];
                }
                for (let model_field in this.error_list[field]) {
                    if (attribute_field === model_field) {
                        return this.error_list[field][model_field][0] ? this.error_list[field][model_field][0] : this.error_list[field][model_field];
                    }
                }
                return '';
            },
            /** Проверяет можно ли удалить тип доставки */
            checkDeleteTypeOfDelivery: function(id, code) {
                axios.post(this.check_delete_link, {_csrf:this._csrf, code: code})
                    .then(response => {
                        this.warning_list = response.data.warnings;
                        if (!this.warning_list['check_delete']) {
                            this.showDropDialogForTypeOfDelivery(id);
                        }
                    });
            },
            showDropDialogForTypeOfDelivery(id) {
                swal({
                    className: "drop-clients",
                    title: "Удалить вид доставки?",
                    text: "Удалённый вид доставки может повлиять на все настроенные переменные",
                    buttons: {
                        confirm: {
                            text: "Удалить",
                            value: true,
                            visible: true,
                            className: "btn btn-danger",
                            closeModal: true
                        },
                        cancel: {
                            text: "Отмена",
                            value: null,
                            visible: true,
                            className: "btn btn-secondary",
                            closeModal: true,
                        }
                    }
                }).then((willDelete) => {
                    if (willDelete) {
                        this.deleteTypeOfDelivery(id);
                    }
                });

            },
        }
    });
});
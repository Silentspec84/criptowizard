"use strict";

document.addEventListener("DOMContentLoaded", function() {

    Vue.component('bid-list', {
        mixins: [listMixins, dateMixins, filtersCheckboxesMixins],
        template: '#bid-list-template',
        data: function () {
            return {
                bids: [],
                filters_checkboxes: null,
                query: '',
                show_modal: false,
                archive_candidates: [],
                error_list: {},
                warning_list: {},
                status_list: [],
                status_modes: [],
                set_to_archive_link: '/manager-bid-api/archive',
                get_list_data_link: '/manager-bid-api/list'
            };
        },
        methods: {
            archiveModal: function(id) {
                this.archive_candidates = [];
                if(id === undefined) {
                    for(let i = 0; i < this.bids.length; i++) {
                        if(this.bids[i]['checked']) {
                            this.archive_candidates.push(this.bids[i].id);
                        }
                    }
                }
                else {
                    this.archive_candidates.push(id);
                }
                this.show_modal = true;
            },
            archive: function() {
                axios.post(this.set_to_archive_link, {
                    _csrf: this.$refs.configs.dataset.csrf,
                    ids: this.archive_candidates
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                    if(response.data.is_success) {
                        this.load();
                    }
                }).catch(e => {
                    console.warn('archiving error');
                });
                this.show_modal = false;
            },
            getTableHeads: function() {
                return {
                    id: '№ заявки',
                    from: 'Откуда',
                    to: 'Куда',
                    cargo: 'Груз',
                    client_name: 'Клиент',
                    manager_name: 'Менеджер',
                    status: 'Статус',
                    actions: ''
                }
            },
            load: function() {
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.post(this.get_list_data_link, {
                    _csrf: this.$refs.configs.dataset.csrf,
                    filters_checkboxes: this.filters_checkboxes
                }, {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Requested-With': 'XMLHttpRequest'
                    }
                }).then(response => {
                    if (!response.data.is_success) {
                        this.error_list = {};
                        for (var key in response.data.errors[0]) {
                            this.error_list[key] = response.data.errors[0][key];
                        }
                    } else {
                        this.bids = response.data.content;
                        for(let i = 0; i < this.bids.length; i++) {
                            this.bids[i]['checked'] = false;
                            this.bids[i]['shown'] = true;
                        }
                        this.warning_list = response.data.warnings;
                    }
                })
                    .catch(e => {
                        this.error_list = {};
                        for (var key in response.data.errors[0]) {
                            this.error_list[key] = response.data.errors[0][key];
                        }
                    });
            },
            getStatusClass: function(status) {
                let css_style = status ? this.status_modes[status] : '';
                let mode = status ? 'badge-' + css_style  : '';
                return 'badge ' + mode + ' text-white small py-1 px-2';
            },
            getStatusTitle: function(status) {
                return status ? this.status_list[status] : '';
            }
        },
        mounted: function () {
            this.load();
            this._csrf = this.$refs.configs.dataset.csrf;
            this.filters_checkboxes = JSON.parse(this.$refs.configs.dataset.filters_checkboxes);
            this.status_list = JSON.parse(this.$refs.configs.dataset.status_list);
            this.status_modes = JSON.parse(this.$refs.configs.dataset.status_modes);
        }
    });


    //index page
    new Vue({
        el: '#bid'
    });
});

document.addEventListener("DOMContentLoaded", function() {

    Vue.directive('mask', VueMask.VueMaskDirective);

    Vue.component('client-list', {
        mixins: [listMixins, dateMixins, validatorMixins],
        template: '#client-list-template',
        data: function () {
            return {
                client_list: [],
                child_companies_for_owners: [],
                child_companies: [],
                _csrf: null,
                query: null,
                email_is_sending: false,
                show_modal: false,
                form: {
                    title: 'Добавить клиента',
                    is_new: true // если true - добавление нового клиента, иначе - редактирование существующего
                },
                new_client_form: {
                    id: null,
                    client_company_id: null,
                    manager_client_rel: null,
                    inn: null,
                    first_name: null,
                    last_name: null,
                    middle_name: null,
                    email: null,
                    phone: null,
                    client_company_name: null,
                    client_company_business_type: null,
                    order_rate_id: null,
                    manager_user_id: null,
                    contract: null,
                    create_account: false
                },
                status_filter: null,
                status_list: [],
                status_modes: [],
                business_type_filter: null,
                business_type_list: [],
                managers: [],
                rate_list: [],
                base_order_rate_id: null,
                error_list: [],
                warning_list: {},
                get_link: '/client-api/get-client-list',
                get_managers_link: '/client-api/get-managers-for-client',
                get_rate_link: '/rate-api/get-rate-list-for-client',
                add_link: '/client-api/add-client',
                drop_link: '/client-api/drop-client',
                send_mail_link: '/client-api/send-validation-mail',
                change_status_link: '/client-api/change-client-status',
            };
        },
        methods: {
            showModal: function(client) {
                if (client) {
                    if (client.status === this.$config.STATUS_INACTIVE) {
                        swal({
                            text: 'Данный клиент не активен! Для редактирования сначала переведите учетную запись данного клиента в статус "Активный"',
                            value: true,
                            visible: true,
                            className: "",
                            closeModal: true,
                        });
                        return;
                    }
                    this.initForm(client);
                }
                this.form.title = 'Редактировать данные';
                this.form.is_new = false;
                this.show_modal = true;
            },
            initForm: function(client) {
                this.new_client_form.id = client.id;
                this.new_client_form.manager_client_rel = client.manager_client_rel;
                this.new_client_form.status = client.status;
                this.new_client_form.client_company_id = client.client_company_id;
                this.new_client_form.inn = client.inn;
                this.new_client_form.first_name = client.first_name;
                this.new_client_form.last_name = client.last_name;
                this.new_client_form.middle_name = client.middle_name;
                this.new_client_form.email = client.email;
                this.new_client_form.phone = client.phone;
                this.new_client_form.client_company_name = client.full_name;
                this.new_client_form.client_company_business_type = client.business_type;
                this.new_client_form.order_rate_id = client.order_rate_id;
                this.new_client_form.manager_user_id = client.manager_user_id;
                this.new_client_form.contract = client.contract;
                this.new_client_form.create_account = false;
                this.new_client_form.date_created = this.date('DD.MM.YYYY', client.date_created);
            },
            closeModal: function() {
                this.new_client_form = {
                    id: null,
                    client_company_id: null,
                    manager_client_rel: null,
                    inn: null,
                    first_name: null,
                    last_name: null,
                    middle_name: null,
                    email: null,
                    phone: null,
                    client_company_name: null,
                    client_company_business_type: null,
                    order_rate_id: null,
                    manager_user_id: null,
                    contract: null,
                    create_account: false
                };
                setTimeout(() => {
                    this.form.title = 'Добавить клиента';
                }, 200);
                this.form.is_new = true;
                this.show_modal = false;
                this.error_list = {};
            },
            showCheckoboxCreateAccount: function(client) {
                return (!client.status
                    || client.status === this.$config.STATUS_DRAFT
                    || client.status === this.$config.STATUS_NO_ACCOUNT);
            },
            validate: function() {
                this.error_list = {};
                let config = {
                    required: {
                        fields: ['client_company_name', 'first_name',
                            'last_name', 'middle_name', 'phone', 'email', 'inn', 'contract', 'manager_user_id',
                            'client_company_business_type', 'order_rate_id', 'create_account'],
                        message: 'Данное поле обязательно к заполнению!',
                    },
                    isInn: {
                        fields: ['inn'],
                        message: 'Данное поле должно быть целым числом',
                    },
                    min: {
                        fields: ['client_company_name', 'first_name', 'last_name', 'middle_name', 'email', 'contract'],
                        min: 2,
                    },
                    max: {
                        fields: ['client_company_name', 'first_name', 'last_name', 'middle_name', 'email', 'contract'],
                        max: {client_company_name: 100, first_name: 40, last_name: 40, middle_name: 40, email: 80, contract: 10},
                    },
                    email: {
                        fields: ['email'],
                        message: 'Неправильный формат почты',
                    },
                    companyName: {
                        fields: ['client_company_name'],
                        message: 'Поле Организация должно содержать только буквы русского или латинского алфавита, тире и точки. Длина не должна быть меньше 3-х символов и больше 40.',
                    },
                    phone: {
                        fields: ['phone'],
                        message: 'Неправильный формат телефона (+#(###)###-##-##)',
                    },
                    userName: {
                        fields: ['first_name', 'last_name', 'middle_name'],
                        message: 'Поле Контактное лицо должно содержать только буквы русского или латинского алфавита, тире и точки. Длина не должна быть меньше 2-х символов и больше 88.',
                    },
                };

                return this.validateForm(config, this.new_client_form);
            },
            addClient: function() {
                delete this.new_client_form.date_created;
                if (!this.validate()) {
                    this.createSwal("Невозможно сохранить заявку! Исправьте ошибки в форме клиента.", "error", 3000);
                    return;
                }
                let self = this;
                self.new_client_form._csrf = self._csrf;
                this.createSwal("Идет сохранение...", "info", 60000);
                return axios.post(self.add_link, self.new_client_form)
                    .then(function (response) {
                        if (!response.data.is_success) {
                            self.error_list = {};
                            if (typeof response.data.errors[0] === "string") {
                                return self.createSwal(response.data.errors[0], "error", 3000);
                            }
                            for (let key in response.data.errors[0]) {
                                self.error_list[key] = response.data.errors[0][key];
                            }
                            return self.createSwal("Невозможно сохранить заявку! Исправьте ошибки в форме клиента.", "error", 3000);
                        } else {
                            let swal_message = self.new_client_form.id ? "Данные клиента отредактированы" : "Клиент создан";
                            self.closeModal();
                            self.error_list = {};
                            self.new_item_form = {};
                            swal(swal_message, {
                                buttons: false,
                                timer: 1500,
                            });
                        }
                        self.load();
                    });
            },
            load: function() {
                this.createSwal("Идет загрузка данных...", "info", 60000);
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                axios.get('/client-api/get-child-companies-for-owners')
                    .then(({data}) => {
                        this.child_companies_for_owners = data.content.child_companies_for_owners;
                        this.child_companies = data.content.child_companies;

                        axios.post(this.get_link, {
                            _csrf: this.$refs.config.dataset.csrf
                        }, {
                            headers: {
                                'Content-Type': 'application/json',
                                'X-Requested-With': 'XMLHttpRequest'
                            }
                        }).then(response => {
                            if (!response.data.is_success) {
                                this.error_list = {};
                                for (let key in response.data.errors[0]) {
                                    this.error_list[key] = response.data.errors[0][key];
                                }
                                swal.close();
                            } else {
                                this.client_list = response.data.content;
                                this.warning_list = response.data.warnings;
                                swal.close();
                            }
                        }).catch(e => {
                            this.error_list = {};
                            for (let key in response.data.errors[0]) {
                                this.error_list[key] = response.data.errors[0][key];
                            }
                        });
                    });
            },
            getErrorClass: function(field) {
                return this.getErrorText(field) === "" ? "col-md-12" : "col-md-12  border border-danger";
            },
            getErrorText: function(field) {
                if (this.getError(field).length === 0) {
                    return "";
                }
                return this.getError(field)[0];
            },
            showDropDialog: function(client) {
                let self = this;
                if (client.status !==  this.$config.STATUS_INACTIVE) {
                    swal({
                        className: "drop-clients",
                        title: "Удалить позицию?",
                        text: "Удалённая информация не может быть восстановлена",
                        buttons: {
                            confirm: {
                                text: "Удалить",
                                value: true,
                                visible: true,
                                className: "btn btn-danger",
                                closeModal: true
                            },
                            cancel: {
                                text: "Отмена",
                                value: null,
                                visible: true,
                                className: "btn btn-secondary",
                                closeModal: true,
                            }
                        }
                    }).then((willDelete) => {
                        if (willDelete) {
                            self.dropItem(client.manager_client_rel);
                        }
                    });
                }
                if (client.status === this.$config.STATUS_INACTIVE) {
                    swal({
                        text: 'Данный клиент не активен! Для удаления сначала переведите учетную запись данного клиента в статус "Активный"',
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true,
                    });
                }
            },
            resendRegistrationEmail: function(id) {
                if (this.email_is_sending) {
                    return;
                }
                this.email_is_sending = true;
                let self = this;
                self.createSwal("Идет отправка письма", "info", 60000);
                return axios.post(self.send_mail_link, {_csrf: this._csrf, id: id})
                    .then(function (response) {
                        if (!response.data.is_success) {
                            self.error_list = {};
                            if (typeof response.data.errors[0] === "string") {
                                self.createSwal(response.data.errors[0], "error", 3000);
                            }
                            self.email_is_sending = false;
                        } else {
                            self.createSwal("Готово!", "success", 3000);
                            swal.close();
                            self.email_is_sending = false;
                        }
                    });
            },
            getTableHeads: function() {
                return {
                    full_name: 'Наименование Клиента',
                    company_group: 'Дочерние ЮЛ',
                    manager_user_id: 'Менеджер',
                    status: 'Статус',
                    action: 'Действие',
                    actions: ' '
                }
            },
            getNotSortableColumns: function () {
                return {action: 'Действие', actions: ' '};
            },
            getCustomValuesToSortedColumns: function () {
                let managers = [];
                for (let key in this.managers) {
                    managers[this.managers[key]['id']] = this.managers[key]['title'];
                }
                return {status: this.status_list, manager_initials: managers}
            },
            getBusinessType: function(client) {
                return '<span>' + this.business_type_list[client.business_type] + '</span>';
            },
            getClientInitials: function (client) {
                return '<span>' + client.last_name
                    + ' ' + client.first_name
                    + ' ' + client.middle_name + '</span>';
            },
            getManagerInitials: function (manager_id) {
                for (let key in this.managers) {
                    if (this.managers[key]['id'] === manager_id) {
                        return '<span>' + this.managers[key]['title'] + '</span>';
                    }
                }
                return '<span>Менеджер деактивирован</span>';
            },
            getStatusClass: function(client) {
                let css_style = client.status ? this.status_modes[client.status] : '';
                let mode = client.status ? 'badge-' + css_style  : '';
                return 'badge ' + mode + ' text-white small py-1 px-2';
            },
            getStatusTitle: function(client) {
                return client.status ? this.status_list[client.status] : '';
            },
            getAction: function (client) {
                if (client.status === this.$config.STATUS_DRAFT) {
                    return '<span class="btn btn-link btn-sm p-0">Редактировать данные клиента</span>';
                }
                if (client.status === this.$config.STATUS_NO_ACCOUNT) {
                    return '<span class="btn btn-link btn-sm p-0">Создать аккаунт</span>';
                }
                if (client.status === this.$config.STATUS_EMAIL_NOT_CONFIRM) {
                    return '<span class="btn btn-link btn-sm p-0" data-toggle="tooltip" data-placement="left" ' +
                        'title="Повторно выслать письмо со ссылкой на подтверждение email адреса клиента">Выслать повторно</span>';
                }
                if (client.status === this.$config.STATUS_ACTIVE) {
                    return '<span class="btn btn-link btn-sm p-0">Деактивировать</span>';
                }
                if (client.status === this.$config.STATUS_INACTIVE) {
                    return '<span class="btn btn-link btn-sm p-0">Активировать</span>';
                }
            },
            makeAction: function (client) {
                if (client.status === this.$config.STATUS_DRAFT) {
                    return this.showModal(client);
                }
                if (client.status === this.$config.STATUS_NO_ACCOUNT) {
                    this.initForm(client);
                    this.new_client_form.create_account = true;
                    return this.addClient();
                }
                if (client.status === this.$config.STATUS_EMAIL_NOT_CONFIRM) {
                    this.resendRegistrationEmail(client.manager_client_rel);
                }
                if (client.status === this.$config.STATUS_ACTIVE) {
                    this.changeClientStatus(client.manager_client_rel, false, false);
                }
                if (client.status === this.$config.STATUS_INACTIVE) {
                    this.changeClientStatus(client.manager_client_rel, true, false);
                }
            },
            changeClientStatus: function(id, confirm, first_time) {
                let self = this;
                let title = '';
                let text = '';
                let button_name = '';
                let button_class = '';
                if (first_time) {
                    title = confirm ? "Подтвердить активацию клиента?" : "Отменить активацию клиента?";
                    text = confirm ? "Клиент получит данные для входа в личный кабинет" : "Клиент получит данные для входа в личный кабинет, но доступ к нему будет отключен";
                    button_name = confirm ? "Подтвердить" : "Отменить";
                    button_class = confirm ? "btn btn-primary" : "btn btn-danger";
                } else {
                    title = confirm ? "Активировать клиента?" : "Деактивировать клиента?";
                    text = confirm ? "Клиент сможет пользоваться системой самостоятельно" : "Личный кабинет клиента будет отключен";
                    button_name = confirm ? "Активировать" : "Деактивировать";
                    button_class = confirm ? "btn btn-primary" : "btn btn-danger";
                }

                swal({
                    className: "drop-clients",
                    title: title,
                    text: text,
                    buttons: {
                        confirm: {
                            text: button_name,
                            value: true,
                            visible: true,
                            className: button_class,
                            closeModal: true
                        },
                        cancel: {
                            text: "Отмена",
                            value: null,
                            visible: true,
                            className: "btn btn-secondary",
                            closeModal: true,
                        }
                    }
                }).then((changeStatus) => {
                    if (changeStatus) {
                        self.createSwal("Идет сохранение", "info", 60000);
                        return axios.post(self.change_status_link, {_csrf: self._csrf, id: id, confirm: confirm, first_time: first_time})
                            .then(function (response) {
                                if (!response.data.is_success) {
                                    self.error_list = {};
                                    if (typeof response.data.errors[0] === "string") {
                                        self.createSwal(response.data.errors[0], "error", 3000);
                                    } else {
                                        self.createSwal("Исправьте ошибки в карточке клиента!", "error", 3000);
                                    }
                                } else {
                                    self.createSwal("Сохранено", "success", 3000);
                                    swal.close();
                                    self.load();
                                }
                            });
                    }
                });
            },
            getBusinessTypes: function(zero_row) {
                let result = zero_row ? [{id: null, title: "Все значения"}] : [];
                for (let key in this.business_type_list) {
                    result.push({id: key, title: this.business_type_list[key]});
                }
                return result;
            },
            getClientStatuses: function() {
                let result = [{value: null, title: "Все значения"}];
                for (let key in this.status_list) {
                    result.push({value: key, title: this.status_list[key]});
                }
                return result;
            },
            getManagers: function() {
                let result = [];
                for (let key in this.managers) {
                    result.push({id: this.managers[key]['id'], title: this.managers[key]['title'] + ' (' + this.managers[key]['email'] + ')'});
                }
                return result;
            },
            getRates: function() {
                let result = [];
                for (let key in this.rate_list) {
                    result.push({id: this.rate_list[key]['id'], title: this.rate_list[key]['title']});
                }
                return result;
            },
            getFilteredList: function() {
                let result = [];
                for (let i = 0; i < this.client_list.length; ++i) {
                    let client = this.client_list[i];
                    if ((this.business_type_filter == null && this.status_filter == null) ||
                        (this.status_filter == null && this.business_type_filter == client.business_type) ||
                        (this.status_filter == client.status && this.business_type_filter == null) ||
                        (this.status_filter == client.status && this.business_type_filter == client.business_type)
                    ) {
                        result.push(this.client_list[i]);
                    }
                }
                return result;
            },
            loadManagers: function() {
                let self = this;
                axios.get(self.get_managers_link)
                    .then(function (response) {
                        self.managers = response.data.content;
                    });
            },
            loadRateList: function() {
                let self = this;
                axios.get(self.get_rate_link)
                    .then(function (response) {
                        self.rate_list = response.data.content;
                    });
            },
            updateChildCompanies($event) {
                $event.newList.forEach(el => {
                    if (!this.child_companies.includes(el)) {
                        this.child_companies.push(el);
                    }
                });
                $event.oldList.forEach(el => {
                    if (!$event.newList.includes(el)) {
                        this.child_companies.splice(this.child_companies.indexOf(el), 1);
                    }
                });
            },
        },
        mounted: function() {
            this.load();
            this._csrf = this.$refs.config.dataset.csrf;
            this.status_modes = JSON.parse(this.$refs.config.dataset.status_modes);
            this.status_list = JSON.parse(this.$refs.config.dataset.status_list);
            this.business_type_list = JSON.parse(this.$refs.config.dataset.business_type_list);
            this.base_order_rate_id = JSON.parse(this.$refs.config.dataset.base_order_rate_id);
            this.loadRateList();
            this.loadManagers();
            this.show_modal = JSON.parse(this.$refs.config.dataset.show_modal);
            if (this.show_modal === true) {
                this.showModal();
            }
        }
    });

    new Vue({
        el: '#client'
    });
});
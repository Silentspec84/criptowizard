"use strict";

document.addEventListener("DOMContentLoaded", function() {

    new Vue({
        el: "#cart",
        mixins: [listMixins, urlMixins, validatorRowsMixins, formulasMixins],
        data: {
            title_fields: {},
            rows_cart: [],
            selected_row_keys: [],
            selected_rows: [],
            sorted_rows: [],
            rate_id: "",
            current_rate_id: "",
            editable_rows: [],
            formulas_for_editable_fields: [],
            fields_currency: [],
            rates: {},
            currencies: [],
            error_list: {},
            border_for_block: {},
            not_exist_value: null,
            status: 0, //0 - in process, 1 - success, 2 - error
            invalid_inputs: [],
            client_company_id: null,
            offer_id: null,
            is_loading: false,
            get_link: '/cart-api/get-rows-from-search',
            _csrf: null,
            form: {
                data_list: this.data_list,
                _csrf: null
            },
            is_version_exists: { // существование предыдущей и последующей версси. Нужно для определения состояния кнопкок undo и redo
                prev: false,
                next: false
            }
        },
        mounted: function () {
            var dataset = this.$refs.config.dataset;
            this.client_company_id = JSON.parse(this.$refs.config.dataset.client_company_id);
            this.offer_id = JSON.parse(this.$refs.config.dataset.offer_id);
            this.rate_id = JSON.parse(this.$refs.config.dataset.rate_id);
            this.current_rate_id = "";
            this.not_exist_value = JSON.parse(this.$refs.config.dataset.not_exist_value);
            this.form._csrf = dataset.csrf;
            this._csrf = dataset.csrf;
            this.onLoad();
        },
        methods: {
            isValidDate: function(date)
            {
                return moment(date, 'DD.MM.YYYY', true).isValid();
            },
            isFutureDate: function(date)
            {
                return moment(date, 'DD.MM.YYYY', true).isAfter(moment().format("YYYY-MM-DD"))
                        || moment(date, 'DD.MM.YYYY', true).isSame(moment().format("YYYY-MM-DD"));
            },
            sortBy: function (data) {
                this.sorted_rows = data;
            },

            onUpdateCell: function (line, row, value) {
                if (this.recalculate(line, row, value)) {
                    this.saveRowChanges(line, row, value);
                }
                this.$nextTick(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            },

            onUpdateCol: function (rows) {
                let update_rows =  [];

                for (let i = 0; i < rows.length; i++) {
                    if (this.recalculate(rows[i].index, rows[i].key, rows[i].value)) {
                        update_rows.push(this.rows_cart[rows[i].index]);
                    }
                }

                if (update_rows.length > 0) {
                    this.saveAllRowChanges(update_rows);
                }
                this.$nextTick(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });

            },

            recalculate: function(line, row, value) {
                this.rows_cart = this.sorted_rows.length ? this.sorted_rows : this.rows_cart;

                this.validate_rows(line, row, value);
                if (this.areRowsHaveErrors()) {
                    return false;
                }

                if (typeof(this.formulas_for_editable_fields[row]) === 'undefined') {
                    return true;
                }

                let related_fields_id = this.formulas_for_editable_fields[row]['related_fields'];
                if (this.formulas_for_editable_fields[row]['type_field'] === 'subtotal') {
                    // Случай когда приходит this.not_exist_value в поле подитог, ставим связанному полю и результирующему также this.not_exist_value
                    if (value === this.not_exist_value) {
                        this.rows_cart[line][related_fields_id] = this.not_exist_value;
                        // Пересчет формул заданных пользователем
                        this.rows_cart = this.getCalculateRows(this.title_fields, this.rows_cart, this.formulas);

                        return true;
                    }

                    // Случай когда приходит НЕ this.not_exist_value в поле подитог, проверяем значение связаного поля
                    // Связаное поле this.not_exist_value, значит нужно пересчитать это поле по формуле пересчета
                    if (this.rows_cart[line][related_fields_id] === this.not_exist_value) {
                        var formula_parts = this.formulas_for_editable_fields[row]['formula_for_recalculate'].match(/(\d+)\s*\=\s*(\d+)\s*([\+\-])\s*(\d+)/);
                        this.rows_cart[line][formula_parts[1]] = this.calculateFormulaForEditableCells(formula_parts, line, [formula_parts[3]]);
                    }
                }

                // Если у связанного поля с маржой значение this.not_exist_value, то не менять связанные поля и не пересчитывать Итог, только Итог Маржи
                if (this.formulas_for_editable_fields[row]['type_field'] === 'rate' && this.rows_cart[line][related_fields_id] === this.not_exist_value) {
                    // Пересчет формул заданных пользователем
                    this.rows_cart = this.getCalculateRows(this.title_fields, this.rows_cart, this.formulas);
                    return true;
                }

                // Рассчитываем поле по обычной формуле
                var formula_parts = this.formulas_for_editable_fields[row]['formula'].match(/(\d+)\s*\=\s*(\d+)\s*([\+\-])\s*(\d+)/);
                this.rows_cart[line][formula_parts[1]] = this.calculateFormulaForEditableCells(formula_parts, line, [formula_parts[3]]);
                // Пересчет формул заданных пользователем
                this.rows_cart = this.getCalculateRows(this.title_fields, this.rows_cart, this.formulas);

                return true;
            },
            onLoad: async function() {
                const response = await axios.get('/cart-api/get-rows-from-search', {params: {client_company_id: this.client_company_id}});
                if (response.data.is_success) {
                    this.status = 1;
                    this.title_fields = response.data.content.title_fields;
                    this.rows_cart = Object.values(response.data.content.rows_cart);
                    this.formulas = response.data.content.formulas;
                    this.currencies = response.data.content.currencies;
                    this.editable_rows = response.data.content.editable_rows;
                    this.formulas_for_editable_fields = response.data.content.formulas_for_editable_fields;
                    this.fields_currency = response.data.content.fields_currency;
                    this.border_for_block = response.data.content.border_for_block;
                    this.is_version_exists = response.data.content.is_version_exists;
                    this.current_rate_id = response.data.content.current_rate_id;
                    if (this.current_rate_id === 0) {
                        this.current_rate_id = this.rate_id;
                    }
                    this.$nextTick(function () {
                        $('[data-toggle="tooltip"]').tooltip('hide');
                    });

                    for (let row in this.rows_cart) {
                        this.rows_cart[row]['shown'] = true;
                    }

                    // Пересчет формул заданных пользователем
                    this.rates = JSON.parse(this.$refs.config.dataset.rates);
                    this.rows_validation_rules = response.data.content.rows_validation_rules;
                    this.rows_cart = this.getCalculateRows(this.title_fields, this.rows_cart, this.formulas);
                    if(this.error_list['all_fields']) {
                        this.error_list['all_fields'] = "<div class=\"alert alert-danger alert-dismissible fade show\">" + "Произошла ошибка. Попробуйте повторить позже</div>";
                        this.status = 2;
                        this.is_loading = false;
                        return;
                    }
                    this.is_loading = false;
                    return;
                }

                this.is_loading = false;
                this.status = 2;
                this.error_list = response.data.errors;
            },
            getTableHeads: function() {
                let heads = {};
                for(let key in this.title_fields) {
                    heads[key] = this.title_fields[key]['title'];
                }

                return heads;
            },
            updateRows(rate_id, is_change_rate) {
                if (rate_id) {
                    var hash_checked_rows = [];
                    this.rows_cart.forEach(function(row, index, arr) {
                        if (row['checked'] === true) {
                            hash_checked_rows.push(row['additional_params']['hash_row']);
                        }
                    });

                    return axios.post('/cart-api/get-rows-from-search', {_csrf: this.form._csrf, is_update_row: true, hash_checked_rows: hash_checked_rows , rate_id: rate_id, client_company_id: this.client_company_id, is_change_rate: is_change_rate})
                        .then(response => {
                            if (response.data.is_success) {
                                this.status = 1;
                                this.title_fields = response.data.content.title_fields;
                                this.rows_cart = Object.values(response.data.content.rows_cart);
                                this.rows_cart = this.getCalculateRows(this.title_fields, this.rows_cart, this.formulas);
                                if(this.error_list['all_fields']) {
                                    this.error_list['all_fields'] = "<div class=\"alert alert-danger alert-dismissible fade show\">" + "Произошла ошибка. Попробуйте повторить позже</div>";
                                    this.status = 2;
                                    this.is_loading = false;
                                    return;
                                }
                                for (let row in this.rows_cart) {
                                    this.rows_cart[row]['shown'] = true;
                                }

                                this.rate_id = response.data.content.rate_id;
                                this.current_rate_id = response.data.content.current_rate_id;
                                this.is_version_exists = response.data.content.is_version_exists;
                                if (this.current_rate_id === 0) {
                                    this.current_rate_id = this.rate_id;
                                }

                            } else {
                                this.status = 2;
                                this.error_list = response.data.errors;
                            }
                        }).catch(e => {
                            this.status = 2;
                        });
                }
            },
            /** отправляет запрос на изменения 1-й строки */
            saveRowChanges: function (line, row, value) {
                return axios.post('/cart-api/save-cart-rows', {_csrf: this.form._csrf, row: this.rows_cart[line], client_company_id: parseInt(this.client_company_id), rate_id: this.current_rate_id})
                    .then(response => {
                        if (response.data.is_success) {
                            this.is_version_exists = response.data.content.is_version_exists;
                        } else {
                            this.status = 2;
                            this.error_list = response.data.errors;
                        }
                    }).catch(e => {
                        this.status = 2;
                    });
            },
            /** аналогично saveRowChanges, но для нескольких строк */
            saveAllRowChanges: function (rows) {
                return axios.post('/cart-api/save-cart-rows', {_csrf: this.form._csrf, row: rows, client_company_id: parseInt(this.client_company_id), is_multi_rows: true, rate_id: this.current_rate_id})
                    .then(response => {
                        if (response.data.is_success) {
                            this.is_version_exists = response.data.content.is_version_exists;
                        } else {
                            this.status = 2;
                            this.error_list = response.data.errors;
                        }
                    }).catch(e => {
                        this.status = 2;
                    });
            },

            GenerateKp: function() {
                this.error_list = {};
                this.selected_rows = this.rows_cart.filter(item => item['checked']);

                if (this.areRowsHaveErrors()) {
                    swal('Исправьте ошибки, выделенные красным!', {
                        buttons: false,
                        icon: "error",
                        timer: 2000,
                    });
                    return;
                }


                axios.post('/cart-api/generate-kp', {_csrf:this._csrf, rows: this.selected_rows, offer_id: this.offer_id}).then(response => {
                    if (response.data.is_success) {
                        swal("Сохранено", {
                            buttons: false,
                            timer: 1500,
                        });
                        window.location.href = "/manager-offer/view?id=" + this.offer_id;
                    } else {
                        this.error_list = {};
                        for (var key in response.data.errors) {
                            this.error_list[key] = response.data.errors[key];
                        }
                    }
                }).catch(e => {
                    this.status = 2;
                });
            },
            DeleteRows: function() {
                let is_selected_all_rows = false;
                this.selected_rows = this.rows_cart.filter(item => item['checked']);
                if (this.selected_rows.length === 0) {
                    return; // нет отмеченных строк, нечего сохранять
                }

                if (this.selected_rows.length === this.rows_cart.length) { //выбраны все строки
                    is_selected_all_rows = true;
                }

                axios.post('/cart-api/delete-cart-rows', {_csrf:this._csrf, rows: this.selected_rows, client_company_id: parseInt(this.client_company_id), is_all_rows: is_selected_all_rows}).then(response => {
                    if (response.data.is_success) {
                        swal("Строки удалены из корзины", {
                            buttons: false,
                            timer: 3000,
                        });
                        this.onLoad();
                    } else {
                        this.status = 2;
                        this.error_list = response.data.errors;
                    }
                }).catch(e => {
                    this.status = 2;
                });
            },
            calculateFormulaForEditableCells: function(formula_parts, number_row, operator) {
                let row = this.rows_cart[number_row];

                if (!formula_parts[1]) {
                    this.error_editable_cell['error'] = 'Ошибка подсчета формулы';
                } else {
                    operator[0] = operator[0] || '+';

                    for (var i = 0; i < operator.length; i++) {
                        if (i === 0) {
                            row[formula_parts[1]] = +parseFloat(row[formula_parts[i+2]]);
                        }
                        if (operator[i] === '+') {
                            row[formula_parts[1]] = parseFloat((row[formula_parts[1]])) + +parseFloat(row[formula_parts[i * 2 + 4]]);
                        } else if (operator[i] === '-') {
                            row[formula_parts[1]] = parseFloat(row[formula_parts[1]]) - +parseFloat(row[formula_parts[i * 2 + 4]]);
                        }
                    }

                }

                return Math.round((row[formula_parts[1]])*100)/100;
            },

            /**
             * Возвращает query из get-параметров поиска, с которого перешли в корзину.
             * Служит, чтобы можно было вернуться к поиску с предзаполненной из get-параметров поисковой формой.
             * @returns {string}
             */
            getSearchUrlParams: function() {
               let cart_url = this.parseUrl(location.href);
               if (!cart_url.searchObject.search_params) {
                    return '';
               }
               let encoded_search_params  = cart_url.searchObject.search_params;
               let search_params_query = decodeURIComponent(encoded_search_params);
               return search_params_query;
            },

            /**
             * Меняет текущую версию корзины на 1 назад
             */
            undo: async function () {
                this.is_loading = true;
                const response = await axios.post('/cart-api/undo', {_csrf:this._csrf, client_company_id: parseInt(this.client_company_id)});
                if (response.data.is_success) {
                    this.onLoad();
                    return;
                }
                this.status = 2;
                this.error_list = response.data.errors;
            },
            /**
             * Меняет версию текущую версию корзины на 1 вперед
             */
            redo: async function () {
                this.is_loading = true;
                const response = await axios.post('/cart-api/redo', {_csrf:this._csrf, client_company_id: parseInt(this.client_company_id)});
                if (response.data.is_success) {
                    this.onLoad();
                    return;
                }
                this.status = 2;
                this.error_list = response.data.errors;
            }
        },
    });
});

"use strict";

document.addEventListener("DOMContentLoaded", function() {

    Vue.component('history-import-table-data-list', {
        mixins: [listMixins],
        template: '#history-import-table-data-list-template',
        data: function () {
            return {
                files_for_render: {},
                error_list: {}
            };
        },
        methods: {
            getTableHeads: function() {
                return {
                    table_name: 'Таблица',
                    save_mode: 'Действие',
                    field_codes: 'Переменные',
                    date_loaded: 'Дата загрузки',
                    date_upload_since: 'Действует с',
                    date_upload_by: 'Действует по',
                    user_full_name: 'Пользователь'
                }
            },
            load: async function() {
                axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
                const response = await axios.get('/history-import-table-data-api/get-files-for-render');

                if (!response.data.is_success) {
                    this.error_list = response.data.errors;
                } else {
                    this.files_for_render = response.data.content;
                    for(let i = 0; i < this.files_for_render.length; i++) {
                        this.$set(this.files_for_render[i], 'shown', true);
                    }
                }
            },
        },
        mounted: function () {
            this.load();
        }
    });

    new Vue({
        el: '#history-import-table-data'
    });
});

"use strict";

document.addEventListener("DOMContentLoaded", function() {
    new Vue({
        components: {
            Multiselect: window.VueMultiselect.default
        },
        mixins: [listMixins, searchFilterMixins],
        el: "#manager_search_filter",
        data: {
            _csrf: null,
            link_get_filter_keys_and_values: '/manager-search-filter-api/get-filter-keys-and-values',
            link_get_options_filter_keys: '/manager-search-filter-api/get-options-filter-keys',
            link_get_options_filter_values: '/manager-search-filter-api/get-options-filter-values',
            link_add_filter_key: '/manager-search-filter-api/add-filter-key',
            link_add_filter_row: '/manager-search-filter-api/add-filter-row',
            link_safe_delete_filter_key: '/manager-search-filter-api/safe-delete-filter-key',
            link_delete_filter_key: '/manager-search-filter-api/delete-filter-key',
            link_delete_filter_row: '/manager-search-filter-api/delete-filter-row',
            link_view_search_filter_rows: '/manager-search-filter/view-rows',
            link_view_exception_rows: '/manager-search-filter/view-exception-rows',
        },
        methods: {
            /** Переход по ссылке на поисковую выдачу для не попавших под фильтры строк*/
            viewExceptionRows() {
                window.open(this.link_view_exception_rows, '_blank');
            },
        }
    });
});
"use strict";

/**
 * Компонент для отображения строк по фильтрам-чекбоксам.
 * Например фильтр показывающий архивные/активные записи.
 */
Vue.component('filters-checkboxes', {
    template: `<div v-if="filters">
                    <span class="mr-2">{{header_filters}}</span>
                    <div class="checkbox checkbox-primary form-check-inline" v-for="(filter, index) in filters">
                        <input type="checkbox" :id="'checkbox_' + index" v-model="filter.is_active" v-on:change="changeFilter()">
                        <label class="ml-2 mr-2" :for="'checkbox_' + index">{{filter.title}}</label>
                    </div>
                </div>`,
    props: {
        header_filters: {
            type: String,
            default: ''
        },
        /** Массив вида:
         * [
         *      0 => [
         *          title => 'Архивные',
         *          set_value => Offer::IS_ARCHIVED,
         *          is_active => false
         *      ],
         *          ...
         *      N => [
         *          title => 'Активные',
         *          set_value => Offer::NOT_ARCHIVED,
         *          is_active => true
         *      ]
         * ]
         * */
        filters: [],
    },
    methods: {
        changeFilter() {
            this.$emit('changeFilters', this.filters);
        }
    }
});
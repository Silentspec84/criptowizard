export const urlMixins = {
    methods: {
        /**
         * Разбирает url на компоненты.
         * К каждому get-параметру можно обратиться через свойства объекта searchObject
         * @param url
         * @param strict_boolean если true - параметры со строковыми значениями 'true' или 'false' приводятся к логическому типу boolean
         * @returns {{protocol: (string|*|string), host: (string|*), hostname: (string|*), port: (string|Function|*|number), pathname: (string|*), search, searchObject: {}, hash}}
         */
        parseUrl(url, strict_boolean) {
            let parser = document.createElement('a'),
                searchObject = {},
                queries, split, i;
            // Let the browser do the work
            parser.href = url;
            // Convert query string to object
            queries = parser.search.replace(/^\?/, '').split('&');
            for( i = 0; i < queries.length; i++ ) {
                split = queries[i].split('=');
                if (strict_boolean) {
                    if (split[1] === 'true') {
                        split[1] = true;
                    } else if (split[1] === 'false') {
                        split[1] = false;
                    }
                }
                searchObject[split[0]] = split[1];
            }
            return {
                protocol: parser.protocol,
                host: parser.host,
                hostname: parser.hostname,
                port: parser.port,
                pathname: parser.pathname,
                search: parser.search,
                searchObject: searchObject,
                hash: parser.hash
            };
        },

        /**
         * Аналог http://php.net/manual/ru/function.http-build-query.php на js.
         * Генерирует URL-кодированную строку запроса
         * @param data
         * @returns {string}
         */
        httpBuildQuery(data) {
            const ret = [];
            for (let d in data) {
                ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
            }
            return ret.join('&');
        },

        /**
         * Перестраивает get_link, добавляя туда get-параметров из params
         * Например, была строка /search?page=1, а в параметрах было {sort_direction:1, field_order:5}
         * тогда на выходе получим строку /search?page=1&sort_direction=1&field_order=5
         * @param get_link
         * @param params
         * @returns {string}
         */
        reBuildGetLink(get_link, params) {
            let url = this.parseUrl(get_link);
            for (let param in url.searchObject) {
                if (param && url.searchObject[param] && url.searchObject.hasOwnProperty(param) && !params[param]) {
                    params[param] = url.searchObject[param];
                }
            }
            return url.pathname + '?' + this.httpBuildQuery(params);
        }

    }
};
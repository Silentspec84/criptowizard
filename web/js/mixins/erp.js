/**
 * MixIns which render erp order list for manager and client by user query
 *
 * use in /web/js/ui
 * element which refers to vue entity must have
 *
 */
import {paginatorMixin} from './paginator';
import {urlMixins} from './url';
import {calendarSettingsMixin} from './calendar-settings';

export const erpMixins = {
    mixins: [paginatorMixin, urlMixins, calendarSettingsMixin],
    data: function () {
        return {
            headers: {},
            orders: [],
            offers: [],
            query: '',
            status: 'active_records',
            show_modal: false,
            archive_candidates: [],
            is_only_active_data: true,
            error_list: [],
            warning_list: {},
            filters: {
                status: 0,
                active_records: true,
                archive_records: false,
            },
            statuses: [],
            pages: [],
            current_page: null,
            get_link:'/erp-api/search',
            is_loading: false, // загрузка в процессе?
            //заполняем поля фильтра по дате начальными значениями
            //диапазон - неделя
        };
    },
    watch: {
        status: function (val) {
            if (val === 'active_records') {
                this.filters.active_records = true;
                this.filters.archive_records = false;
            }
            if (val === 'archive_records') {
                this.filters.active_records = false;
                this.filters.archive_records = true;
            }
            if (val === 'all_records') {
                this.filters.active_records = true;
                this.filters.archive_records = true;
            }
        }
    },
    methods: {
        getTableHeads: function (as_array) {
            if (as_array) {
                let head_list = [];
                for (let key in this.headers) {
                    let head = {
                        key: key,
                        value: this.headers[key]
                    }
                    head_list.push(head);
                }
                return head_list;
            }
            return this.headers;
        },
        loadData(link, params) {
            this.is_loading = true;
            if (link) {
                this.get_link = link;
            }
            if (params) {
                this.get_link = this.reBuildGetLink(this.get_link, params);
            }

            axios.post(this.get_link, {
                _csrf: this.$csrfToken,
                filters: this.filters,
                query: this.query
            }, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            }).then(response => {
                if (response.data.is_success) {
                    this.error_list = [];
                    this.warning_list = [];
                    this.warning_list = response.data.warnings;
                    this.orders = response.data.content.rows;
                    this.headers = response.data.content.headers;
                    this.current_page = response.data.content.current_page;
                    this.page_links = response.data.content.page_links;
                    this.statuses = response.data.content.statuses;
                    this.pages = this.getPages(this.page_links);
                } else {
                    this.error_list = response.data.errors;
                    this.orders = [];
                }
                this.is_loading = false;
            }).catch(e => {
                console.warn('cant load dates');
            });

        },
        load() {
            let link = '';
            let parse_url = this.parseUrl(window.location, true);
            if (parse_url.search) {
                let get_params = parse_url.searchObject;
                this.filters = get_params;
                this.query = get_params.query;

                if (get_params.page) {
                    link = this.get_link + '?' + this.httpBuildQuery({page: get_params.page});
                }
            }

            this.loadData(link);
        },
        search: function (link) {
            this.error_list = {};
            delete this.filters.page;
            delete this.filters.query;
            if (this.query) {
                this.filters.query = this.query;
            }

            window.history.pushState(null, null, '?'+this.httpBuildQuery(this.filters));
            this.loadData(link);
        }
    },
    mounted: function () {
        this.filters.date_from = (function () {
            let prev_days = 365;
            let date = new Date();
            let time = date.valueOf();
            let new_date = new Date(time - ((24 * 60 * 60 * 1000) * prev_days));
            return (("0" + (new_date.getDate())).slice(-2)) + '.' +  (("0" + (new_date.getMonth() + 1)).slice(-2)) + '.' + new_date.getFullYear();
        })();

        this.filters.date_to = (function () {
            let date = new Date();
            return (("0" + (date.getDate())).slice(-2)) + '.' + (("0" + (date.getMonth() + 1)).slice(-2)) + '.' + date.getFullYear();
        })();

        const picker_from = new Pikaday({
            field: document.getElementById('date_from'),
            ...this.calendarSettings
        });
        const picker_to = new Pikaday({
            field: document.getElementById('date_to'),
            ...this.calendarSettings
        });

        this.load();
        this._csrf = this.$csrfToken;
    }
}
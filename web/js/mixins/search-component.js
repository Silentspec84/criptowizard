/** Миксин для работы с компонентом поиска, используется, например, в итоговой таблице и настройках поисковой выдачи(фильтрах) */
export const seacrchComponentMixins = {
    data: function () {
        return {
            link_get_search_result: '',
            query: '',
            search_placeholder: 'Что искать...',
            error_list: {},
            search_result: {}
        };
    },
    methods: {
        /** Метод валидирует запрос из формы, и пытается получить поисковую выдачу */
        async search() {
            if (this.query.length < 3) {
                this.$set(this.error_list, 'search', 'Введите минимум 3 символа');
                return false;
            }
            const response = await this.getSearchResultResponse();
            if (!response) {
                return false;
            }
            if (!response.data.is_success) {
                this.error_list = {};
                this.search_result = {};
                this.error_list = response.data.errors;
                return false;
            } else {
                this.error_list = {};
                this.search_result = response.data.content;
                return true;
            }
        },
        /**
         * Метод запускаемый при нажатие кнопки искать, переопределите в компоненте,
         * к которому примешан миксин, если хотите обрабатывать результаты выполнения метода search
         **/
        async onSearch() {
            return await this.search();
        },
        /**
         * Возвращает response, метод получения поисковой выдачи по ссылке link_get_search_result.
         * Если стандартное поведение не подходит переопределите в компоненте, к которому примешан миксин,
         * По дефолту делает post запрос по link_get_search_result и соответственно нужен _csrf.
         * Если они не определены засетит ошибку и вернет false
         * */
        getSearchResultResponse() {
            if (!this.link_get_search_result || !this._csrf) {
                this.$set(this.error_list, 'search', 'Ошибка поиска, обратитесь к админстратору');
                return false;
            }

            return axios.post(this.link_get_search_result, {_csrf:this._csrf, query: this.query});
        },
        /** Добавляет класс для визуальной-подсветки value, совпадающего c query */
        highlight(value){
            if (!this.query || this.query.length < 3) {
                return value;
            }

            value = String(value);
            let regexp = new RegExp(this.query, 'gi'); // регулярка впилина, чтобы находить регистронезависимо
            let result = value.replace(regexp, function(str) {
                return '<span class=\'highlight\'>' + str + '</span>';
            });

            return result;
        },
    }
};
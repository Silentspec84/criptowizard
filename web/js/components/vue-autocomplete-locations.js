"use strict";

/**
 *
 * @type {{data: locationAutocompleteMixins.data, methods: {onChange: locationAutocompleteMixins.methods.onChange, printSuggestion: locationAutocompleteMixins.methods.printSuggestion, chooseSuggestion: locationAutocompleteMixins.methods.chooseSuggestion}}}
 */
const locationAutocompleteMixins = {
    data: function() {
        return {
            list: [],
            items: [],
            loading_valid_location_ids: null,
            discharge_valid_location_ids: null,
            show_suggestions: 0,
            end_step: false,
            all_locations_index: null,
            related_location_index: null,
            //если выбрана конечная точка порт, жд, авто.
            is_selected_endpoint: false
        };
    },
    methods: {
        onChange: function(query, item_id, type, title, lang) {
            if (!query && !item_id) { // данные стерли, надо об этом сказать
                this.$emit('option_chosen', {
                    'field_title': this.filed_title,
                    'item': null
                });
                return;
            }
            let get_data = [];
            if (query) {
                if (this.end_step) return;
                get_data.push('query=' + query);
                this.is_selected_endpoint = false;
            }
            if (item_id) {
                this.end_step = true;
                get_data.push('parent_location_id=' + item_id);

                if (this.list.length > 0) {
                    let filtered_list = this.list.filter(item => item.id === item_id);
                    if (this.excludedTypes.indexOf(type) != -1) {
                        this.item = filtered_list[0];
                    }
                }
            }
            if (type) {
                get_data.push('type=' + type);
            }
            if (title) {
                get_data.push('title=' + title);
            }
            if (lang) {
                get_data.push('lang=' + lang);
            }

            let result = get_data.join('&');
            let link = this.link + result;
            let self = this;
            if (!this.is_selected_endpoint) {
                axios.get(link)
                    .then(response => {
                        this.related_location_index = null;
                        self.list = response.data.content;
                        self.list = self.list.filter(el => {
                            if (el.id.length === 1) {
                                if (this.location_type === '1') {
                                    return this.loading_valid_location_ids.includes(el.id.join(','));
                                }
                                return this.discharge_valid_location_ids.includes(el.id.join(','));
                            } else {// "все порты" показываем всегда, но id, которые за ними скрываются должны фильтроваться
                                el.id = el.id.filter(port => {
                                    if (this.location_type === '1') {
                                        return this.loading_valid_location_ids.includes(port);
                                    }
                                    return this.discharge_valid_location_ids.includes(port);
                                });

                                return true;
                            }
                        });

                        if (self.list.length === 0) {//Список после фильтра оказался пустым, показываем ошибку
                            self.list.push({
                                title:'Не найдено подходящих маршрутов',
                                type: {
                                    error: true,
                                }
                            });
                        }
                        this.setRelatedLocationIndex();
                    }).catch(function () {
                        console.warn('Ошибка при загрузке данных по ссылке' + link);
                    });
            }

        },
        setRelatedLocationIndex: function() {
            for (let i = 0; i < this.list.length; i++) {
                if (this.list[i].is_related_location) {
                    this.related_location_index = i;
                    break;
                }
            }
        },
        printSuggestion: function(query) {
            if (query.length >=3) {
                this.end_step = false;
                this.items = [];

                if (this.timer) {
                    clearTimeout(this.timer);
                    this.timer = null;
                }
                this.timer = setTimeout(() => {
                    this.onChange(query);
                }, 200);
            } else {
                this.list = [];
                this.$emit('option_chosen', {
                    'field_title': this.filed_title,
                    'item': null
                });
            }
        },
        chooseSuggestion: function(item) {
            this.items.push(item);
            if (item.render_type === "end_point") {
                this.is_selected_endpoint = true;
            }

            this.onChange(null, item.id, item.type.type, item.title, item.lang);
            if (this.is_selected_endpoint) {
                this.item = this.item ? this.item : item;
                if (item.is_all_locations) {
                    this.item.title = this.item.type.title;
                }
                this.$emit('option_chosen', {
                    'field_title': this.filed_title,
                    'item': this.item
                });
            }
        }
    },
};

/**
* Поиск по локациям с автодополнениям.
*/
Vue.component('locations-autocomplete', {
    template: `
        <v-autocompleete v-model="item.title" :onshow="show_suggestions" :options="list" items="items" @input="printSuggestion(item.title)" :input_class='input_class' @hide="$emit('hide');">
                <div slot="selected" class="location-icon" v-if="item.is_icon_needed || is_selected_endpoint">
                    <i :class="item.type.icon"></i>
                </div>
        
                <div slot="option" slot-scope="option" v-show="!is_selected_endpoint">
                    <div v-if="related_location_index == option.index" class="disabled">
                         <span>Связанные регионы </span>
                    </div>
                 
                    <div @click="chooseSuggestion(option.data);">
                        <i :class="option.data.type.icon"></i>    
                        <span :class="{'text-danger': option.data.type.error}">{{option.data.title}}</span>
                    </div>
                </div>
        </v-autocompleete>`,
    mixins: [locationAutocompleteMixins],
    data: function() {
        return {
            item: {
                title: null,
                type: {
                    icon: null
                },
                is_icon_needed: null
            },
            timer: null,
            selectedCity: '',
            excluded: [],
            excluded_steps: []
        };
    },
    props: {
        excludedTypes: {
            type: String,
            default: ''
        },
        excludedSteps: {
            type: String,
            default: null
        },
        link: {
            type: String
        },
        location_type: {
            type: String
        },
        get_valid_locations_url: {
          type: String
        },
        filed_title: {
            type: String
        },
        input_class: {
            type: String,
            default: 'form-control form-control-sm',
        },
        location_id: {
            type: [],
            default: 0
        },
        lang: {
            type: '',
            default: 0
        },
        type: {
            type: Number,
            default: 0
        },
        title: {
            type: String
        },
        _csrf: {
            type: String,
            default: ''
        }
    },
    mounted: function () {
        if (this.location_type === '1') {
            axios.get(`${this.get_valid_locations_url}?location_type=1`)
                .then(response => {
                    if (response.data.errors.all_fields) {
                        this.$emit('anErrorOccured', response.data.errors.all_fields);
                        return;
                    }
                    this.loading_valid_location_ids = response.data.content.valid_location_ids;
                });
        }
        if (this.location_type === '2') {
            axios.get(`${this.get_valid_locations_url}?location_type=2`)
                .then(response => {
                    if (response.data.errors.all_fields) {
                        this.$emit('anErrorOccured', response.data.errors.all_fields);
                        return;
                    }
                    this.discharge_valid_location_ids = response.data.content.valid_location_ids;
                });
        }

        if (this.excludedSteps !== null) {
            this.excluded_steps = JSON.parse(this.excludedSteps);
        }
        this.excluded = JSON.parse(this.excludedTypes);

        if (!this.location_id || !this.type || !this.title) {
            return;
        }

        var location_id = [];
        if (this.location_id instanceof Array) {
            location_id = JSON.stringify(this.location_id);
        } else {
            location_id = this.location_id;
        }

        axios.post('/public-api/get-location?location_id=' + location_id + '&type='+ this.type + '&title=' + this.title + '&lang=' + this.lang, {_csrf:this._csrf}).then(response => {
        this.item = response.data.content;
        this.item.is_icon_needed = true;
            this.$emit('option_chosen', {
                'field_title': this.filed_title,
                'item': this.item
            });
        }).catch(e => {
            this.item.is_icon_needed  = false;
            console.log('Ошибка получения локации');
        });
    }
});

"use strict";

Vue.component('sticky-search-form', {
    template: `<div class="sticky-form">
        <div class="row">
            <div class="col-md-5 d-flex align-items-center">
                <div class="sticky-search__item">
                    Маршрут <span class="text-dark">порт {{loading_location}} &mdash; {{discharge_location}}</span>
                </div>
                <div class="sticky-search__item" v-if="company_title">
                    Клиент: <span class="text-dark">{{company_title}}</span>
                </div>
            </div>
            <div class="col-md-7">
                <div class="btn btn-secondary pull-right" @click="$emit('uncollapse')">Изменить запрос</div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 d-flex align-items-center">
                <div class="sticky-search__item">
                    Груз: <span class="text-dark">{{cargo}}" х 1</span>
                </div>
                <div class="sticky-search__item">
                    Вес: <span class="text-dark">{{weight}} кг.</span>
                </div>
                <div class="sticky-search__item">
                    Место ТО: <span class="text-dark">{{custom_type}}</span>
                </div>
                <div class="sticky-search__item">
                    Дата отгрузки: <span class="text-dark">{{date}}</span>
                </div>
                <div class="sticky-search__item" v-if="options">
                    Доп. параметры: 
                    <span class="text-dark">{{options}}</span>
                </div>
            </div>
        </div>
</div>`,
    props: {
        fields: {
            type: Object,
            default() {
                return {};
            }
        },
        additionalParams: {
            type: Object,
            default() {
                return {};
            }
        }
    },
    data() {
        return {
            loading_location: this.fields.loading_location_object.title,
            discharge_location: this.fields.discharge_location_object.title,
            company_title: this.additionalParams.client_company_title,
            cargo: this.fields.cargo,
            weight: this.additionalParams.weight,
            custom_type: this.additionalParams.custom_type === 1 ? 'Порт' : 'Станция',
            loading_date: this.fields.loading_date,
        }
    },
    computed: {
        options() {
            const res = [];
            if (this.additionalParams.is_home_consignment) {
                res.push('H B/L');
            }
            if (this.additionalParams.is_security) {
                res.push('Охрана');
            }
            if (this.additionalParams.is_danger_cargo) {
                res.push('Опасный груз');
            }
            if (this.additionalParams.is_special_tariff) {
                res.push('Спец. тариф');
            }

            return res.join(', ');
        },
        date() {
            let timestamp = moment(this.loading_date, "YYYY-MM-DD").unix();
            return moment.unix(timestamp).format("DD.MM.YYYY");
        },
    },
});
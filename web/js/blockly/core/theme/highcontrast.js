/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview High contrast theme.
 * Darker colours to contrast the white font.
 */
'use strict';

goog.provide('Blockly.Themes.HighContrast');

goog.require('Blockly.Theme');


// Temporary holding object.
Blockly.Themes.HighContrast = {};

Blockly.Themes.HighContrast.defaultBlockStyles = {
  "logic_blocks": {
    "colourPrimary": "#01579b",
    "colourSecondary": "#64C7FF",
    "colourTertiary": "#C5EAFF"
  },
  "math_blocks": {
    "colourPrimary": "#1a237e",
    "colourSecondary": "#8A9EFF",
    "colourTertiary": "#DCE2FF"
  },
  "text_blocks": {
    "colourPrimary": "#004d40",
    "colourSecondary": "#5ae27c",
    "colourTertiary": "#D2FFDD"
  },
  "variable_dynamic_blocks": {
    "colourPrimary": "#880e4f",
    "colourSecondary": "#FF73BE",
    "colourTertiary": "#FFD4EB"
  },
  "time_blocks": {
    "colourPrimary": "#4a148c",
    "colourSecondary": "#AD7BE9",
    "colourTertiary": "#CDB6E9"
  },
  "procedure_blocks": {
    "colourPrimary": "#006064",
    "colourSecondary": "#77E6EE",
    "colourTertiary": "#CFECEE"
  },
  "loop_blocks": {
    "colourPrimary": "#33691e",
    "colourSecondary": "#9AFF78",
    "colourTertiary": "#E1FFD7"
  },
  "list_blocks": {
    "colourPrimary": "#4a148c",
    "colourSecondary": "#AD7BE9",
    "colourTertiary": "#CDB6E9"
  },
  "price_blocks": {
    "colourPrimary": "#98a50b",
    "colourSecondary": "#d1fb75",
    "colourTertiary": "#ebfbda"
  },
  "mm_blocks": {
    "colourPrimary": "#20a50c",
    "colourSecondary": "#acfb90",
    "colourTertiary": "#c2fbc3"
  },
  "osc_blocks": {
    "colourPrimary": "#03a566",
    "colourSecondary": "#9cfbbc",
    "colourTertiary": "#dbfbe5"
  },
  "trend_blocks": {
    "colourPrimary": "#0078a5",
    "colourSecondary": "#b5d5fb",
    "colourTertiary": "#dcf3fb"
  },
  "oth_blocks": {
    "colourPrimary": "#8e00a5",
    "colourSecondary": "#f8b1fb",
    "colourTertiary": "#f6e0fb"
  },
  "components_blocks": {
    "colourPrimary": "#a50053",
    "colourSecondary": "#fb82b5",
    "colourTertiary": "#fbe7ed"
  },
  "components_blocks_cap": {
    "colourPrimary": "#0004a5",
    "colourSecondary": "#9896fb",
    "colourTertiary": "#cdbcfb"
  },
  "components_blocks_main": {
    "colourPrimary": "#5700a5",
    "colourSecondary": "#d296fb",
    "colourTertiary": "#f2cafb"
  },
  "colour_blocks": {
    "colourPrimary": "#a52714",
    "colourSecondary": "#FB9B8C",
    "colourTertiary": "#FBE1DD"
  },
  "variable_blocks": {
    "colourPrimary": "#880e4f",
    "colourSecondary": "#FF73BE",
    "colourTertiary": "#FFD4EB"
  },

  "hat_blocks": {
    "colourPrimary": "#880e4f",
    "colourSecondary": "#FF73BE",
    "colourTertiary": "#FFD4EB",
    "hat": "cap"
  }
};

Blockly.Themes.HighContrast.categoryStyles = {
  "logic_category": {
    "colour": "#01579b"
  },
  "math_category": {
    "colour": "#1a237e"
  },
  "text_category": {
    "colour": "#004d40"
  },
  "variable_dynamic_category": {
    "colour": "#880e4f"
  },
  "time_category": {
    "colour": "#885811"
  },
  "procedure_category": {
    "colour": "#006064"
  },
  "loop_category": {
    "colour": "#33691e"
  },
  "list_category": {
    "colour": "#4a148c"
  },
  "price_category": {
    "colour": "#038c00"
  },
  "mm_category": {
    "colour": "#008c70"
  },
  "ind_category": {
    "colour": "#36008c"
  },
  "osc_category": {
    "colour": "#002f8c"
  },
  "trend_category": {
    "colour": "#8c0016"
  },
  "oth_category": {
    "colour": "#8c0067"
  },
  "components_category": {
    "colour": "#148c00"
  },
  "colour_category": {
    "colour": "#99a500"
  },
  "variable_category": {
    "colour": "#00885d"
  },

};

// This style is still being fleshed out and may change.
Blockly.Themes.HighContrast =
    new Blockly.Theme(Blockly.Themes.HighContrast.defaultBlockStyles,
        Blockly.Themes.HighContrast.categoryStyles);

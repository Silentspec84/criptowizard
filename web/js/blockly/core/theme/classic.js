/**
 * @license
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Classic theme.
 * Contains multi-coloured border to create shadow effect.
 */
'use strict';

goog.provide('Blockly.Themes.Classic');

goog.require('Blockly.Theme');


// Temporary holding object.
Blockly.Themes.Classic = {};

Blockly.Themes.Classic.defaultBlockStyles = {
    "logic_blocks": {
        "colourPrimary": "10"
    },
    "math_blocks": {
        "colourPrimary": "30"
    },
    "text_blocks": {
        "colourPrimary": "50"
    },
    "variable_dynamic_blocks": {
        "colourPrimary": "70"
    },
    "time_blocks": {
        "colourPrimary": "90"
    },
    "procedure_blocks": {
        "colourPrimary": "110"
    },
    "loop_blocks": {
        "colourPrimary": "130"
    },
    "list_blocks": {
        "colourPrimary": "150"
    },
    "price_blocks": {
        "colourPrimary": "170"
    },
    "mm_blocks": {
        "colourPrimary": "190"
    },
    "osc_blocks": {
        "colourPrimary": "230"
    },
    "trend_blocks": {
        "colourPrimary": "250"
    },
    "oth_blocks": {
        "colourPrimary": "270"
    },
    "components_blocks": {
        "colourPrimary": "290"
    },
    "components_blocks_cap": {
        "colourPrimary": "#5b67a5",
        "colourSecondary": "#bdc2db",
        "colourTertiary": "#495284",
        "hat": "cap"
    },
    "components_blocks_main": {
        "colourPrimary": "#a5745b",
        "colourSecondary": "#dbc7bd",
        "colourTertiary": "#845d49"
    },
  "colour_blocks": {
    "colourPrimary": "20"
  },
  "variable_blocks": {
    "colourPrimary": "330"
  },
  "hat_blocks": {
    "colourPrimary": "330",
    "hat": "cap"
  },

};

Blockly.Themes.Classic.categoryStyles = {
    "logic_category": {
        "colour": "10"
    },
    "math_category": {
        "colour": "30"
    },
    "text_category": {
        "colour": "50"
    },
    "variable_dynamic_category": {
        "colour": "70"
    },
    "time_category": {
        "colour": "90"
    },
    "procedure_category": {
        "colour": "110"
    },
    "loop_category": {
        "colour": "130"
    },
    "list_category": {
        "colour": "150"
    },
    "price_category": {
        "colour": "170"
    },
    "mm_category": {
        "colour": "190"
    },
    "ind_category": {
        "colour": "210"
    },
    "osc_category": {
        "colour": "230"
    },
    "trend_category": {
        "colour": "250"
    },
    "oth_category": {
        "colour": "270"
    },
    "components_category": {
        "colour": "290"
    },
  "colour_category": {
    "colour": "20"
  },
  "variable_category": {
    "colour": "330"
  }
};

Blockly.Themes.Classic =
    new Blockly.Theme(Blockly.Themes.Classic.defaultBlockStyles,
        Blockly.Themes.Classic.categoryStyles);

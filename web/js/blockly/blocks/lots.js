'use strict';

Blockly.Blocks['lots_min_lot'] = {
    /**
     * Выбор таймфрейма
     */
    init: function() {
        this.appendDummyInput()
            .appendField('Минимальный объем');
        this.setOutput(true, 'Number');
        this.setStyle('mm_blocks');
        this.setTooltip('Минимальный объем');
    },
};

Blockly.Blocks['lots_max_lot'] = {
    /**
     * Выбор таймфрейма
     */
    init: function() {
        this.appendDummyInput()
            .appendField('Максимальный объем');
        this.setOutput(true, 'Number');
        this.setStyle('mm_blocks');
        this.setTooltip('Максимальный значение');
    },
};

Blockly.Blocks['lots_lot_step'] = {
    /**
     * Выбор таймфрейма
     */
    init: function() {
        this.appendDummyInput()
            .appendField('Шаг приращения объема');
        this.setOutput(true, 'Number');
        this.setStyle('mm_blocks');
        this.setTooltip('Шаг приращения объема');
    },
};

Blockly.Blocks['lots_lot_fix'] = {
    /**
     * Выбор таймфрейма
     */
    init: function() {
        var balance_currency = new Blockly.FieldDropdown(config.balance_currency);
        this.appendValueInput('LOT_FIX')
            .appendField('Фикс объем сделки').setCheck('Number');
        this.appendDummyInput()
            .appendField('В валюте')
            .appendField(balance_currency, 'CURRENCY');
        this.setOutput(true, 'Number');
        this.setStyle('mm_blocks');
        this.setTooltip('Фиксированный объем сделки');
        this.setInputsInline(true);
    },
};

Blockly.Blocks['lots_lot_perc'] = {
    /**
     * Выбор таймфрейма
     */
    init: function() {
        var balance_currency = new Blockly.FieldDropdown(config.balance_currency);
        this.appendValueInput('LOT_PERC')
            .appendField('Объем сделки в %').setCheck('Number');
        this.appendDummyInput()
            .appendField('В валюте')
            .appendField(balance_currency, 'CURRENCY');
        this.setOutput(true, 'Number');
        this.setStyle('mm_blocks');
        this.setTooltip('Объем в % от валюты депозита');
        this.setInputsInline(true);
    },
};
'use strict';

Blockly.Blocks['prices_data'] = {
    /**
     * Выбор таймфрейма
     */
    init: function() {
        var prices_data = new Blockly.FieldDropdown(config.prices_data);
        var pairs = new Blockly.FieldDropdown(config.pairs);
        var TimeFrames = new Blockly.FieldDropdown(config.TimeFrames);
        this.appendDummyInput()
            .appendField('Массив рыночных данных')
            .appendField(prices_data, 'DATA');
        this.appendDummyInput()
            .appendField('по паре')
            .appendField(pairs, 'SYMBOL');
        this.appendDummyInput()
            .appendField('с периодом')
            .appendField(TimeFrames, 'TIMEFRAME');
        this.setOutput(true, 'Array');
        this.setStyle('price_blocks');
        this.setTooltip('Массив рыночных данных');
    },
};

Blockly.Blocks['prices_ask'] = {
    /**
     * Выбор таймфрейма
     */
    init: function() {
        var dom_data = new Blockly.FieldDropdown(config.dom_data);
        this.appendDummyInput()
            .appendField('Last Ask')
            .appendField(dom_data, 'DATA');
        this.setOutput(true, 'Number');
        this.setStyle('price_blocks');
        this.setTooltip('Последнее значение для лучшего Ask');
    },
};

Blockly.Blocks['prices_bid'] = {
    /**
     * Выбор таймфрейма
     */
    init: function() {
        var dom_data = new Blockly.FieldDropdown(config.dom_data);
        this.appendDummyInput()
            .appendField('Last Bid')
            .appendField(dom_data, 'DATA');
        this.setOutput(true, 'Number');
        this.setStyle('price_blocks');
        this.setTooltip('Последнее значение для лучшего Bid');
    },
};

Blockly.Blocks['prices_ask_depth_of_market'] = {
    /**
     * Выбор таймфрейма
     */
    init: function() {
        var dom_data = new Blockly.FieldDropdown(config.dom_data);
        this.appendDummyInput()
            .appendField('Asks DOM');
        this.appendDummyInput()
            .appendField('Тип данных')
            .appendField(dom_data, 'DATA');
        this.setOutput(true, 'Array');
        this.setStyle('price_blocks');
        this.setTooltip('Массив данных стакана со стороны покупок');
    },
};

Blockly.Blocks['prices_bid_depth_of_market'] = {
    /**
     * Выбор таймфрейма
     */
    init: function() {
        var dom_data = new Blockly.FieldDropdown(config.dom_data);
        this.appendDummyInput()
            .appendField('Bids DOM');
        this.appendDummyInput()
            .appendField('Тип данных')
            .appendField(dom_data, 'DATA');
        this.setOutput(true, 'Array');
        this.setStyle('price_blocks');
        this.setTooltip('Массив данных стакана со стороны продаж');
    },
};

Blockly.Blocks['prices_last_data'] = {
    /**
     * Выбор таймфрейма
     */
    init: function() {
        var pairs = new Blockly.FieldDropdown(config.pairs);
        var TimeFrames = new Blockly.FieldDropdown(config.TimeFrames);
        var prices_data = new Blockly.FieldDropdown(config.prices_data);
        this.appendDummyInput()
            .appendField('Последняя')
            .appendField(prices_data, 'DATA');
        this.appendDummyInput()
            .appendField('по паре')
            .appendField(pairs, 'SYMBOL');
        this.appendDummyInput()
            .appendField('с периодом')
            .appendField(TimeFrames, 'TIMEFRAME');
        this.setOutput(true, 'Number');
        this.setStyle('price_blocks');
        this.setTooltip('Последнее значение');
    },
};

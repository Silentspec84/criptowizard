'use strict';

Blockly.Blocks['time'] = {
    init: function() {
        this.appendDummyInput()
            .appendField('Текущее время');
        this.setOutput(true, 'Number');
        this.setStyle('time_blocks');
        this.setTooltip('Текущее время');
    },
};

Blockly.Blocks['day_of_week'] = {
    init: function() {
        this.appendDummyInput()
            .appendField('День недели');
        this.appendValueInput('DAY_OF_WEEK').setCheck('Number');
        this.setOutput(true, 'Number');
        this.setTooltip('День недели указанного времени');
        this.setStyle('time_blocks');
        this.setInputsInline(true);
    },
};

Blockly.Blocks['DST'] = {
    init: function() {
        this.appendDummyInput()
            .appendField('Daylight Saving Time (DST)');
        this.setOutput(true, 'Boolean');
        this.setStyle('time_blocks');
        this.setTooltip('Флаг перехода на летнее/зимнее время');
    },
};

Blockly.Blocks['day_of_month'] = {
    init: function() {
        this.appendDummyInput()
            .appendField('Число месяца');
        this.appendValueInput('DAY_OF_MONTH').setCheck('Number');
        this.setOutput(true, 'Number');
        this.setTooltip('Число месяца указанного времени');
        this.setStyle('time_blocks');
        this.setInputsInline(true);
    },
};

Blockly.Blocks['day_of_year'] = {
    init: function() {
        this.appendDummyInput()
            .appendField('День года');
        this.appendValueInput('DAY_OF_YEAR').setCheck('Number');
        this.setOutput(true, 'Number');
        this.setTooltip('Порядковый номер дня в году указанного времени');
        this.setStyle('time_blocks');
        this.setInputsInline(true);
    },
};

Blockly.Blocks['hour'] = {
    init: function() {
        this.appendDummyInput()
            .appendField('Текущий час');
        this.setOutput(true, 'Number');
        this.setStyle('time_blocks');
        this.setTooltip('Текущий час');
    },
};

Blockly.Blocks['minute'] = {
    init: function() {
        this.appendDummyInput()
            .appendField('Текущая минута');
        this.setOutput(true, 'Number');
        this.setStyle('time_blocks');
        this.setTooltip('Текущая минута');
    },
};

Blockly.Blocks['second'] = {
    init: function() {
        this.appendDummyInput()
            .appendField('Текущая секунда');
        this.setOutput(true, 'Number');
        this.setStyle('time_blocks');
        this.setTooltip('Текущая секунда');
    },
};

Blockly.Blocks['month'] = {
    init: function() {
        this.appendDummyInput()
            .appendField('Текущий месяц');
        this.setOutput(true, 'Number');
        this.setStyle('time_blocks');
        this.setTooltip('Текущий месяц');
    },
};

Blockly.Blocks['year'] = {
    init: function() {
        this.appendDummyInput()
            .appendField('Текущий год');
        this.setOutput(true, 'Number');
        this.setStyle('time_blocks');
        this.setTooltip('Текущий год');
    },
};
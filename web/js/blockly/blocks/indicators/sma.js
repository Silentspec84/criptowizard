'use strict';

Blockly.Blocks['sma'] = {
    init: function init() {
        this.appendDummyInput().appendField('Simple Moving Average Array');
        this.appendValueInput('INPUT').setCheck('Array').appendField('Input List');
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Period');
        this.setOutput(true, 'Array');
        this.setColour('#7f00ff');
        this.setTooltip('Calculates Simple Moving Average (SMA) list from a list of values with a period');
        this.setHelpUrl('https://github.com/binary-com/binary-bot/wiki');
    },
};

Blockly.JavaScript.sma = block => {
    const input = expectValue(block, 'INPUT');
    const period = Blockly.JavaScript.valueToCode(block, 'PERIOD', Blockly.JavaScript.ORDER_ATOMIC) || '10';
    return [`Bot.smaa(${input}, ${period})`, Blockly.JavaScript.ORDER_NONE];
};



// WEBPACK FOOTER //
// ./src/botPage/view/blockly/blocks/indicators/smaa.js
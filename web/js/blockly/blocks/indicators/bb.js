'use strict';
// https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#3qghes

Blockly.Blocks['bb'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Bollinger Bands Array')
            .appendField(new Blockly.FieldDropdown(config.bbResult), 'BBRESULT_LIST');
        this.appendValueInput('INPUT').setCheck('Array').appendField('Input List');
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Period');
        this.appendValueInput('UPMULTIPLIER').setCheck('Number').appendField('Std. Dev. Up Multiplier');
        this.appendValueInput('DOWNMULTIPLIER').setCheck('Number').appendField('Std. Dev. Down Multiplier');
        this.setOutput(true, 'Array');
        this.setColour('#7f00ff');
        this.setTooltip('Calculates Bollinger Bands (BB) list from a list with a period');
        this.setHelpUrl('https://github.com/binary-com/binary-bot/wiki');
    },
};

Blockly.JavaScript.bb = block => {
    const bbResult = block.getFieldValue('BBRESULT_LIST');
    const input = expectValue(block, 'INPUT');
    const period = Blockly.JavaScript.valueToCode(block, 'PERIOD', Blockly.JavaScript.ORDER_ATOMIC) || '10';
    const stdDevUp = Blockly.JavaScript.valueToCode(block, 'UPMULTIPLIER', Blockly.JavaScript.ORDER_ATOMIC) || '5';
    const stdDevDown = Blockly.JavaScript.valueToCode(block, 'DOWNMULTIPLIER', Blockly.JavaScript.ORDER_ATOMIC) || '5';
    return [
        `Bot.bba(${input}, {
    periods: ${period},
    stdDevUp: ${stdDevUp},
    stdDevDown: ${stdDevDown},
  }, ${bbResult})`,
        Blockly.JavaScript.ORDER_NONE,
    ];
};



// WEBPACK FOOTER //
// ./src/botPage/view/blockly/blocks/indicators/bba.js
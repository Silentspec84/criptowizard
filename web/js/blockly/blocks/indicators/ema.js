'use strict';

Blockly.Blocks['ema'] = {
    init: function init() {
        this.appendDummyInput().appendField('Exponential Moving Average Array');
        this.appendValueInput('INPUT').setCheck('Array').appendField('Input List');
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Period');
        this.setOutput(true, 'Array');
        this.setColour('#7f00ff');
        this.setTooltip(
            'Calculates Exponential Moving Average (EMA) list from a list of values with a period'
        );
        this.setHelpUrl('https://github.com/binary-com/binary-bot/wiki');
    },
};

Blockly.JavaScript.ema = block => {
    const input = expectValue(block, 'INPUT');
    const period = Blockly.JavaScript.valueToCode(block, 'PERIOD', Blockly.JavaScript.ORDER_ATOMIC) || '10';
    return [`Bot.emaa(${input}, ${period})`, Blockly.JavaScript.ORDER_NONE];
};



// WEBPACK FOOTER //
// ./src/botPage/view/blockly/blocks/indicators/emaa.js
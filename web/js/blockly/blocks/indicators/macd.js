'use strict';

Blockly.Blocks['macd'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('MACD Array')
            .appendField(new Blockly.FieldDropdown(config.macdFields), 'MACDFIELDS_LIST');
        this.appendValueInput('INPUT')
            .setCheck('Array')
            .appendField('Input List');
        this.appendValueInput('FAST_EMA_PERIOD')
            .setCheck('Number')
            .appendField('Fast EMA Period');
        this.appendValueInput('SLOW_EMA_PERIOD')
            .setCheck('Number')
            .appendField('Slow EMA Period');
        this.appendValueInput('SIGNAL_EMA_PERIOD')
            .setCheck('Number')
            .appendField('Signal EMA Period');
        this.setOutput(true, 'Array');
        this.setColour('#7f00ff');
        this.setTooltip('Calculates Moving Average Convergence Divergence (MACD) list from a list');
        this.setHelpUrl('https://github.com/binary-com/binary-bot/wiki');
    },
};

Blockly.JavaScript.macd = block => {
    const macdField = block.getFieldValue('MACDFIELDS_LIST');
    const input = expectValue(block, 'INPUT');
    const fastEmaPeriod =
        Blockly.JavaScript.valueToCode(block, 'FAST_EMA_PERIOD', Blockly.JavaScript.ORDER_ATOMIC) || '12';
    const slowEmaPeriod =
        Blockly.JavaScript.valueToCode(block, 'SLOW_EMA_PERIOD', Blockly.JavaScript.ORDER_ATOMIC) || '26';
    const signalEmaPeriod =
        Blockly.JavaScript.valueToCode(block, 'SIGNAL_EMA_PERIOD', Blockly.JavaScript.ORDER_ATOMIC) || '9';
    return [
        `Bot.macda(${input}, {
    fastEmaPeriod: ${fastEmaPeriod},
    slowEmaPeriod: ${slowEmaPeriod},
    signalEmaPeriod: ${signalEmaPeriod},
  }, ${macdField})`,
        Blockly.JavaScript.ORDER_NONE,
    ];
};



// WEBPACK FOOTER //
// ./src/botPage/view/blockly/blocks/indicators/macda.js
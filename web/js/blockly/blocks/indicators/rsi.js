'use strict';

Blockly.Blocks['rsi'] = {
    init: function init() {
        this.appendDummyInput().appendField('Relative Strength Index Array');
        this.appendValueInput('INPUT').setCheck('Array').appendField('Input List');
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Period');
        this.setOutput(true, 'Array');
        this.setColour('#7f00ff');
        this.setTooltip('Calculates Relative Strength Index (RSI) list from a list of values with a period');
        this.setHelpUrl('https://github.com/binary-com/binary-bot/wiki');
    },
};

Blockly.JavaScript.rsi = block => {
    const input = expectValue(block, 'INPUT');
    const period = Blockly.JavaScript.valueToCode(block, 'PERIOD', Blockly.JavaScript.ORDER_ATOMIC) || '14';
    return [`Bot.rsia(${input}, ${period})`, Blockly.JavaScript.ORDER_NONE];
};



// WEBPACK FOOTER //
// ./src/botPage/view/blockly/blocks/indicators/rsia.js
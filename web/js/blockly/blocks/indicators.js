'use strict';

// Bollinger Bands
Blockly.Blocks['iBands'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Bollinger Bands');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendDummyInput()
            .appendField('Индекс линии')
            .appendField(new Blockly.FieldDropdown(config.BBMode), 'BBMODE');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        // кол-во стандартных отклонений
        this.appendValueInput('UPMULTIPLIER').setCheck('Number').appendField('Std. Dev. Up Multiplier');
        this.appendValueInput('DOWNMULTIPLIER').setCheck('Number').appendField('Std. Dev. Down Multiplier');
        // сдвиг относительно цены
        this.appendValueInput('BANDS_SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Bollinger Bands');
        this.setStyle('trend_blocks');
        this.setInputsInline(false);
    },
};

// Blockly.JavaScript.bb = block => {
//     const bbResult = block.getFieldValue('BBRESULT_LIST');
//     const input = expectValue(block, 'INPUT');
//     const period = Blockly.JavaScript.valueToCode(block, 'PERIOD', Blockly.JavaScript.ORDER_ATOMIC) || '10';
//     const stdDevUp = Blockly.JavaScript.valueToCode(block, 'UPMULTIPLIER', Blockly.JavaScript.ORDER_ATOMIC) || '5';
//     const stdDevDown = Blockly.JavaScript.valueToCode(block, 'DOWNMULTIPLIER', Blockly.JavaScript.ORDER_ATOMIC) || '5';
//     return [
//         `Bot.bb(${input}, {
//     periods: ${period},
//     stdDevUp: ${stdDevUp},
//     stdDevDown: ${stdDevDown},
//   }, ${bbResult})`,
//         Blockly.JavaScript.ORDER_NONE,
//     ];
// };

// Accelerator/Decelerator
Blockly.Blocks['iAC'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Accelerator/Decelerator');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // сдвиг относительно цены
        this.appendValueInput('AC_SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Accelerator/Decelerator');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Accumulation/Distribution
Blockly.Blocks['iAD'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Accumulation/Distribution');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // сдвиг относительно цены
        this.appendValueInput('AD_SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Accumulation/Distribution');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Average Directional Movement Index
Blockly.Blocks['iADX'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Average Directional Movement Index');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendDummyInput()
            .appendField('Индекс линии')
            .appendField(new Blockly.FieldDropdown(config.ADXMode), 'BBMODE');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        // сдвиг относительно цены
        this.appendValueInput('ADX_SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Average Directional Movement Index');
        this.setStyle('trend_blocks');
        this.setInputsInline(false);
    },
};

// Alligator
Blockly.Blocks['Alligator'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Alligator');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendDummyInput()
            .appendField('Индекс линии')
            .appendField(new Blockly.FieldDropdown(config.AlligatorMode), 'BBMODE');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // jaw_period
        this.appendValueInput('JAWS_PERIOD').setCheck('Number').appendField('Период челюстей');
        // jaw_shift
        this.appendValueInput('JAWS_SHIFT').setCheck('Number').appendField('Сдвиг челюстей');
        // teeth_period
        this.appendValueInput('TEETH_PERIOD').setCheck('Number').appendField('Период зубов');
        // teeth_shift
        this.appendValueInput('TEETH_SHIFT').setCheck('Number').appendField('Сдвиг зубов');
        // lips_period
        this.appendValueInput('LIPS_PERIOD').setCheck('Number').appendField('Период губ');
        // lips_shift
        this.appendValueInput('LIPS_SHIFT').setCheck('Number').appendField('Сдвиг губ');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'BBMODE');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Alligator');
        this.setStyle('trend_blocks');
        this.setInputsInline(false);
    },
};

// Awesome Oscillator
Blockly.Blocks['iAO'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Awesome Oscillator');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // сдвиг относительно цены
        this.appendValueInput('AD_SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Awesome Oscillator');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Average True Range
Blockly.Blocks['iATR'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Average True Range');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        // сдвиг относительно цены
        this.appendValueInput('ATR_SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Average True Range');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Bears Power
Blockly.Blocks['iBearsPower'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Bears Power');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'BBMODE');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Bears Power');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Bulls Power
Blockly.Blocks['iBullsPower'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Bulls Power');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'BBMODE');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Bulls Power');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Commodity Channel Index
Blockly.Blocks['iCCI'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Commodity Channel Index');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'BBMODE');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Commodity Channel Index');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// DeMarker
Blockly.Blocks['iDeMarker'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('DeMarker');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'BBMODE');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора DeMarker');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Envelopes
Blockly.Blocks['iEnvelopes'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Envelopes');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        // Отклонение (в процентах)
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Отклонение, %');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        this.appendDummyInput()
            .appendField('Индекс линии')
            .appendField(new Blockly.FieldDropdown(config.EnvelopesMode), 'MODE');
        this.setOutput(true, 'Array');
        this.setStyle('trend_blocks');
        this.setTooltip('Рассчет индикатора Envelopes');
        this.setInputsInline(false);
    },
};

// Force Index
Blockly.Blocks['iForce'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Force Index');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Force Index');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Fractals
Blockly.Blocks['iFractals'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Fractals');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.appendDummyInput()
            .appendField('Индекс линии')
            .appendField(new Blockly.FieldDropdown(config.FractalMode), 'MODE');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Fractals');
        this.setStyle('oth_blocks');
        this.setInputsInline(false);
    },
};

// Ichimoku Kinko Hyo
Blockly.Blocks['iIchimoku'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Ichimoku Kinko Hyo');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период Tenkan-sen
        this.appendValueInput('PERIOD_TS').setCheck('Number').appendField('Период Tenkan-sen');
        // Период Kijun-sen
        this.appendValueInput('PERIOD_KS').setCheck('Number').appendField('Период Kijun-sen');
        // Период Senkou Span B
        this.appendValueInput('PERIOD_SS').setCheck('Number').appendField('Период Senkou Span B');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        this.appendDummyInput()
            .appendField('Индекс линии')
            .appendField(new Blockly.FieldDropdown(config.IchimokuMode), 'MODE');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Ichimoku Kinko Hyo');
        this.setStyle('trend_blocks');
        this.setInputsInline(false);
    },
};

// Market Facilitation Index
Blockly.Blocks['iBWMFI'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Market Facilitation Index');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Market Facilitation Index');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Momentum
Blockly.Blocks['iMomentum'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Momentum');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Momentum');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Money Flow Index
Blockly.Blocks['iMFI'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Money Flow Index');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Money Flow Index');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Moving Average
Blockly.Blocks['iMA'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Moving Average');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Moving Average');
        this.setStyle('trend_blocks');
        this.setInputsInline(false);
    },
};

// Moving Average of Oscillator
Blockly.Blocks['iOsMA'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Moving Average of Oscillator');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период быстрой средней
        this.appendValueInput('PERIOD_FAST').setCheck('Number').appendField('Период быстрой средней');
        // Период медленной средней
        this.appendValueInput('PERIOD_SLOW').setCheck('Number').appendField('Период медленной средней');
        // Период сигнальной линии
        this.appendValueInput('PERIOD_SIGNAL').setCheck('Number').appendField('Период сигнальной линии');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Moving Average of Oscillator');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Moving Averages Convergence/Divergence
Blockly.Blocks['iMACD'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('MACD');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период быстрой средней
        this.appendValueInput('PERIOD_FAST').setCheck('Number').appendField('Период быстрой средней');
        // Период медленной средней
        this.appendValueInput('PERIOD_SLOW').setCheck('Number').appendField('Период медленной средней');
        // Период сигнальной линии
        this.appendValueInput('PERIOD_SIGNAL').setCheck('Number').appendField('Период сигнальной линии');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Moving Averages Convergence/Divergence');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// On Balance Volume
Blockly.Blocks['iOBV'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('On Balance Volume');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора On Balance Volume');
        this.setStyle('oth_blocks');
        this.setInputsInline(false);
    },
};

// Parabolic Stop and Reverse system
Blockly.Blocks['iSAR'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Parabolic Stop and Reverse system');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // step
        this.appendValueInput('STEP').setCheck('Number').appendField('Шаг изменения цены');
        // maximum
        this.appendValueInput('STEP_MAX').setCheck('Number').appendField('Максимальный шаг');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Parabolic Stop and Reverse system');
        this.setStyle('trend_blocks');
        this.setInputsInline(false);
    },
};

// Relative Strength Index
Blockly.Blocks['iRSI'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Relative Strength Index');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Relative Strength Index');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Relative Vigor Index
Blockly.Blocks['iRVI'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Relative Vigor Index');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.appendDummyInput()
            .appendField('Индекс линии')
            .appendField(new Blockly.FieldDropdown(config.RviMode), 'MODE');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Relative Vigor Index');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Standard Deviation
Blockly.Blocks['iStdDev'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Standard Deviation');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Standard Deviation');
        this.setStyle('oth_blocks');
        this.setInputsInline(false);
    },
};

// Stochastic Oscillator
Blockly.Blocks['iStochastic'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Stochastic Oscillator');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период линии K
        this.appendValueInput('PERIOD_K').setCheck('Number').appendField('Период линии K');
        // Период линии D
        this.appendValueInput('PERIOD_D').setCheck('Number').appendField('Период линии D');
        // Замедление
        this.appendValueInput('PERIOD_SLOW').setCheck('Number').appendField('Замедление');
        this.appendDummyInput()
            .appendField('Тип цены')
            .appendField(new Blockly.FieldDropdown(config.PriceFieldStoch), 'PRICE_FIELD');
        this.appendDummyInput()
            .appendField('Индекс линии')
            .appendField(new Blockly.FieldDropdown(config.ModeStoch), 'MODE');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Stochastic Oscillator');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};

// Larry Williams' Percent Range
Blockly.Blocks['iWPR'] = {
    init: function init() {
        this.appendDummyInput()
            .appendField('Larry Williams Percent Range');
        // имя символа
        this.appendValueInput('SYMBOL').setCheck('String').appendField('Инструмент');
        // таймфрейм
        this.appendValueInput('TIMEFRAME').setCheck('String').appendField('Таймфрейм');
        this.appendValueInput('PRICE_TYPE').setCheck('String').appendField('Ценовые данные');
        // Период
        this.appendValueInput('PERIOD').setCheck('Number').appendField('Период');
        this.appendDummyInput()
            .appendField('Тип сглаживания')
            .appendField(new Blockly.FieldDropdown(config.MaMethod), 'METHOD');
        // Сдвиг
        this.appendValueInput('SHIFT').setCheck('Number').appendField('Сдвиг');
        this.setOutput(true, 'Array');
        this.setTooltip('Рассчет индикатора Larry Williams Percent Range');
        this.setStyle('osc_blocks');
        this.setInputsInline(false);
    },
};
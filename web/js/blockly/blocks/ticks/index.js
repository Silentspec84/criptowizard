import './tick';
import './ticks';
import './ohlc';
import './ohlc_values';
import './read_ohlc';
import './get_ohlc';
import './check_direction';
import './direction';
import './tick_analysis';
import './last_digit';
import './last_digit_list';



// WEBPACK FOOTER //
// ./src/botPage/view/blockly/blocks/ticks/index.js
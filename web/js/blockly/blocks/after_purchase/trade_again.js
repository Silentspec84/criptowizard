'use strict';

// https://blockly-demo.appspot.com/static/demos/blockfactory/index.html#xkasg4

Blockly.Blocks['trade_again'] = {
    init: function init() {
        this.appendDummyInput().appendField('Trade Again');
        this.setPreviousStatement(true, 'TradeAgain');
        this.setColour('#f2f2f2');
        this.setTooltip('Runs the trade block again');
        this.setHelpUrl('');
    },
    onchange: function onchange(ev) {

    }
};



// WEBPACK FOOTER //
// ./src/botPage/view/blockly/blocks/after_purchase/trade_again.js
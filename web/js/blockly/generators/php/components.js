'use strict';

goog.provide('Blockly.PHP.components');

goog.require('Blockly.PHP');

Blockly.PHP['components_period'] = function(block) {
    const timeframe = block.getFieldValue('PERIOD');
    let code = '';
    if (timeframe === 'default') {
        code = '$period = 0';
    } else {
        code = '$period = ' + timeframe;
    }
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};

// Blockly.Blocks['components_pairs'] = {
//     /**
//      * Выбор таймфрейма
//      */
//     init: function() {
//         var pairs = new Blockly.FieldDropdown(config.pairs);
//         this.appendDummyInput()
//             .appendField('Валютная пара')
//             .appendField(pairs, 'PAIR');
//         this.setOutput(true, 'String');
//         this.setStyle('components_blocks');
//         this.setTooltip('Выбор инструмента');
//     },
// };

Blockly.PHP['components_pairs'] = function(block) {
    let code = '';
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};

// Blockly.Blocks['components_price_types'] = {
//     /**
//      * Выбор таймфрейма
//      */
//     init: function() {
//         var price_types = new Blockly.FieldDropdown(config.price_types);
//         this.appendDummyInput()
//             .appendField('Тип ценовых данных')
//             .appendField(price_types, 'PRICE_TYPE');
//         this.setOutput(true, 'String');
//         this.setStyle('components_blocks');
//         this.setTooltip('Цены для рассчетов');
//     },
// };

Blockly.PHP['components_price_types'] = function(block) {
    let code = '';
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};

// Blockly.Blocks['components_notify'] = {
//     /**
//      * Выбор таймфрейма
//      */
//     init: function() {
//         var notify_src = new Blockly.FieldDropdown(config.notify_src);
//         this.appendDummyInput()
//             .appendField('Уведомления')
//             .appendField(notify_src, 'NOTIFY_SRC');
//         this.setOutput(true, 'String');
//         this.setStyle('components_blocks');
//         this.setTooltip('Уведомления на контакты, указанные в личном кабинете');
//     },
// };

Blockly.PHP['components_notify'] = function(block) {
    let code = '';
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};

// Blockly.Blocks['components_balance'] = {
// //     /**
// //      * Выбор таймфрейма
// //      */
// //     init: function() {
// //         var balance_currency = new Blockly.FieldDropdown(config.balance_currency);
// //         this.appendDummyInput()
// //             .appendField('Баланс на счете в')
// //             .appendField(balance_currency, 'BALANCE');
// //         this.setOutput(true, 'Number');
// //         this.setStyle('components_blocks');
// //         this.setTooltip('Величина баланса по указанной валюте');
// //     },
// // };

Blockly.PHP['components_balance'] = function(block) {
    let code = '';
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};

// Blockly.Blocks['components_prices_deals'] = {
//     /**
//      * Выбор таймфрейма
//      */
//     init: function() {
//
//         var order_mode = new Blockly.FieldDropdown(config.order_mode);
//         var pairs = new Blockly.FieldDropdown(config.pairs);
//         var order_data = new Blockly.FieldDropdown(config.order_data);
//         var order_type = new Blockly.FieldDropdown(config.order_type);
//
//         this.appendDummyInput()
//             .appendField('Массив')
//             .appendField(order_data, 'ORDER_DATA');
//         this.appendDummyInput()
//             .appendField('по')
//             .appendField(order_mode, 'ORDER_MODE');
//         this.appendDummyInput()
//             .appendField(order_type, 'ORDER_TYPE')
//             .appendField('ордерам');
//         this.appendDummyInput()
//             .appendField('по паре')
//             .appendField(pairs, 'SYMBOL');
//         this.setOutput(true, 'Array');
//         this.setStyle('components_blocks');
//         this.setTooltip('Массив данных по ордерам');
//     },
// };

Blockly.PHP['components_prices_deals'] = function(block) {
    let code = '';
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};

// Blockly.Blocks['components_stop_all'] = {
//     /**
//      * Выбор таймфрейма
//      */
//     init: function() {
//         this.appendDummyInput()
//             .appendField('СТОП');
//         this.setPreviousStatement(true);
//         this.setStyle('components_blocks');
//         this.setTooltip('Полная остановка работы');
//     },
// };

Blockly.PHP['components_stop_all'] = function(block) {
    let code = '';
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};

// Blockly.Blocks['components_main'] = {
//
//     init: function() {
//
//         var markets = new Blockly.FieldDropdown(config.markets);
//         var brokers = new Blockly.FieldDropdown(config.brokers);
//         var pairs_main = new Blockly.FieldDropdown(config.pairs_main);
//         var timeframes = new Blockly.FieldDropdown(config.timeframes_no_default);
//
//         this.setStyle('components_blocks_cap');
//
//         this.appendDummyInput()
//             .appendField('Каркас стратегии');
//
//         this.appendDummyInput()
//             .appendField('Рынок')
//             .appendField(markets, 'MARKET')
//             .appendField('Брокер')
//             .appendField(brokers, 'BROKER')
//             .appendField('Инструмент')
//             .appendField(pairs_main, 'PAIR');
//
//         this.appendDummyInput()
//             .appendField('Основной таймфрейм')
//             .appendField(timeframes, 'TIMEFRAME');
//
//         this.appendDummyInput()
//             .appendField('Перезапускать при ошибке исполнения')
//             .appendField(new Blockly.FieldCheckbox('TRUE'), 'RELOAD_ENABLED');
//
//         this.appendDummyInput()
//             .appendField('Тело стратегии');
//
//         this.appendStatementInput('InitValues')
//             .appendField('Инициализация переменных');
//
//         this.appendStatementInput('SignalValues')
//             .appendField('Блоки генерации сигналов');
//
//         this.appendStatementInput('SignalValues')
//             .appendField('Блоки исполнения сигналов');
//
//     },
// };

Blockly.PHP['components_main'] = function(block) {
    let code = '';



    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};

// Blockly.Blocks['components_init'] = {
//     init: function() {
//         this.setStyle('components_blocks_main');
//         this.setPreviousStatement(true);
//         this.appendDummyInput()
//             .appendField('Блок инициализации переменных');
//         this.appendStatementInput('InitValues')
//             .appendField('Переменные:');
//         this.setTooltip('Блок инициализации переменных (периоды индикаторов и прочие настройки)');
//     },
// };

Blockly.PHP['components_init'] = function(block) {
    let code = '';
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};

// Blockly.Blocks['components_signal'] = {
//     init: function() {
//         this.setStyle('components_blocks_main');
//         this.setPreviousStatement(true);
//         this.appendDummyInput()
//             .appendField('Блок генерации сигнала');
//         this.appendValueInput('SIGNAL_NAME').setCheck('String').appendField('Название');
//         this.appendStatementInput('Signal')
//             .appendField('Условие появления');
//         this.setTooltip('Блок генерации сигнала');
//         this.setNextStatement(true);
//         this.setInputsInline(true);
//     },
// };

Blockly.PHP['components_signal'] = function(block) {
    let code = '';
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};

// Blockly.Blocks['components_action'] = {
//     init: function() {
//         this.setStyle('components_blocks_main');
//         this.setPreviousStatement(true);
//         this.appendDummyInput()
//             .appendField('Блок исполнения сигнала');
//         this.appendValueInput('SIGNAL_NAME').setCheck('String').appendField('Название');
//         this.appendStatementInput('Action')
//             .appendField('Действие при исполнении сигнала');
//         this.setTooltip('Блок исполнения сигнала');
//         this.setNextStatement(true);
//         this.setInputsInline(true);
//     },
// };

Blockly.PHP['components_action'] = function(block) {
    let code = '';
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};

// Blockly.Blocks['components_return_signal_result'] = {
//     /**
//      * Выбор таймфрейма
//      */
//     init: function() {
//         this.appendValueInput('SIGNAL_RESULT').setCheck('Boolean')
//             .appendField('return signal result (true/false)');
//         this.setPreviousStatement(true);
//         this.setStyle('components_blocks_main');
//         this.setTooltip('Возвращает результат (true - сигнал, false - нет)');
//     },
// };

Blockly.PHP['components_return_signal_result'] = function(block) {
    let code = '';
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};
//
// Blockly.Blocks['components_trade'] = {
//     init: function() {
//
//         var order_type = new Blockly.FieldDropdown(config.order_type);
//         var order_side = new Blockly.FieldDropdown(config.order_side);
//
//         this.setStyle('components_blocks_main');
//         this.setPreviousStatement(true);
//         this.appendDummyInput()
//             .appendField('Открытие ордера');
//         this.appendDummyInput()
//             .appendField('Сторона ордера')
//             .appendField(order_side, 'ORDER_SIDE');
//         this.appendDummyInput()
//             .appendField('Тип ордера')
//             .appendField(order_type, 'ORDER_TYPE');
//         this.appendValueInput('PRICE').setCheck('Number').appendField('Цена');
//         this.appendValueInput('VOLUME').setCheck('Number').appendField('Объем');
//         this.setTooltip('Блок открытия позиции');
//         this.setNextStatement(true);
//     },
// };

Blockly.PHP['components_trade'] = function(block) {
    let code = '';
    return [code, Blockly.PHP.ORDER_ASSIGNMENT];
};
const config = {

    BBMode  : [['main', '0'], ['upper', '1'], ['lower', '2']],
    ADXMode  : [['main', '0'], ['+DI', '1'], ['-DI', '2']],
    FractalMode : [['upper', '1'], ['lower', '2']],
    RviMode : [['main', '0'], ['signal', '1']],
    IchimokuMode : [['Tenkan-sen', '1'], ['Kijun-sen', '2'], ['Senkou Span A', '3'], ['Senkou Span B', '4'], ['Chikou Span', '5']],
    AlligatorMode : [['GatorJaws', '0'], ['GatorTeeth', '1'], ['GatorLips', '2']],
    EnvelopesMode : [['main', '0'], ['upper', '1'], ['lower', '2']],
    ModeStoch : [['main', '0'], ['signal', '1']],

    ModeMath : [['Максимальное из', 'MATH_MAX'], ['Минимальное из', 'MATH_MIN']],

    order_mode : [['открытым', 'OPENED'], ['закрытым', 'CLOSED']],

    order_data : [['id', 'ID'], ['цен', 'PRICES'], ['объемов', 'VOLUMES']],

    order_side : [['Покупка', 'BUY'], ['Продажа', 'SELL']],

    order_type : [['LIMIT', 'LIMIT'], ['MARKET', 'MARKET'], ['STOP_LOSS', 'STOP_LOSS'],
        ['STOP_LOSS_LIMIT', 'STOP_LOSS_LIMIT'], ['TAKE_PROFIT', 'TAKE_PROFIT'],
        ['TAKE_PROFIT_LIMIT', 'TAKE_PROFIT_LIMIT']],

    notify_src: [['На email', 'EMAIL'], ['В канал TG', 'TG'], ['СМС', 'SMS']],

    balance_currency: [['BTC', 'BTC'], ['ETH', 'ETH'], ['LTC', 'LTC'], ['BCH', 'BCH'], ['USDT', 'USDT']],

    prices_data: [['Open', 'OPEN'], ['High', 'HIGH'], ['Low', 'LOW'], ['Close', 'CLOSE'],
        ['Base Volume', 'BASE_VOLUME'], ['Quote Volume', 'QUOTE_VOLUME'], ['Num of trades', 'TRADES']],

    dom_data : [['Price', 'PRICE'], ['Base Volume', 'BASE_VOLUME'], ['Quote Volume', 'QUOTE_VOLUME']],

    TimeFrames: [
        ['Default', 'default'], ['1 минута', '1'], ['2 минуты', '2'],
        ['3 минуты', '3'], ['4 минуты', '4'], ['5 минут', '5'],
        ['6 минут', '6'], ['8 минут', '8'], ['10 минут', '10'],
        ['12 минут', '12'], ['15 минут', '15'], ['20 минут', '20'],
        ['30 минут', '30'], ['1 час', '60'], ['2 часа', '120'],
        ['3 часа', '180'], ['4 часа', '240'], ['5 часов', '300'],
        ['6 часов', '360'], ['8 часов', '480'], ['10 часов', '600'],
        ['12 часов', '720'], ['1 день', '1440'], ['2 дня', '2880'],
        ['3 дня', '4320'], ['4 дня', '5720'], ['5 дней', '7200'],
        ['8 дней', '11520'], ['10 дней', '14400'], ['15 дней', '21600'],
        ['20 дней', '28800'], ['1 месяц', '43200'], ['2 месяца', '86400'],
        ['3 месяца', '129600'], ['4 месяца', '172800'], ['6 месяцев', '259200'],
        ['12 месяцев', '518400'],
    ],

    timeframes_no_default: [
        ['1 минута', '1'], ['2 минуты', '2'],
        ['3 минуты', '3'], ['4 минуты', '4'], ['5 минут', '5'],
        ['6 минут', '6'], ['8 минут', '8'], ['10 минут', '10'],
        ['12 минут', '12'], ['15 минут', '15'], ['20 минут', '20'],
        ['30 минут', '30'], ['1 час', '60'], ['2 часа', '120'],
        ['3 часа', '180'], ['4 часа', '240'], ['5 часов', '300'],
        ['6 часов', '360'], ['8 часов', '480'], ['10 часов', '600'],
        ['12 часов', '720'], ['1 день', '1440'], ['2 дня', '2880'],
        ['3 дня', '4320'], ['4 дня', '5720'], ['5 дней', '7200'],
        ['8 дней', '11520'], ['10 дней', '14400'], ['15 дней', '21600'],
        ['20 дней', '28800'], ['1 месяц', '43200'], ['2 месяца', '86400'],
        ['3 месяца', '129600'], ['4 месяца', '172800'], ['6 месяцев', '259200'],
        ['12 месяцев', '518400'],
    ],

    pairs: [['По умолчанию', 'NULL'], ['ETH/BTC', 'ETH_BTC'], ['VET/BTC', 'VET_BTC'], ['BNB/BTC', 'BNB_BTC'],
        ['LINK/BTC', 'LINK_BTC'], ['XRP/BTC', 'XRP_BTC'], ['EOS/BTC', 'EOS_BTC'],
        ['LTC/BTC', 'LTC_BTC'], ['BTC/USDT', 'BTC_USDT'], ['ETH/USDT', 'ETH_USDT'],
        ['XRP/USDT', 'XRP_USDT'], ['EOS/USDT', 'EOS_USDT'], ['BNB/USDT', 'BNB_USDT'],
        ['VET/USDT', 'VET_USDT'], ['LTC/USDT', 'LTC_USDT'], ['LINK/USDT', 'LINK_USDT']],

    price_types: [['Цена закрытия', 'PRICE_CLOSE'], ['Цена открытия', 'PRICE_OPEN'], ['Максимальная цена', 'PRICE_HIGH'],
        ['Минимальная цена', 'PRICE_LOW'], ['Медианная цена, (high+low)/2', 'PRICE_MEDIAN'],
        ['Типичная цена, (high+low+close)/3', 'PRICE_TYPICAL'], ['Взвешенная цена закрытия, (high+low+close+close)/4', 'PRICE_WEIGHTED']],

    MaMethod: [['Простое усреднение', 'MODE_SMA'], ['Экспоненциальное усреднение', 'MODE_EMA'],
        ['Сглаженное усреднение', 'MODE_SMMA'], ['Линейно-взвешенное усреднение', 'MODE_LWMA']],

    PriceFieldStoch: [['Low/High', '0'], ['Close/Close', '1']],

    markets: [['crypto', 'CRYPTO'], ['forex', 'FOREX'], ['moex', 'MOEX']],

    brokers: [['binance', 'BINANCE'], ['exmo', 'EXMO'], ['bittrex', 'BITTREX'], ['HitBTC', 'HITBTC']],

    pairs_main: [['ETH/BTC', 'ETH_BTC'], ['VET/BTC', 'VET_BTC'], ['BNB/BTC', 'BNB_BTC'],
        ['LINK/BTC', 'LINK_BTC'], ['XRP/BTC', 'XRP_BTC'], ['EOS/BTC', 'EOS_BTC'],
        ['LTC/BTC', 'LTC_BTC'], ['BTC/USDT', 'BTC_USDT'], ['ETH/USDT', 'ETH_USDT'],
        ['XRP/USDT', 'XRP_USDT'], ['EOS/USDT', 'EOS_USDT'], ['BNB/USDT', 'BNB_USDT'],
        ['VET/USDT', 'VET_USDT'], ['LTC/USDT', 'LTC_USDT'], ['LINK/USDT', 'LINK_USDT']],
};
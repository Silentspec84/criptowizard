<?php


namespace app\commands;


use app\classes\connectors\ApiConnectorInterface;
use app\models\Daemon;
use app\models\Exchange;
use app\models\Instrument;
use app\models\Logs;
use Yii;
use yii\console\Controller;

class DaemonPairsController extends Controller
{

    /** @var Logs $logger  */
    private $logger = null;

    /** @var Exchange $exchange  */
    private $exchange = null;

    /** @var ApiConnectorInterface $api  */
    private $api = null;

    public function beforeAction($action)
    {
        $exchange_id = Yii::$app->request->params[1];
        $this->exchange = Exchange::findOne($exchange_id);
        $this->logger = Logs::getLogger($this->exchange->id, Logs::LOAD_PAIRS, true);
        $this->api = new $this->exchange->class($this->logger);
        return parent::beforeAction($action);
    }

    /**
     * Загружает по апи пары и сохраняет их в бд
     * @param $id - id биржи
     */
    public function actionRun($id)
    {
        $this->logger->saveMessage('Удаляем существующие инструменты');
        if (Instrument::deleteAll(['exchange_id' => $id]) < 0) {
            $this->logger->saveMessage('Не удалось удалить существующие инструменты');
            return;
        }

        $this->logger->saveMessage('Получаем валютные пары по апи');
        $pairs = $this->api->getPairsSettingsArray();

        if (!$pairs) {
            $this->logger->saveMessage('Данные по настройкам пар не были получены');
            return;
        }

        $this->logger->saveMessage('Сохраняем данные по настройкам пар');
        if (!Instrument::saveInstrumentsSettings($pairs)) {
            $this->logger->saveMessage('Данные по настройкам пар не были сохранены');
            return;
        }
        $this->logger->saveMessage('Данные по настройкам пар успешно сохранены');
    }
}
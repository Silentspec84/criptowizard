<?php

use app\assets\BalancesAsset;

BalancesAsset::register($this);
?>


<div class="container-fluid" id="balances" ref="config"
     data-csrf = '<?= Yii::$app->request->csrfToken?>'
     data-id = '<?=json_encode(!empty($id) ? $id : 0)?>'
>

    <div class="table-responsive mb-auto">
        <table class="table table-striped table-bordered table-hover">
            <thead class="thead-dark">
            <tr>
                <th class="text-center" scope="col">
                    ID
                </th>
                <th class="text-center" scope="col">
                    Биржа
                </th>
                <th class="text-center" scope="col">
                    Валюта
                </th>
                <th class="text-center" scope="col">
                    Минимальный объем
                </th>
                <th class="text-center" scope="col">
                    Депозит
                </th>
                <th class="text-center" scope="col">
                    Статус
                </th>
                <th class="text-center" scope="col">
                    Действия
                </th>
            </tr>
            </thead>
            <tbody>
            <tr v-show="!loading" v-for="balance, index in balances">
                <td class="text-center align-middle">{{balance.balance_data.id}}</td>
                <td class="text-center align-middle">{{balance.exchange_name}}</td>
                <td class="text-center align-middle">{{balance.balance_data.currency}}</td>
                <td class="text-center align-middle">
                    <button type="button" class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{balance.balance_data.min_volume}}
                    </button>
                    <div class="dropdown-menu p-2">
                        <input  type="number" data-toggle="tooltip" data-placement="right" placeholder="Минимальный объем" data-original-title="" title=""
                                class="form-control col-12 dropdown-item"
                                v-bind:data-original-title="getError('min_volume')"
                                v-bind:class="getError('min_volume') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                v-model="new_min_volume"/>
                        <button type="submit" class="btn btn-outline-secondary btn-block" v-on:click="saveMinVolume(balance, index)">Сохранить</button>
                    </div>
                </td>


                <td class="text-center align-middle">{{balance.balance_data.deposit}}</td>
                <td class="text-center align-middle">
                    <button v-show="balance.balance_data.status == 1" type="button" class="btn btn-outline-success btn-sm p-0" v-on:click="switchCurrency(index)">Включен</button>
                    <button v-show="balance.balance_data.status == 0" type="button" class="btn btn-outline-danger btn-sm p-0" v-on:click="switchCurrency(index)">Отключен</button>
                </td>
                <td class="text-center">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <!--                            <button class="dropdown-item" type="button" >Редактировать биржу</button>-->
                            <!--                            <button class="dropdown-item" type="button" >Загрузить пары с биржи</button>-->
                            <!--                            <button class="dropdown-item" type="button">Управление парами</button>-->
                            <!--                            <button class="dropdown-item" type="button" >Просчитать цепочки</button>-->
                            <!--                            <button class="dropdown-item" type="button">Управление цепочками</button>-->
                            <!--                            <button class="dropdown-item" type="button" >Обновить балансы</button>-->
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>



<?php

use app\assets\DealsAsset;
use app\assets\ExchangesAsset;
use app\controllers\DealsApiController;

DealsAsset::register($this);
?>

<div class="container-fluid" id="deals" ref="config"
     data-csrf='<?= Yii::$app->request->csrfToken?>'>

    <div id="filters_panel fixed-top">
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <div class="row">
                            <div class="col-3">
                                <button class="btn btn-outline-primary btn-block" type="button" data-toggle="collapse"
                                        data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fas fa-chevron-circle-down mr-2"></i>Поиск и фильтрация
                                </button>
                            </div>
                            <div class="col-3">
                                <button v-if="filter_form" class="btn btn-outline-secondary" type="button" v-on:click="clearForm()">
                                    <i class="fas fa-ban mr-2"></i>Очистить форму
                                </button>
                            </div>
                            <div class="col-3">
                            </div>
                            <div class="col-3">
                            </div>
                        </div>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">@ поиск по бирже</span>
                            </div>
                            <select class="custom-select form-control mx-auto" v-model="filters.selected_exchange">
                                <option v-for="exchange in exchanges" v-bind:value="exchange">
                                    {{ exchange }}
                                </option>
                            </select>
                            <div class="input-group-prepend">
                                <span class="input-group-text">@ поиск по валюте</span>
                            </div>
                            <select class="custom-select form-control mx-auto" v-model="filters.selected_currency">
                                <option v-for="currency in currencies" v-bind:value="currency">
                                    {{ currency }}
                                </option>
                            </select>
                        </div>
                        <br>
                        <button class="btn btn-outline-primary" type="button" data-toggle="collapse"
                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"
                                v-on:click="makeFiltration()">
                            <i class="fas fa-search mr-2"></i>Применить
                        </button>
                        <button class="btn btn-outline-secondary" type="button" v-on:click="clearForm()">
                            <i class="fas fa-ban mr-2"></i>Очистить форму
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div>
        <table class="table table-striped table-bordered table-hover">
            <thead class="thead-dark">
            <tr>
                <th class="text-center" scope="col" style="width: 5% !important">
                    Базовая валюта
                    <button v-if="sorted_by_currency < 0" v-on:click="sortDeals('currency', 1)" class="fas fa-chevron-up ml-2 btn btn-outline-secondary btn-sm"></button>
                    <button v-if="sorted_by_currency >= 0" v-on:click="sortDeals('currency', -1)" class="fas fa-chevron-down ml-2 btn btn-outline-secondary btn-sm"></button>
                </th>
                <th class="text-center" scope="col" style="width: 5% !important">
                    Биржа
                </th>
                <th class="text-center" scope="col" style="width: 25% !important">
                    Паттерны
                </th>
                <th class="text-center" scope="col" style="width: 5% !important">
                    Прибыль %
                    <button v-if="sorted_by_profit < 0" v-on:click="sortDeals('profit_perc', 1)" class="fas fa-chevron-up ml-2 btn btn-outline-secondary btn-sm"></button>
                    <button v-if="sorted_by_profit >= 0" v-on:click="sortDeals('profit_perc', -1)" class="fas fa-chevron-down ml-2 btn btn-outline-secondary btn-sm"></button>
                </th>
                <th class="text-center" scope="col" style="width: 5% !important">
                    Прибыль
                </th>
                <th class="text-center" scope="col" style="width: 5% !important">
                    Прибыль % на сделку
                    <button v-if="sorted_by_profit_per_deal < 0" v-on:click="sortDeals('av_deal_profit_perc', 1)" class="fas fa-chevron-up ml-2 btn btn-outline-secondary btn-sm"></button>
                    <button v-if="sorted_by_profit_per_deal >= 0" v-on:click="sortDeals('av_deal_profit_perc', -1)" class="fas fa-chevron-down ml-2 btn btn-outline-secondary btn-sm"></button>
                </th>
                <th class="text-center" scope="col" style="width: 5% !important">
                    Прибыль на сделку
                </th>
                <th class="text-center" scope="col" style="width: 5% !important">
                    Av vol на сделку
                </th>
                <th class="text-center" scope="col" style="width: 5% !important">
                    Max vol на сделку
                </th>
                <th class="text-center" scope="col" style="width: 5% !important">
                    Min vol на сделку
                </th>
                <th class="text-center" scope="col" style="width: 5% !important">
                    Сделок
                    <button v-if="sorted_by_count < 0" v-on:click="sortDeals('count', 1)" class="fas fa-chevron-up ml-2 btn btn-outline-secondary btn-sm"></button>
                    <button v-if="sorted_by_count >= 0" v-on:click="sortDeals('count', -1)" class="fas fa-chevron-down ml-2 btn btn-outline-secondary btn-sm"></button>
                </th>
                <th class="text-center" scope="col" style="width: 25% !important">
                    График
                </th>
            </tr>
            </thead>
            <tbody>
            <tr v-show="!loading" v-for="item, index in deals_filtered">
                <td class="text-center align-middle">{{item.currency}}</td>
                <td class="text-center align-middle">{{item.exchange}}</td>

                <td class="text-center align-middle">
                    <div v-for="pattern, index in item.patterns">
                        <p>{{pattern}}</p>
                    </div>
                </td>

                <td class="text-center align-middle">{{Math.floor(item.profit_perc*1000)/1000}}</td>
                <td class="text-center align-middle">{{Math.floor(item.profit*1000000)/1000000}}</td>
                <td class="text-center align-middle">{{Math.floor(item.av_deal_profit_perc*1000)/1000}}</td>

                <td class="text-center align-middle">
                    {{Math.floor(item.av_deal_profit*1000000)/1000000}}
                </td>

                <td class="text-center align-middle">
                    {{Math.floor(item.av_deal_volume*1000000)/1000000}}
                </td>

                <td class="text-center align-middle">
                    {{Math.floor(item.max_deal_volume*1000000)/1000000}}
                </td>

                <td class="text-center align-middle">
                    {{Math.floor(item.min_deal_volume*1000000)/1000000}}
                </td>

                <td class="text-center align-middle">{{item.count}}</td>
                <td class="text-center align-middle">
                    <apexchart v-show="is_loaded" type=area width=100% :options="chartOptions" :series="[item.chartSeries]"></apexchart>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="text-center"><span v-if="no_results" class="h4">Ничего не найдено... Попробуйте изменить настройки поиска</span></div>
    </div>
</div>



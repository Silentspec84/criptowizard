<?php

use app\assets\ExchangesAsset;

ExchangesAsset::register($this);
?>


<div class="container-fluid" id="exchanges" ref="config"
     data-csrf='<?= Yii::$app->request->csrfToken?>'>

    <div class="table-responsive mb-auto">
        <table class="table table-striped table-bordered table-hover">
            <thead class="thead-dark">
            <tr>
                <th class="text-center" scope="col">
                    ID
                </th>
                <th class="text-center" scope="col">
                    Название
                </th>
                <th class="text-center" scope="col">
                    Ссылка
                </th>
                <th class="text-center" scope="col">
                    Комиссия
                </th>
                <th class="text-center" scope="col">
                    Пар загружено
                </th>
                <th class="text-center" scope="col">
                    Цепочек просчитанно
                </th>
                <th class="text-center" scope="col">
                    Действия
                </th>
            </tr>
            </thead>
            <tbody>
            <tr v-show="!loading" v-for="exchange, index in exchanges">
                <td class="text-center align-middle">{{exchange.id}}</td>
                <td class="text-center align-middle">{{exchange.name}}</td>
                <td class="text-center align-middle"><a :href="exchange.link" target="_blank">{{exchange.link}}</a></td>
                <td class="text-center align-middle">
                    <button type="button" class="btn btn-outline-secondary dropdown-toggle dropdown-toggle-split btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{exchange.commission}}
                    </button>
                    <div class="dropdown-menu p-2">
                        <input  type="number" data-toggle="tooltip" data-placement="right" placeholder="Комиссия" data-original-title="" title=""
                                class="form-control col-12 dropdown-item"
                                v-bind:data-original-title="getError('commission')"
                                v-bind:class="getError('commission') ? 'form-control mb-3 is-invalid' : 'form-control mb-3'"
                                v-model="exchange.new_commission"/>
                        <button type="submit" class="btn btn-outline-secondary btn-block" v-on:click="saveCommission(exchange)">Сохранить</button>
                    </div>
                </td>
                <td class="text-center align-middle">{{exchange.pairs_count}} (активно {{exchange.pairs_active}})</td>
                <td class="text-center align-middle">{{exchange.chains_count}}</td>
                <td class="text-center">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <button class="dropdown-item" type="button" v-on:click="loadPairs(index)">Загрузить пары с биржи</button>
                            <button class="dropdown-item" type="button" v-on:click="window.open('/instruments/index?id=' + exchange.id, '_blank')">Управление парами</button>
                            <button class="dropdown-item" type="button" v-on:click="loadChains(index)">Просчитать цепочки</button>
                            <button class="dropdown-item" type="button" v-on:click="window.open('/chains/index?id=' + exchange.id, '_blank')">Управление цепочками</button>
                            <button class="dropdown-item" type="button" v-on:click="loadBalances(index)">Обновить балансы</button>
                            <button class="dropdown-item" type="button" v-on:click="window.open('/balances/index?id=' + exchange.id, '_blank')">Посмотреть балансы</button>
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>


</div>



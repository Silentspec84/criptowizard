<?php

use app\assets\StrategyBuilderAsset;

StrategyBuilderAsset::register($this);
?>

<xml xmlns="https://developers.google.com/blockly/xml" id="toolbox" style="display: none">
    <category name="Логика" categorystyle="logic_category">
        <block type="controls_if"></block>
        <block type="controls_ifelse"></block>
        <block type="logic_compare"></block>
        <block type="logic_operation"></block>
        <block type="logic_boolean"></block>
        <block type="logic_ternary"></block>
        <block type="logic_negate"></block>
        <block type="logic_null"></block>
    </category>
    <category name="Математика" categorystyle="math_category">
        <block type="math_number"></block>
        <block type="math_arithmetic"></block>
        <block type="math_single"></block>
        <block type="math_trig"></block>
        <block type="math_constant"></block>
        <block type="math_number_property"></block>
        <block type="math_change"></block>
        <block type="math_round"></block>
        <block type="math_on_list"></block>
        <block type="math_modulo"></block>
        <block type="math_constrain"></block>
        <block type="math_random_int"></block>
        <block type="math_random_float"></block>
        <block type="math_atan2"></block>
        <block type="math_min_max"></block>
    </category>
    <category name="Текст" categorystyle="text_category">
        <block type="text"></block>
        <block type="text_multiline"></block>
        <block type="text_join"></block>
        <block type="text_length"></block>
        <block type="text_isEmpty"></block>
        <block type="text_append"></block>
        <block type="text_indexOf"></block>
        <block type="text_charAt"></block>
        <block type="text_getSubstring"></block>
        <block type="text_changeCase"></block>
        <block type="text_trim"></block>
        <block type="text_print"></block>
        <block type="text_prompt_ext"></block>
        <block type="text_prompt"></block>
        <block type="text_count"></block>
        <block type="text_replace"></block>
        <block type="text_reverse"></block>
    </category>
    <category name="Переменные" categorystyle="variable_dynamic_category">
        <block type="variables_get_dynamic"></block>
        <block type="variables_set_dynamic"></block>
    </category>
    <category name="Время" categorystyle="time_category">
        <block type="time"></block>
        <block type="day_of_week"></block>
        <block type="DST"></block>
        <block type="day_of_month"></block>
        <block type="day_of_year"></block>
        <block type="hour"></block>
        <block type="minute"></block>
        <block type="second"></block>
        <block type="month"></block>
        <block type="year"></block>
    </category>
    <category name="Методы" categorystyle="procedure_category">
        <block type="procedures_defnoreturn"></block>
        <block type="procedures_defreturn"></block>
        <block type="procedures_ifreturn"></block>
    </category>
    <category name="Циклы" categorystyle="loop_category">
        <block type="controls_repeat_ext"></block>
        <block type="controls_repeat"></block>
        <block type="controls_whileUntil"></block>
        <block type="controls_for"></block>
        <block type="controls_forEach"></block>
        <block type="controls_flow_statements"></block>
    </category>
    <category name="Массивы" categorystyle="list_category">
        <block type="lists_create_empty"></block>
        <block type="lists_repeat"></block>
        <block type="lists_create_with"></block>
        <block type="lists_reverse"></block>
        <block type="lists_isEmpty"></block>
        <block type="lists_length"></block>
        <block type="lists_indexOf"></block>
        <block type="lists_getIndex"></block>
        <block type="lists_setIndex"></block>
        <block type="lists_getSublist"></block>
        <block type="lists_sort"></block>
        <block type="lists_split"></block>
        <block type="clone_list"></block>
        <block type="list_min_max"></block>
    </category>
    <category name="Цены" categorystyle="price_category">
        <block type="prices_data"></block>
        <block type="prices_ask"></block>
        <block type="prices_bid"></block>
        <block type="prices_ask_depth_of_market"></block>
        <block type="prices_bid_depth_of_market"></block>
        <block type="prices_last_data"></block>
    </category>
    <category name="Манименеджмент" categorystyle="mm_category">
        <block type="lots_min_lot"></block>
        <block type="lots_max_lot"></block>
        <block type="lots_lot_step"></block>
        <block type="lots_lot_fix"></block>
        <block type="lots_lot_perc"></block>
    </category>
    <category name="Индикаторы" categorystyle="ind_category">
        <category name="Осцилляторы" categorystyle="osc_category">
            <block type="iAC"></block>
            <block type="iAD"></block>
            <block type="iAO"></block>
            <block type="iATR"></block>
            <block type="iBearsPower"></block>
            <block type="iBullsPower"></block>
            <block type="iCCI"></block>
            <block type="iDeMarker"></block>
            <block type="iForce"></block>
            <block type="iBWMFI"></block>
            <block type="iMomentum"></block>
            <block type="iMFI"></block>
            <block type="iOsMA"></block>
            <block type="iMACD"></block>
            <block type="iRSI"></block>
            <block type="iRVI"></block>
            <block type="iStochastic"></block>
            <block type="iWPR"></block>
        </category>
        <category name="Трендовые" categorystyle="trend_category">
            <block type="iBands"></block>
            <block type="iADX"></block>
            <block type="Alligator"></block>
            <block type="iEnvelopes"></block>
            <block type="iIchimoku"></block>
            <block type="iMA"></block>
            <block type="iSAR"></block>
        </category>
        <category name="Прочие" categorystyle="oth_category">
            <block type="iFractals"></block>
            <block type="iOBV"></block>
            <block type="iStdDev"></block>
        </category>
    </category>
    <category name="Компоненты" categorystyle="components_category">
        <block type="components_main"></block>
        <block type="components_init"></block>
        <block type="components_signal"></block>
        <block type="components_action"></block>
        <block type="components_trade"></block>
        <block type="components_return_signal_result"></block>
        <block type="components_period"></block>
        <block type="components_pairs"></block>
        <block type="components_price_types"></block>
        <block type="components_notify"></block>
        <block type="components_balance"></block>
        <block type="components_prices_deals"></block>
        <block type="components_stop_all"></block>

    </category>
</xml>

<div id="builder" ref="config"
     data-csrf = '<?= Yii::$app->request->csrfToken?>'
>

    <div class="card">
        <div class="card-header">
            <span class="h4 pull-left">Конструктор стратегий</span>
            <form class="form-inline pull-right">
                <div class="form-row pull-right">
                    <button type="button" class="btn btn-outline-secondary pull-right ml-3" title="Тестировать стратегию" v-on:click="clearWorkspace()"><i class="far fa-play-circle fa-lg"></i></i></button>
                </div>
                <div class="form-row pull-right ml-3">
        <!--            <button type="button" class="btn btn-outline-secondary pull-right ml-1" title="Сохранить стратегию"><i class="far fa-save fa-lg"></i></button>-->
        <!--            <button type="button" class="btn btn-outline-secondary pull-right ml-3" title="Загрузить сохраненную стратегию"><i class="far fa-folder-open fa-lg"></i></i></button>-->

                    <button type="button" class="btn btn-outline-secondary pull-right ml-1" title="Назад" v-on:click="undoRedo(false)"><i class="fas fa-undo fa-lg"></i></i></button>
                    <button type="button" class="btn btn-outline-secondary pull-right ml-1" title="Вперед" v-on:click="undoRedo(true)"><i class="fas fa-redo fa-lg"></i></i></button>
                    <button type="button" class="btn btn-outline-secondary pull-right ml-1" title="Очистить рабочую область" v-on:click="clearWorkspace()"><i class="far fa-trash-alt fa-lg"></i></button>
                </div>
                <div class="form-row pull-right ml-3">
                    <div class="js-upload" uk-form-custom>
                        <input type="file" multiple id="strategy_file" class="btn btn-outline-secondary" title="Загрузить стратегию из файла" v-on:change="loadFromFile()">
                        <button class="btn btn-outline-secondary ml-1" type="button" tabindex="-1" title="Загрузить стратегию из файла" ><i class="fas fa-folder fa-lg"></i></button>
                    </div>
                    <button type="button" class="btn btn-outline-secondary pull-right ml-1" title="Экспортировать стратегию в файл" v-on:click="saveToFile()"><i class="far fa-file-code fa-lg"></i></i></button>
                </div>
                <div class="form-row pull-right ml-3">
                    <input  type="text" data-toggle="tooltip" placeholder="Название стратегий" data-original-title="" title=""
                            class="form-control"
                            v-bind:data-original-title="getError('strategy_name')"
                            v-bind:class="getError('strategy_name') ? 'form-control is-invalid' : 'form-control'"
                            v-model="strategy_name"/>
                </div>
                <div class="form-row pull-right">
                    <select class="form-control ml-4" v-model="current_theme" v-on:change="setTheme()">
                        <option v-for="theme in getThemes()" :value="theme.value">{{theme.name}}</option>
                    </select>
                </div>
            </form>
        </div>
        <div class="card-body">
            <div id="blocklyDiv" style=" height: 72vh; width: 95vw;"></div>
        </div>
    </div>

    <button type="button" class="btn btn-primary" v-on:click="getCode">Primary</button>
    <p v-if="code != null">
        {{code}}
    </p>
</div>

<style type="text/css">
    .v-divider{
        margin-left:5px;
        margin-right:5px;
        width:1px;
        height:100%;
        border-left:1px solid gray;
    }
</style>
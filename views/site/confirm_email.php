<?php

use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var User $user */

?>
<br xmlns="http://www.w3.org/1999/html">
<br><br><br><br>
<div class="container-fluid mt-5">
    <div class="row align-middle">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="card">
                <h3 class="card-header text-center">
                    Добро пожаловать, <?=$user->login_name?>!
                </h3>
                <div class="card-body text-center">
                    <p>
                        Ваш email <?=$user->email?> успешно подтвержден.
                        Логин и пароль для доступа к аккаунту отправлены на почту.
                    </p>
                    <p>
                        Теперь вы можете <?= Html::a(Html::encode('добавить ваши счета'), Url::to(['/accounts'], true)); ?> или <?= Html::a(Html::encode('заполнить профиль'), Url::to(['/'], true)); ?> в личном кабинете.
                    </p>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>

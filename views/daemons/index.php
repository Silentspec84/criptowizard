<?php

use app\assets\DaemonsAsset;

DaemonsAsset::register($this);
?>


<div class="container-fluid" id="daemons" ref="config"
     data-csrf='<?= Yii::$app->request->csrfToken?>'>

    <div class="table-responsive mb-auto">
        <table class="table table-striped table-bordered table-hover">
            <thead class="thead-dark">
            <tr>
                <th class="text-center" scope="col">
                    ID
                </th>
                <th class="text-center" scope="col">
                    Биржа
                </th>
                <th class="text-center" scope="col">
                    Команда
                </th>
                <th class="text-center" scope="col">
                    Стратегия
                </th>
                <th class="text-center" scope="col">
                    Тип
                </th>
                <th class="text-center" scope="col">
                    Таймер, с
                </th>
                <th class="text-center" scope="col">
                    Последний коннект
                </th>
                <th class="text-center" scope="col">
                    Состояние демона
                </th>
                <th class="text-center" scope="col">
                    Статус демона
                </th>
            </tr>
            </thead>
            <tbody>
            <tr v-show="!loading" v-for="daemon, index in daemons">
                <td class="text-center align-middle">{{daemon.daemon_data.id}}</td>
                <td class="text-center align-middle">{{daemon.exchange_name}}</td>
                <td class="text-center align-middle">{{daemon.daemon_data.command}}</td>
                <td class="text-center align-middle">{{daemon.daemon_data.strategy}}</td>
                <td class="text-center align-middle">{{daemon.daemon_type}}</td>
                <td class="text-center align-middle">
                    <div class="btn-group">
                        <button type="button" class="btn btn-outline-primary">{{daemon.daemon_data.timer}}</button>
                        <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="fas fa-chevron-down fa-xs"></span>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" v-on:click="setTimer(index, 5)">5</a>
                            <a class="dropdown-item" v-on:click="setTimer(index, 10)">10</a>
                            <a class="dropdown-item" v-on:click="setTimer(index, 15)">15</a>
                            <a class="dropdown-item" v-on:click="setTimer(index, 30)">30</a>
                            <a class="dropdown-item" v-on:click="setTimer(index, 60)">60</a>
                            <a class="dropdown-item" v-on:click="setTimer(index, 120)">120</a>
                            <a class="dropdown-item" v-on:click="setTimer(index, 300)">300</a>
                        </div>
                    </div>
                </td>
                <td class="text-center align-middle">{{formatDatetime(daemon.daemon_data.date_last_connected)}}</td>
                <td class="text-center align-middle">
                    <button v-show="daemon.daemon_data.enabled == 1" type="button" class="btn btn-outline-success btn-sm p-0" v-on:click="switchDaemon(index)">Включен</button>
                    <button v-show="daemon.daemon_data.enabled == 0" type="button" class="btn btn-outline-danger btn-sm p-0" v-on:click="switchDaemon(index)">Отключен</button>
                </td>
                <td class="text-center align-middle">
                    <button v-show="daemon.status == 1" type="button" class="btn btn-outline-success btn-sm p-0">Работает</button>
                    <button v-show="daemon.status == 0" type="button" class="btn btn-outline-danger btn-sm p-0">Не работает</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>



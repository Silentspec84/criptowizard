<?php

use app\assets\ChainsAsset;

ChainsAsset::register($this);
?>


<div class="container-fluid" id="chains" ref="config"
     data-csrf = '<?= Yii::$app->request->csrfToken?>'
     data-id = '<?=json_encode(!empty($id) ? $id : 0)?>'
>

    <div class="table-responsive mb-auto">
        <table class="table table-striped table-bordered table-hover">
            <thead class="thead-dark">
            <tr>
                <th class="text-center" scope="col">
                    ID
                </th>
                <th class="text-center" scope="col">
                    Биржа
                </th>
                <th class="text-center" scope="col">
                    Паттерн
                </th>
                <th class="text-center" scope="col">
                    Базовые пары
                </th>
                <th class="text-center" scope="col">
                    Направления
                </th>
                <th class="text-center" scope="col">
                    Базовая валюта
                </th>
                <th class="text-center" scope="col">
                    Статус
                </th>
                <th class="text-center" scope="col">
                    Действия
                </th>
            </tr>
            </thead>
            <tbody>
            <tr v-show="!loading" v-for="chain, index in chains">
                <td class="text-center align-middle">{{chain.chain_data.id}}</td>
                <td class="text-center align-middle">{{chain.exchange_name}}</td>
                <td class="text-center align-middle">{{chain.chain_data.pattern}}</td>
                <td class="text-center align-middle">{{chain.chain_data.base}}</td>
                <td class="text-center align-middle">{{chain.chain_data.dir}}</td>
                <td class="text-center align-middle">{{chain.chain_data.first_currency}}</td>
                <td class="text-center align-middle">
                    <button v-show="chain.chain_data.status == 1" type="button" class="btn btn-outline-success btn-sm p-0" v-on:click="switchChain(index)">Включен</button>
                    <button v-show="chain.chain_data.status == 0" type="button" class="btn btn-outline-danger btn-sm p-0" v-on:click="switchChain(index)">Отключен</button>
                </td>
                <td class="text-center">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-bars"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <!--                            <button class="dropdown-item" type="button" >Редактировать биржу</button>-->
                            <!--                            <button class="dropdown-item" type="button" >Загрузить пары с биржи</button>-->
                            <!--                            <button class="dropdown-item" type="button">Управление парами</button>-->
                            <!--                            <button class="dropdown-item" type="button" >Просчитать цепочки</button>-->
                            <!--                            <button class="dropdown-item" type="button">Управление цепочками</button>-->
                            <!--                            <button class="dropdown-item" type="button" >Обновить балансы</button>-->
                        </div>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>



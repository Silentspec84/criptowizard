<?php

use app\assets\AppAsset;
use yii\helpers\Html;
use app\models\User;
use app\components\Header;
use app\components\Footer;

AppAsset::register($this);

/** @var User $user */
$user = Yii::$app->getUser()->identity;
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>

        <title><?= Html::encode($this->title) ?></title>
        <? if (isset($this->title)): ?>
            <meta name="title" content="<?= $this->title ?>">
        <? endif; ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="Cache-Control" content="no-cache">
        <meta name="csrf-token" content="<?= Yii::$app->request->csrfToken?>">
        <meta name="js-config" content='<?php
        $contoller = get_class(Yii::$app->controller);
        echo json_encode($contoller::getJsConfig());
        ?>'>
        <?php $this->head(); ?>
    </head>

    <body class="fixed-left">
    <?php $this->beginBody() ?>

    <div class="page">

        <header id="main_menu" class="page__header header" ref="config" data-csrf='<?= Yii::$app->request->csrfToken?>'>
            <!--Виджет кастомного хедера-->
            <?php Header::begin();?>
            <?php Header::end();?>
        </header>

        <main>
            <div class="container-fluid">
                <?= $content; ?>
            </div>
        </main>

        <?php if (Yii::$app->controller->action->id != 'recover'):?>
            <footer>
                <!--Виджет кастомного футера -->
                <?php ##Footer::begin();?>
                <?php ##Footer::end();?>
            </footer>
        <?endif;?>

    </div>

    <?php $this->endBody()?>
    </body>
</html>
<?php $this->endPage() ?>
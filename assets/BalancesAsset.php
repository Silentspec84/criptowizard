<?php

namespace app\assets;

use yii\web\AssetBundle;

class BalancesAsset extends AssetBundle
{
    public $js = [
        "js/pages/balances.js",
    ];

    public $css = [
    ];

    public $depends = [
        'app\assets\AppAsset'
    ];
}
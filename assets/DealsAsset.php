<?php

namespace app\assets;

use yii\web\AssetBundle;

class DealsAsset extends AssetBundle
{
    public $js = [
        "js/pages/deals.js",
        'https://cdn.jsdelivr.net/npm/apexcharts@latest',
        'https://cdn.jsdelivr.net/npm/vue-apexcharts',
    ];

    public $css = [
    ];

    public $depends = [
        'app\assets\AppAsset'
    ];
}
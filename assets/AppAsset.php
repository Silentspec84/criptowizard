<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/app.css',
        'css/main.css',
        'css/style.css',
        'css/bootstrap.min.css',
        'css/all.css',
        'css/icons.css',
        'css/site.css',
        'css/bootstrap-grid.min.css',
        'css/bootstrap-reboot.min.css',
        'css/bootstrap-switch.min.css',
        'css/uikit.css'
    ];
    public $js = [
        "js/pages/main-menu.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'app\assets\VueAsset',
    ];

}

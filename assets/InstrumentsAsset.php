<?php

namespace app\assets;

use yii\web\AssetBundle;

class InstrumentsAsset extends AssetBundle
{
    public $js = [
        "js/pages/instruments.js",
    ];

    public $css = [
    ];

    public $depends = [
        'app\assets\AppAsset'
    ];
}
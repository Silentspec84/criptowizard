<?php

namespace app\assets;

use yii\web\AssetBundle;

class ChainsAsset extends AssetBundle
{
    public $js = [
        "js/pages/chains.js",
    ];

    public $css = [
    ];

    public $depends = [
        'app\assets\AppAsset'
    ];
}
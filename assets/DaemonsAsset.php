<?php

namespace app\assets;

use yii\web\AssetBundle;

class DaemonsAsset extends AssetBundle
{
    public $js = [
        "js/pages/daemons.js",
    ];

    public $css = [
    ];

    public $depends = [
        'app\assets\AppAsset'
    ];
}
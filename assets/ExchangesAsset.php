<?php

namespace app\assets;

use yii\web\AssetBundle;

class ExchangesAsset extends AssetBundle
{
    public $js = [
        "js/pages/exchanges.js",
    ];

    public $css = [
    ];

    public $depends = [
        'app\assets\AppAsset'
    ];
}
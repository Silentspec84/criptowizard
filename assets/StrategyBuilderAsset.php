<?php

namespace app\assets;

use yii\web\AssetBundle;

class StrategyBuilderAsset extends AssetBundle
{
    public $js = [
        "js/components/document-saver.js",
        "js/blockly/blockly_uncompressed.js",
        "js/blockly/common/const.js",
        "js/pages/strategy_builder.js",
        "js/blockly/blocks/components.js",
        "js/blockly/blocks/indicators.js",
        "js/blockly/blocks/math.js",
        "js/blockly/blocks/lists.js",
        "js/blockly/blocks/logic.js",
        "js/blockly/blocks/loops.js",
        "js/blockly/blocks/prices.js",
        "js/blockly/blocks/lots.js",
        "js/blockly/blocks/time.js",
        "js/blockly/blocks/procedures.js",
        "js/blockly/blocks/text.js",
        "js/blockly/blocks/variables.js",
        "js/blockly/blocks/variables_dynamic.js",
        "js/blockly/msg/js/en.js",
        "js/blockly/php_compressed.js",
        "js/blockly/generators/php/components.js",
    ];

    public $css = [
    ];

    public $depends = [
        'app\assets\AppAsset'
    ];

    public $jsOptions = ['position' => \yii\web\View::POS_END];
}
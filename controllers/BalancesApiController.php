<?php

namespace app\controllers;

use app\classes\connectors\ApiConnectorInterface;
use app\classes\connectors\PoloniexApiConnector;
use app\classes\helpers\ArrayHelper;
use app\classes\helpers\InstrumentParserHelper;
use app\classes\processors\DaemonProcessor;
use app\components\AccessRule;
use app\models\Currency;
use app\models\Daemon;
use app\models\Exchange;
use app\models\Instrument;
use app\models\PairChain;
use app\models\User;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;


/**
 * Class SiteApiController
 * @package app\controllers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */

class BalancesApiController extends BaseApiController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['get-balances', 'switch-currency', 'set-min-volume'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['get-balances', 'switch-currency', 'set-min-volume'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function() {
                    $this->redirect('/');
                }
            ],
        ];
    }

    /**
     * Метод возвращает список бирж и дополнительную информацию по ним
     *
     * @return \app\classes\api\Response
     */
    public function actionGetBalances()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'integer', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $chains = [];
        $exchange_id = $this->request_post['id'];

        if ($exchange_id === 0) {
            $balances = Currency::find()->all();
            $exchanges = Exchange::find()->indexBy('id')->all();
        } else {
            $balances = Currency::find()->where(['exchange_id' => $exchange_id])->all();
            $exchanges = Exchange::find()->where(['id' => $exchange_id])->indexBy('id')->all();
        }

        $result = [];
        foreach ($balances as $balance) {
            /** @var Currency $balance */
            $result[$balance->id]['balance_data'] = $balance;
            $result[$balance->id]['exchange_name'] = $exchanges[$balance->exchange_id]->name;
        }

        return $this->response->setContent(['balances' => $result]);
    }

    public function actionSwitchCurrency()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'integer', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $currency_id = $this->request_post['id'];
        /** @var Exchange $exchange */
        $currency = Currency::findOne($currency_id);
        if (!$currency) {
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }
        $currency->status = $currency->status === Currency::STATUS_OFF ? Currency::STATUS_ON : Currency::STATUS_OFF;
        if (!$currency->save()) {
            return $this->response->addError($currency->errors);
        }

        return $this->response->setContent(['status' => $currency->status]);
    }

    public function actionSetMinVolume()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'balance' => ['type' => 'array', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $balance_array = $this->request_post['balance'];
        $balance = Currency::findOne($balance_array['balance_data']['id']);
        $balance->min_volume = $balance_array['balance_data']['min_volume'];
        if (!$balance->save()) {
            return $this->response->addError($balance->errors);
        }

        return $this->response;
    }
}

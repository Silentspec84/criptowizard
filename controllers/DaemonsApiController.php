<?php

namespace app\controllers;

use app\classes\connectors\ApiConnectorInterface;
use app\components\AccessRule;
use app\models\Daemon;
use app\models\Exchange;
use app\models\Instrument;
use app\models\Logs;
use app\models\PairChain;
use yii\filters\AccessControl;


/**
 * Class DaemonsApiController
 * @package app\controllers
 */

class DaemonsApiController extends BaseApiController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['get-daemons', 'switch-daemon', 'set-timer'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['get-daemons', 'switch-daemon', 'set-timer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function() {
                    $this->redirect('/');
                }
            ],
        ];
    }

    /**
     * Метод возвращает список бирж и дополнительную информацию по ним
     *
     * @return \app\classes\api\Response
     */
    public function actionGetDaemons()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $exchanges = Exchange::find()->indexBy('id')->all();
        $daemons = Daemon::find()->all();

        $result = [];
        /** @var Daemon $daemon */
        foreach ($daemons as $daemon) {
            $result[$daemon->id]['daemon_data'] = $daemon;
            $result[$daemon->id]['daemon_type'] = Daemon::$daemon_types[$daemon->job_type];
            $result[$daemon->id]['exchange_name'] = $exchanges[$daemon->exchange_id]->name;
            $status = 1;
            if ($daemon->date_last_connected < time() - 4 * $daemon->timer) {
                $status = 0;
            }
            $result[$daemon->id]['status'] = $status;
        }

        return $this->response->setContent(['daemons' => $result]);
    }

    public function actionSwitchDaemon()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'integer', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $daemon_id = $this->request_post['id'];
        $daemon = Daemon::findOne($daemon_id);
        $daemon->enabled = $daemon->enabled === Daemon::ENABLED_OFF ? Daemon::ENABLED_ON : Daemon::ENABLED_OFF;
        if (!$daemon->save()) {
            return $this->response->addError("Не удалось произвести сохранение данных!");
        }
        $exchange_id = $daemon->exchange_id;

        if ($daemon->enabled === Daemon::ENABLED_ON) {
            $logger = Logs::getLogger($exchange_id, 'Start daemon ' . $daemon_id . ' (' . Daemon::$daemon_types[$daemon->job_type] . ')', true);
            $logger->saveMessage('Старт демона для биржи с id ' . $exchange_id  . ' (' . Daemon::$daemon_types[$daemon->job_type] . ')');

            $command = 'cd /app && /usr/local/bin/php yii daemon-trade/run ' . $daemon_id . ' > /dev/null &';
            exec($command);

            return $this->response;
        }

        return $this->response;
    }

    public function actionSetTimer()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'integer', 'required' => true],
            'timer' => ['type' => 'integer', 'required' => true],
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $daemon_id = $this->request_post['id'];
        $timer = $this->request_post['timer'];

        $daemon = Daemon::findOne($daemon_id);
        $daemon->timer = $timer;

        if (!$daemon->save()) {
            return $this->response->addError("Не удалось произвести сохранение данных!");
        }

        return $this->response->setContent(['timer' => $daemon->timer]);
    }

}

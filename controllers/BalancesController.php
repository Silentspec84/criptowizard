<?php

namespace app\controllers;

use app\components\AccessRule;
use yii\filters\AccessControl;

class BalancesController extends BaseController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
        ];
    }
    /**
     * @return string
     */
    public function actionIndex($id = 0)
    {
        return $this->render('index', ['id' => $id]);
    }
}
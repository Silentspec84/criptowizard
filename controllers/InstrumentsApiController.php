<?php

namespace app\controllers;

use app\classes\connectors\ApiConnectorInterface;
use app\classes\connectors\PoloniexApiConnector;
use app\classes\helpers\ArrayHelper;
use app\classes\helpers\InstrumentParserHelper;
use app\classes\processors\DaemonProcessor;
use app\components\AccessRule;
use app\models\Daemon;
use app\models\Exchange;
use app\models\Instrument;
use app\models\PairChain;
use app\models\User;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;


/**
 * Class SiteApiController
 * @package app\controllers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */

class InstrumentsApiController extends BaseApiController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['get-pairs', 'switch-pair'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['get-pairs', 'switch-pair'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function() {
                    $this->redirect('/');
                }
            ],
        ];
    }

    /**
     * Метод возвращает список бирж и дополнительную информацию по ним
     *
     * @return \app\classes\api\Response
     */
    public function actionGetPairs()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'integer', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $instruments = [];
        $exchange_id = $this->request_post['id'];

        if ($exchange_id === 0) {
            $instruments = Instrument::find()->all();
            $exchanges = Exchange::find()->indexBy('id')->all();
        } else {
            $instruments = Instrument::find()->where(['exchange_id' => $exchange_id])->all();
            $exchanges = Exchange::find()->where(['id' => $exchange_id])->indexBy('id')->all();
        }

        $result = [];
        foreach ($instruments as $instrument) {
            /** @var Instrument $instrument */
            $result[$instrument->id]['pair_data'] = $instrument;
            $result[$instrument->id]['exchange_name'] = $exchanges[$instrument->exchange_id]->name;
        }

        return $this->response->setContent(['pairs' => $result]);
    }

    public function actionSwitchPair()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'integer', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $pair_id = $this->request_post['id'];
        /** @var Exchange $exchange */
        $pair = Instrument::findOne($pair_id);
        if (!$pair) {
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }
        $pair->status = $pair->status === Instrument::STATUS_OFF ? Instrument::STATUS_ON : Instrument::STATUS_OFF;
        if (!$pair->save()) {
            return $this->response->addError($pair->errors);
        }

        return $this->response->setContent(['status' => $pair->status]);
    }

    public function actionLoadChains()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'string', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $exchange_id = $this->request_post['id'];
        $daemon = Daemon::find()->where(['exchange_id' => $exchange_id, 'job_type' => Daemon::TYPE_CHAINS])->one();
        $processor = new DaemonProcessor();
        $processor->start($daemon);

        $count = PairChain::find()->where(['exchange_id' => $exchange_id])->count();
        $this->response->setContent(['chains' => $count]);
        return $this->response;
    }

}

<?php

namespace app\controllers;

use app\components\AccessRule;
use DOMDocument;
use yii\filters\AccessControl;

/**
 * Class SiteApiController
 * @package app\controllers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */

class StrategyBuilderApiController extends BaseApiController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['save-xml'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['save-xml'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function() {
                    $this->redirect('/');
                }
            ],
        ];
    }

    public function actionSaveXml()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'xml' => ['type' => 'string', 'required' => true],
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }
        $doc = new DOMDocument('1.0');
        $doc->loadXML($this->request_post['xml']);
        $doc->save("test.xml");
        return $this->response->setContent(['link' => '/test.xml']);
    }
}

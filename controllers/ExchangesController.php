<?php

namespace app\controllers;

use app\classes\connectors\ApiConnectorInterface;
use app\classes\connectors\BinanceApiConnector;
use app\classes\connectors\BittrexApiConnector;
use app\classes\connectors\ExmoApiConnector;
use app\classes\helpers\InstrumentParserHelper;
use app\classes\strategies\StrategyArbitrageIn;
use app\models\Deal;
use app\models\Exchange;
use app\models\Instrument;
use app\models\Logs;
use app\models\PairChain;
use Yii;
use yii\filters\AccessControl;
use app\components\AccessRule;
use yii\helpers\Json;

class ExchangesController extends BaseController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'run'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'run'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],

            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRun()
    {
        $xchange = Exchange::findOne(1);
        $logger = Logs::getLogger(1, Logs::CALC_CHAINS, true);
        $ph = new InstrumentParserHelper($xchange, $logger);
        ea($ph->parseChains());
        ea(Deal::find()->where(['hash' => 'dfgdfgdg'])->one());
//        $deal = Deal::find()->where(['id'=>661])->asArray()->one();
//        $deal = Json::decode($deal['deal']);
//        $pattern = PairChain::find()->where(['id'=>9814])->one();
//        $exchange = Exchange::findOne(1);
//        (new ExmoApiConnector())->playPattern($deal, $pattern, $exchange);
    }

}

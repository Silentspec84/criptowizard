<?php

namespace app\controllers;

use app\classes\connectors\ApiConnectorInterface;
use app\classes\helpers\ArrayHelper;
use app\classes\helpers\InstrumentParserHelper;
use app\classes\processors\DaemonProcessor;
use app\components\AccessRule;
use app\models\Daemon;
use app\models\Deal;
use app\models\Exchange;
use app\models\Instrument;
use app\models\PairChain;
use app\models\User;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;


/**
 * Class SiteApiController
 * @package app\controllers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */

class DealsApiController extends BaseApiController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['get-deals', 'load-instruments', 'load-chains', 'switch-exchange', 'switch-daemon', 'set-timer'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['get-deals', 'load-instruments', 'load-chains', 'switch-exchange', 'switch-daemon', 'set-timer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function() {
                    $this->redirect('/');
                }
            ],
        ];
    }

    public function actionGetDeals()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $exchanges = Exchange::find()->asArray()->indexBy(['id'])->all();
        $deals = Deal::find()->all();

        $result = [];
        $exchange_list = [];
        $currency_list = [];
        /** @var Deal $deal */
        foreach ($deals as $deal) {
            $currency = explode('_', explode('-', $deal->pattern['pattern'])[0])[0];
            $index = $currency . $deal->exchange_id;
            if (!array_key_exists($index, $result)) {
                $result[$index]['currency'] = $currency;
                $result[$index]['exchange'] = $exchanges[$deal->exchange_id]['name'];
                $result[$index]['patterns'][$deal->pattern['pattern']] = $deal->pattern['pattern'];
                $result[$index]['profit_perc'] = round($deal->profit_perc, 3);
                $result[$index]['profit'] = round($deal->profit, 8);
                $result[$index]['count'] = 1;
                $result[$index]['av_deal_profit_perc'] = round($deal->profit_perc, 3);
                $result[$index]['av_deal_profit'] =round($deal->profit, 8);
                $result[$index]['chartSeries']['data'][] = round($deal->profit_perc, 3);
                $result[$index]['deal_volume'] = round($deal->deal[0]['base_volume'], 8);
                $result[$index]['av_deal_volume'] = round($deal->deal[0]['base_volume'], 8);
                $result[$index]['max_deal_volume'] = round($deal->deal[0]['base_volume'], 8);
                $result[$index]['min_deal_volume'] = round($deal->deal[0]['base_volume'], 8);
                $exchange_list[$deal->exchange_id] = $exchanges[$deal->exchange_id]['name'];
                $currency_list[$currency] = $currency;
            } else {
                $result[$index]['patterns'][$deal->pattern['pattern']] = $deal->pattern['pattern'];
                $result[$index]['profit_perc'] += round($deal->profit_perc, 3);
                $result[$index]['profit'] += round($deal->profit, 8);
                $result[$index]['count']++;
                $result[$index]['av_deal_profit_perc'] = round($result[$index]['profit_perc'] / $result[$index]['count'], 3);
                $result[$index]['av_deal_profit'] = round($result[$index]['profit'] / $result[$index]['count'], 8);
                $result[$index]['chartSeries']['data'][] = $result[$index]['profit_perc'];
                $result[$index]['deal_volume'] += round($deal->deal[0]['base_volume'], 8);
                $result[$index]['av_deal_volume'] = round($result[$index]['deal_volume'] / $result[$index]['count'], 8);
                if ($result[$index]['max_deal_volume'] < $deal->deal[0]['base_volume']) {
                    $result[$index]['max_deal_volume'] = round($deal->deal[0]['base_volume'], 8);
                }
                if ($result[$index]['min_deal_volume'] > $deal->deal[0]['base_volume']) {
                    $result[$index]['min_deal_volume'] = round($deal->deal[0]['base_volume'], 8);
                }
                $currency_list[$currency] = $currency;
                $exchange_list[$deal->exchange_id] = $exchanges[$deal->exchange_id]['name'];
            }
        }

        return $this->response->setContent(['deals' => $result, 'exchanges' => $exchange_list, 'currencies' => $currency_list]);
    }

    public function actionSwitchExchange()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'string', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $exchange_id = $this->request_post['id'];
        $exchange = Exchange::findOne($exchange_id);
        $exchange->status = $exchange->status === Exchange::STATUS_OFF ? Exchange::STATUS_ON : Exchange::STATUS_OFF;
        if (!$exchange->save()) {
            return $this->response->addError("Не удалось произвести сохранение данных!");
        }

        return $this->response->setContent(['status' => $exchange->status]);
    }


    public function actionSwitchDaemon()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'string', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $daemon_id = $this->request_post['id'];
        $daemon = Daemon::findOne($daemon_id);
        $daemon->enabled = $daemon->enabled === Daemon::ENABLED_OFF ? Daemon::ENABLED_ON : Daemon::ENABLED_OFF;
        if (!$daemon->save()) {
            return $this->response->addError("Не удалось произвести сохранение данных!");
        }

        if ($daemon->enabled === Daemon::ENABLED_ON) {
            $processor = new DaemonProcessor();
            $processor->start($daemon);
        }

        return $this->response->setContent(['enabled' => $daemon->enabled]);
    }

    public function actionSetTimer()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'string', 'required' => true],
            'timer' => ['type' => 'integer', 'required' => true],
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $daemon_id = $this->request_post['id'];
        $timer = $this->request_post['timer'];

        $daemon = Daemon::findOne($daemon_id);
        $daemon->timer = $timer;

        if (!$daemon->save()) {
            return $this->response->addError("Не удалось произвести сохранение данных!");
        }

        return $this->response->setContent(['timer' => $daemon->timer]);
    }

    public function actionLoadInstruments()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'string', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $exchange_id = $this->request_post['id'];

        $transaction = Yii::$app->db->beginTransaction();
        $connector = $this->getConnector($exchange_id);

        if (Instrument::deleteAll(['exchange_id' => $exchange_id]) < 0) {
            $transaction->rollBack();
            return $this->response->addError("Не удалось произвести загрузку данных!");
        }

        $pairs_data = $connector->getPairsSettingsArray();
        $pairs = [];
        $count = 0;
        foreach ($pairs_data as $pair_name => $pair_settings) {
            $pairs[$count][] = $exchange_id;
            $pairs[$count][] = $pair_name;
            $pairs[$count][] = (float)$pair_settings['min_quantity'];
            $pairs[$count][] = (float)$pair_settings['max_quantity'];
            $pairs[$count][] = (float)$pair_settings['min_price'];
            $pairs[$count][] = (float)$pair_settings['max_price'];
            $pairs[$count][] = (float)$pair_settings['min_amount'];
            $pairs[$count][] = (float)$pair_settings['max_amount'];
            $pairs[$count][] = time();
            $pairs[$count][] = time();
            $count++;
        }

        if (!Instrument::saveInstrumentsSettings($pairs)) {
            $transaction->rollBack();
            return $this->response->addError("Не удалось произвести загрузку данных!");
        }

        try {
            $transaction->commit();
        } catch (Exception $e) {
            return $this->response->addError("Не удалось произвести сохранение!");
        }

        $count = Instrument::find()->where(['exchange_id' => $exchange_id])->count();
        $this->response->setContent(['count' => $count]);
        return $this->response;
    }

    public function actionLoadChains()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'string', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $exchange_id = $this->request_post['id'];

        $transaction = Yii::$app->db->beginTransaction();
        if (PairChain::deleteAll(['exchange_id' => $exchange_id]) < 0) {
            $transaction->rollBack();
            return $this->response->addError("Не удалось произвести загрузку данных!");
        }

        $parser = new InstrumentParserHelper($exchange_id);

        $chains = $parser->parseInstruments();
        foreach ($chains['tripples'] as $chain) {
            $result[] = [
                $exchange_id, PairChain::TYPE_TRIPPLES, $chain['pattern'], Json::encode($chain['base']), Json::encode($chain['dir']), time(), time()
            ];
        }
        foreach ($chains['quadripples'] as $chain) {
            $result[] = [
                $exchange_id, PairChain::TYPE_QUADRIPPLES, $chain['pattern'], Json::encode($chain['base']), Json::encode($chain['dir']), time(), time()
            ];
        }

        if (!PairChain::saveChains($result)) {
            $transaction->rollBack();
            return $this->response->addError("Не удалось произвести загрузку данных!");
        }

        try {
            $transaction->commit();
        } catch (Exception $e) {
            return $this->response->addError("Не удалось произвести сохранение!");
        }

        $count = PairChain::find()->where(['exchange_id' => $exchange_id])->count();
        $this->response->setContent(['chains' => $count]);
        return $this->response;
    }

    /**
     * @param int $id
     * @return \Exception|Exception
     */
    private function getConnector(int $id) {
        try {
            $exchange = Exchange::findOne($id);
        } catch (Exception $e) {
            return $e;
        }

        return new $exchange->class();
    }
}

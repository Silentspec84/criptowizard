<?php

namespace app\controllers;

use app\classes\connectors\ApiConnectorInterface;
use app\classes\connectors\PoloniexApiConnector;
use app\classes\helpers\ArrayHelper;
use app\classes\helpers\InstrumentParserHelper;
use app\classes\processors\DaemonProcessor;
use app\components\AccessRule;
use app\models\Daemon;
use app\models\Exchange;
use app\models\Instrument;
use app\models\Logs;
use app\models\PairChain;
use app\models\User;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;


/**
 * Class SiteApiController
 * @package app\controllers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */

class ExchangesApiController extends BaseApiController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['get-exchanges', 'load-pairs', 'load-chains', 'switch-exchange', 'switch-daemon', 'set-timer', 'load-balances'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['get-exchanges', 'load-pairs', 'load-chains', 'switch-exchange', 'switch-daemon', 'set-timer', 'load-balances'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function() {
                    $this->redirect('/');
                }
            ],
        ];
    }

    /**
     * Метод возвращает список бирж и дополнительную информацию по ним
     *
     * @return \app\classes\api\Response
     */
    public function actionGetExchanges()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $exchanges = Exchange::find()->asArray()->all();

        $result = [];
        foreach ($exchanges as $exchange) {
            $result[$exchange['id']]['id'] = $exchange['id'];
            $result[$exchange['id']]['name'] = $exchange['name'];
            $result[$exchange['id']]['link'] = $exchange['link'];
            $result[$exchange['id']]['commission'] = $exchange['commission'];
            $result[$exchange['id']]['pairs_count'] = Instrument::find()->where(['exchange_id' => $exchange['id']])->count();
            $result[$exchange['id']]['pairs_active'] = Instrument::find()->where(['exchange_id' => $exchange['id'], 'status' => Instrument::STATUS_ON])->count();
            $result[$exchange['id']]['chains_count'] = PairChain::find()->where(['exchange_id' => $exchange['id']])->count();
        }

        return $this->response->setContent(['exchanges' => $result]);
    }

    public function actionLoadPairs()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'string', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            return $this->response->addError('Ошибка загрузки пар с биржи', 'all_fields');
        }

        $exchange_id = $this->request_post['id'];

        $logger = Logs::getLogger($exchange_id, Logs::LOAD_PAIRS, true);
        $logger->saveMessage('Старт загрузки пар для биржи с id ' . $exchange_id);

        $command = 'cd /app && /usr/local/bin/php yii daemon-pairs/run ' . $exchange_id . " > /dev/null &";
        exec($command);

        return $this->response;
    }

    public function actionLoadBalances()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'string', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $exchange_id = $this->request_post['id'];
        /** @var Exchange $exchange */
        $exchange = Exchange::findOne($exchange_id);

        $logger = Logs::getLogger($exchange_id, Logs::CALC_BALANCES, true);
        $logger->saveMessage('Старт расчета балансов для биржи с id ' . $exchange_id);

        /** @var ApiConnectorInterface $api */
        $api = new $exchange->class($logger, $exchange);
        $api->getBalances();

        return $this->response;
    }

    public function actionLoadChains()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'string', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $exchange_id = $this->request_post['id'];

        $logger = Logs::getLogger($exchange_id, Logs::CALC_CHAINS, true);
        $logger->saveMessage('Старт расчета цепочек для биржи с id ' . $exchange_id);

        $command = 'cd /app && /usr/local/bin/php yii daemon-chains/run ' . $exchange_id . " > /dev/null &";
        exec($command);

        return $this->response;
    }

}

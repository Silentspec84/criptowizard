<?php

namespace app\controllers;

use app\classes\connectors\ApiConnectorInterface;
use app\classes\connectors\PoloniexApiConnector;
use app\classes\helpers\ArrayHelper;
use app\classes\helpers\InstrumentParserHelper;
use app\classes\processors\DaemonProcessor;
use app\components\AccessRule;
use app\models\Daemon;
use app\models\Exchange;
use app\models\Instrument;
use app\models\PairChain;
use app\models\User;
use Yii;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\helpers\Json;
use yii\helpers\Url;


/**
 * Class SiteApiController
 * @package app\controllers
 * @author Startbase Team <startbase-dev-team@b2b-center.ru>
 */

class ChainsApiController extends BaseApiController
{

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['get-chains', 'switch-chain'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['get-chains', 'switch-chain'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function() {
                    $this->redirect('/');
                }
            ],
        ];
    }

    /**
     * Метод возвращает список бирж и дополнительную информацию по ним
     *
     * @return \app\classes\api\Response
     */
    public function actionGetChains()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'integer', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $chains = [];
        $exchange_id = $this->request_post['id'];

        if ($exchange_id === 0) {
            $chains = PairChain::find()->all();
            $exchanges = Exchange::find()->indexBy('id')->all();
        } else {
            $chains = PairChain::find()->where(['exchange_id' => $exchange_id])->all();
            $exchanges = Exchange::find()->where(['id' => $exchange_id])->indexBy('id')->all();
        }

        $result = [];
        foreach ($chains as $chain) {
            /** @var PairChain $chain */
            $result[$chain->id]['chain_data'] = $chain;
            $result[$chain->id]['exchange_name'] = $exchanges[$chain->exchange_id]->name;
        }

        return $this->response->setContent(['chains' => $result]);
    }

    public function actionSwitchChain()
    {
        $post_config = [
            '_csrf' => ['type' => 'string', 'required' => true],
            'id' => ['type' => 'integer', 'required' => true]
        ];

        $this->getRequestValidator()->validate($post_config, $this->request_post);
        if ($this->getRequestValidator()->getResponse()->hasErrors()) {
            \Yii::warning('request data error: '.implode('<br>', $this->getRequestValidator()->getResponse()->errors));
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }

        $chain_id = $this->request_post['id'];
        /** @var Exchange $exchange */
        $chain = PairChain::findOne($chain_id);
        if (!$chain) {
            return $this->response->addError('Ошибка данных формы', 'all_fields');
        }
        $chain->status = $chain->status === PairChain::STATUS_OFF ? PairChain::STATUS_ON : PairChain::STATUS_OFF;
        if (!$chain->save()) {
            return $this->response->addError($chain->errors);
        }

        return $this->response->setContent(['status' => $chain->status]);
    }
}

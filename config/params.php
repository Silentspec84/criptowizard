<?php

return [
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'adminEmail' => 'lifeadvizer@yandex.ru',
    'itemsOnPage' => 10,
    'salt' => 'it_best_007'
];
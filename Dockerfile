# Для начала указываем исходный образ, он будет использован как основа
FROM php:7.1-fpm-jessie

# RUN выполняет идущую за ней команду в контексте нашего образа.
# В данном случае мы установим некоторые зависимости и модули PHP.
# Для установки модулей используем команду docker-php-ext-install.
# На каждый RUN создается новый слой в образе, поэтому рекомендуется объединять команды.
RUN apt-get update && apt-get install -y \
            libcurl4-openssl-dev \
            pkg-config \
            libssl-dev \
            curl \
            libmemcached-dev \
            wget \
            git \
            nano \
            libfreetype6-dev \
            libjpeg62-turbo-dev \
            libmcrypt-dev \
    && pecl install mongodb-1.2.2 \
    && docker-php-ext-install bcmath \
    && docker-php-ext-install -j$(nproc) iconv mcrypt mbstring mysqli pdo_mysql zip \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && curl -L -o /tmp/memcached.tar.gz "https://github.com/php-memcached-dev/php-memcached/archive/php7.tar.gz" \
    && mkdir -p /usr/src/php/ext/memcached \
    && tar -C /usr/src/php/ext/memcached -zxvf /tmp/memcached.tar.gz --strip 1 \
    && docker-php-ext-configure memcached \
    && docker-php-ext-install memcached \
    && rm /tmp/memcached.tar.gz \
    && cd /tmp \
    && curl -L 'https://github.com/EasyFreeHost/pecl-memcache/archive/81b1267413662dfc276f4530122cd1e7aba5c1fc.tar.gz' | tar xz \
    && cd pecl-memcache-81b1267413662dfc276f4530122cd1e7aba5c1fc \
    && phpize \
    && ./configure && make && make install && cd .. && rm -Rf pecl-memcache-81b1267413662dfc276f4530122cd1e7aba5c1fc \
    && docker-php-ext-enable memcache
# Куда же без composer'а.
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

### Maintainer Tolstikhin Vladimir <v.tolstikhin@b2b-center.ru>
# Install MSSQL driver
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -; \
    curl https://packages.microsoft.com/config/debian/8/prod.list > /etc/apt/sources.list.d/mssql-release.list; \
    \
    apt-get install -y apt-transport-https; \
    apt-get update; \
    \
    ACCEPT_EULA=Y apt-get install -y msodbcsql17 unixodbc-dev; \
    \
    # Install sqlsrv and pdo_sqlsrv extention
    \
    pecl install sqlsrv; \
    pecl install pdo_sqlsrv; \
    \
    # Enable sqlsrv and pdo_sqlsrv extention
    \
    echo extension=pdo_sqlsrv.so >> `php --ini | grep "Scan for additional .ini files" | sed -e "s|.*:\s*||"`/30-pdo_sqlsrv.ini; \
    echo extension=sqlsrv.so >> `php --ini | grep "Scan for additional .ini files" | sed -e "s|.*:\s*||"`/20-sqlsrv.ini;
# Добавим свой php.ini, можем в нем определять свои значения конфига
#ADD php.ini /usr/local/etc/php/conf.d/40-custom.ini


# Указываем рабочую директорию для PHP
WORKDIR /var/www

# Запускаем контейнер
# Из документации: The main purpose of a CMD is to provide defaults for an executing container. These defaults can include an executable, 
# or they can omit the executable, in which case you must specify an ENTRYPOINT instruction as well.
CMD ["php-fpm"]


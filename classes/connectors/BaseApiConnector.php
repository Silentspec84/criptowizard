<?php


namespace app\classes\connectors;


class BaseApiConnector
{
    protected const MAX = 10000000;

    protected $rate_limit = 80;

    protected $min_volume = 10;

    protected function getPage(string $link)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $link);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // возвращает веб-страницу
        curl_setopt($ch, CURLOPT_HEADER, 0);           // возвращает заголовки
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);        // таймаут ответа
        return curl_exec($ch);
    }

    /**
     * @param $link
     * @return string
     */
    protected function getPairsFromApi($link)
    {
        $result_json = '';
        while (true) {
            $result_json = $this->getPage($link);
            if (empty($result_json)) {
                sleep(3);
                continue;
            }
            break;
        }
        return $result_json;
    }

}
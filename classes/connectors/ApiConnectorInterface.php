<?php

namespace app\classes\connectors;

use app\models\Instrument;

interface ApiConnectorInterface
{

    /**
     * Метод возвращает массив настроек инструментов
     *
     * [
     *      pair_name =>
     *      [
     *          min_quantity => 1
     *          max_quantity => 1
     *          min_price => 1
     *          max_price => 1
     *          min_amount => 1
     *          max_amount => 1
     *      ]
     * ]
     *
     * min_quantity - минимальное кол-во по ордеру
     * max_quantity - максимальное кол-во по ордеру
     * min_price - минимальная цена по ордеру
     * max_price - максимальная цена по ордеру
     * min_amount - минимальная сумма по ордеру
     * max_amount - максимальная сумма по ордеру
     *
     * @return array
     */
    public function getPairsSettingsArray() : array;

    /**
     * Метод возвращает массив стаканов ордеров инструментов
     *
     * @param array $instruments массив объектов Instrument
     * @return array
     */
    public function getPairsOrdersArray(array $instruments) : array;

    public function getPairOrdersArray(string $instrument) : array;

    public function playPattern($deal, $pattern, $logger_trade, $strategy);

    public function getBalances();

}
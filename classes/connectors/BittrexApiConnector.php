<?php

namespace app\classes\connectors;

use app\models\Currency;
use app\models\Exchange;
use app\models\Instrument;
use app\models\Logs;
use app\models\PairChain;
use app\models\TopCurrency;
use yii\base\ErrorException;

class BittrexApiConnector extends BaseApiConnector implements ApiConnectorInterface
{

    private $balances = [];

    /** @var Exchange $exchange*/
    private $exchange;

    public $logger;

    public function __construct(Logs $logger)
    {
        $this->logger = $logger;
        $this->exchange = Exchange::findOne(['class' => 'app\classes\connectors\BittrexApiConnector']);
    }
    /**
     * Метод возвращает массив настроек инструментов
     *
     * @return array
     */
    public function getPairsSettingsArray() : array
    {
        // Получим инфу по торговым парам
        $this->logger->saveMessage('Получаем настройки пар по апи');
        $result_json = $this->getPairsFromApi('https://api.bittrex.com/api/v1.1/public/getmarkets');
        if (!$result_json) {
            $this->logger->saveMessage('Не удалось получить настройки пар по апи');
            return [];
        }
        $pairs_data = json_decode($result_json, true);
        if (empty($pairs_data)) {
            $this->logger->saveMessage('Ошибка получения настроек пар');
            return [];
        }
        $this->logger->saveMessage('Получили настройки пар по апи в количестве ' . count($pairs_data) . ', фильтруем');

        // Получим только пары с валютами из топчика
        $pairs_data = $this->getFilteredPairsByTopCurrencies($pairs_data);
        if (empty($pairs_data)) {
            $this->logger->saveMessage('Ошибка фильтрации пар по топу валют');
            return [];
        }
        $this->logger->saveMessage('Отфильтровали пары по топу валют');

        // Собираем данные по настройкам пар
        $this->logger->saveMessage('Собираем данные по настройкам пар');
        $pairs = [];
        $count = 0;
        foreach ($pairs_data['result'] as $symbol) {
            $pair_name = $symbol['BaseCurrency'] . '_' . $symbol['MarketCurrency'];
            $pairs[$count] = [$this->exchange->id, $pair_name, $symbol['active'], time(), time()];
            $count++;
        }
        $this->logger->saveMessage('Данные успешно получены, обработано ' . count($pairs) . ' пар');
        return $pairs;
    }

    /**
     * Получает на вход настройки по всем парам из апи, возвращает только настройки тех пар,
     * валюты из которых входят в топ. Число топа в переменной rate_limit.
     *
     * @param $pairs_data
     * @return array
     */
    private function getFilteredPairsByTopCurrencies($pairs_data)
    {
        $top_currencies = TopCurrency::find()->asArray()->indexBy('name')->limit($this->rate_limit)->all();
        if (empty($top_currencies)) {
            return [];
        }
        foreach ($pairs_data['result'] as $pair_key => $pair_settings) {
            $pairs_data['result'][$pair_key]['active'] = 1;
            if (empty($top_currencies[$pair_settings['MarketCurrency']]) || empty($top_currencies[$pair_settings['BaseCurrency']])) {
                $pairs_data['result'][$pair_key]['active'] = 0;
            }
        }
        return $pairs_data;
    }

    /**
     * Метод возвращает массив стаканов ордеров инструментов
     *
     * @param array $instruments массив объектов Instrument
     * @return array
     */
    public function getPairsOrdersArray(array $instruments) : array
    {
        $result = [];
        /** @var Instrument $instrument */
        foreach ($instruments as $instrument) {
            $pair = str_replace('_', '-', $instrument->name);
            $link = 'https://api.bittrex.com/api/v1.1/public/getorderbook?market=' . $pair . '&type=both';
            while (true) {
                $result_json = $this->getPage($link);
                if (!$result_json) {
                    sleep(3);
                    continue;
                }
                break;
            }
            if (!$result_json) {
                $this->logger->saveMessage('Не удалось получить настройки пар по апи');
                continue;
            }
            $data = json_decode($result_json, true);
            foreach ($data['result']['sell'] as $ask) {
                $result[$instrument->name]['ask'][] = [$ask['Rate'], $ask['Quantity'], $ask['Rate'] * $ask['Quantity']];
            }
            foreach ($data['result']['buy'] as $bid) {
                $result[$instrument->name]['bid'][] = [$bid['Rate'], $bid['Quantity'], $bid['Rate'] * $bid['Quantity']];
            }
        }
        return $result;
    }

    /**
     * Метод возвращает массив стаканов ордеров инструментов
     *
     * @param array $instruments массив объектов Instrument
     * @return array
     */
    public function getPairsOrdersArrayByPairsNames(array $instruments) : array
    {
        $result = [];
        foreach ($instruments as $instrument) {
            $pair = str_replace('_', '-', $instrument);
            $link = 'https://api.bittrex.com/api/v1.1/public/getorderbook?market=' . $pair . '&type=both';
            while (true) {
                $result_json = $this->getPage($link);
                if (!$result_json) {
                    sleep(3);
                    continue;
                }
                break;
            }
            if (!$result_json) {
                $this->logger->saveMessage('Не удалось получить настройки пар по апи');
                continue;
            }
            $data = json_decode($result_json, true);
            foreach ($data['result']['sell'] as $ask) {
                $result[$instrument]['ask'][] = [$ask['Rate'], $ask['Quantity'], $ask['Rate'] * $ask['Quantity']];
            }
            foreach ($data['result']['buy'] as $bid) {
                $result[$instrument]['bid'][] = [$bid['Rate'], $bid['Quantity'], $bid['Rate'] * $bid['Quantity']];
            }
        }
        return $result;
    }

    public function getPairOrdersArray(string $instrument) : array
    {
        return [];
    }

    /**
     * @param $deal
     * @param $pattern
     * @param $logger_trade
     * @param $strategy
     * @return bool
     */
    public function playPattern($deal, $pattern, $logger_trade, $strategy)
    {
        return true;
    }

    /**
     * Возвращает массив объектов Currency
     *
     * @return array
     */
    public function getBalances()
    {
        $this->logger->saveMessage('Получаем балансы из бд');
        $balances = Currency::find()->where(['exchange_id' => $this->exchange->id])->indexBy('currency')->orderBy(['date_modified' => SORT_DESC])->all();
        /** @var Currency $first_balance_element */
        $first_balance_element = reset($balances);
        if (empty($balances) || !$first_balance_element || time() - $first_balance_element->date_modified >= 20) {
            $this->logger->saveMessage('Информация о балансах в системе отсутствует или устарела, получаем новую по апи');
            $result = []; //todo тут сделать получение балансов
            if (!$result) {
                $this->logger->saveMessage('Информация о балансах по апи не была получена');
                return [];
            }
            $this->logger->saveMessage('Информация о балансах по апи получена, сохраняем ' . count($result['balances']) . ' записей');
            foreach ($result['balances'] as $currency_name => $currency_balance) {
                if (array_key_exists($currency_name, $balances)) {
                    $balances[$currency_name]->deposit = $currency_balance;
                    $balances[$currency_name]->save();
                    continue;
                }
                $currency = new Currency();
                $currency->exchange_id = $this->exchange->id;
                $currency->currency = $currency_name;
                $currency->min_volume = 0;
                $currency->deposit = $currency_balance;
                $currency->status = 0;
                $currency->date_last_connected = time();
                $currency->save();
                $balances[$currency_name] = $currency;
            }
            $this->logger->saveMessage('Сохранение прошло успешно');
        }
        $this->logger->saveMessage('Возвращаем массив объектов Currency в количестве ' . count($balances) . ' объектов');
        return $balances;
    }
}
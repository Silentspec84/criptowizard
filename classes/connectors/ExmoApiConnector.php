<?php

namespace app\classes\connectors;

use app\models\Currency;
use app\models\Daemon;
use app\models\Deal;
use app\models\Exchange;
use app\models\Instrument;
use app\models\Logs;
use app\models\PairChain;
use app\models\TopCurrency;
use yii\helpers\Json;

class ExmoApiConnector extends BaseApiConnector implements ApiConnectorInterface
{

    private const NOT_ENOUGH_DEPO = 'Error 50053: Insufficient funds';

    /** @var Exchange $exchange*/
    private $exchange;

    public $logger;

    private $currencies;

    protected $rate_limit = 100;

    public function __construct(Logs $logger)
    {
        $this->logger = $logger;
        $this->exchange = Exchange::findOne(['class' => 'app\classes\connectors\ExmoApiConnector']);
        $this->currencies = Currency::find()->where(['exchange_id' => $this->exchange->id, 'status' => 1])->indexBy('currency')->all();
        if (!$this->currencies) {
            $this->getBalances();
        }
    }

    /**
     * Метод возвращает массив настроек инструментов
     *
     * @return array
     */
    public function getPairsSettingsArray() : array
    {
        // Получим инфу по торговым парам
        $this->logger->saveMessage('Получаем настройки пар по апи');
        $result_json = $this->getPairsFromApi('https://api.exmo.me/v1/pair_settings/');
        if (!$result_json) {
            $this->logger->saveMessage('Не удалось получить настройки пар по апи');
            return [];
        }
        $pairs_data = json_decode($result_json, true);
        if (empty($pairs_data)) {
            $this->logger->saveMessage('Ошибка получения настроек пар');
            return [];
        }
        $this->logger->saveMessage('Получили настройки пар по апи в количестве ' . count($pairs_data) . ', фильтруем');

        // Получим только пары с валютами из топчика
        $pairs_data = $this->getFilteredPairsByTopCurrencies($pairs_data);
        if (empty($pairs_data)) {
            $this->logger->saveMessage('Ошибка фильтрации пар по топу валют');
            return [];
        }
        $this->logger->saveMessage('Отфильтровали пары по топу валют');

        // Собираем данные по настройкам пар
        $this->logger->saveMessage('Собираем данные по настройкам пар');
        $pairs = [];
        $count = 0;
        foreach ($pairs_data as $pair_name => $pair_settings) {
            $pairs[$count] = [$this->exchange->id, $pair_name, $pair_settings['active'], time(), time()];
            $count++;
        }
        $this->logger->saveMessage('Данные успешно получены, обработано ' . count($pairs) . ' пар');
        return $pairs;
    }

    /**
     * Получает на вход настройки по всем парам из апи, возвращает только настройки тех пар,
     * валюты из которых входят в топ. Число топа в переменной rate_limit.
     *
     * @param $pairs_data
     * @return array
     */
    private function getFilteredPairsByTopCurrencies($pairs_data)
    {
        $top_currencies = TopCurrency::find()->asArray()->indexBy('name')->limit($this->rate_limit)->all();
        if (empty($top_currencies)) {
            return [];
        }
        foreach ($pairs_data as $pair_name => $pair_settings) {
            $currencies = explode('_', $pair_name);
            $pairs_data[$pair_name]['active'] = 1;
            if (empty($top_currencies[$currencies[0]]) || empty($top_currencies[$currencies[1]])) {
                $pairs_data[$pair_name]['active'] = 0;
            }
        }
        return $pairs_data;
    }

    /**
     * Метод возвращает массив стаканов ордеров инструментов
     *
     * @param array $instruments массив объектов Instrument
     * @return array
     */
    public function getPairsOrdersArray(array $instruments) : array
    {
        while (true) {
            $result_json = $this->getPage($this->createOrderGetLink($instruments));
            if (!$result_json) {
                sleep(3);
                continue;
            }
            break;
        }

        return json_decode($result_json, true);
    }

    public function getPairOrdersArray(string $instrument) : array
    {
        $success = false;
        while (!$success) {
            $link = 'https://api.exmo.me/v1/order_book/?limit=10&pair=' . $instrument;
            $result_json = $this->getPage($link);
            if (!$result_json) {
                sleep(3);
                continue;
            }
            $success = true;
            break;
        }

        return json_decode($result_json, true);
    }

    public function getPairsOrdersArrayByPairsNames(array $instruments) : array
    {
        $this->logger->saveMessage('Формируем ссылку');
        $link = 'https://api.exmo.me/v1/order_book/?limit=10&pair=';
        foreach ($instruments as $instrument) {
            $link .= $instrument . ',';
        }
        $this->logger->saveMessage('Ссылка сформирована, получаем стаканы');
        while (true) {
            $result_json = $this->getPage($link);
            if (!$result_json) {
                sleep(3);
                continue;
            }
            break;
        }
        if (!$result_json) {
            $this->logger->saveMessage('Ошибка получения стаканов цен');
            return [];
        }
        $result = json_decode($result_json, true);
        $this->logger->saveMessage('Стаканы получены в количестве ' . count($result));
        return $result;
    }

    /**
     * Возвращает массив объектов Currency
     *
     * @return array
     */
    public function getBalances()
    {
        $this->logger->saveMessage('Получаем балансы из бд');
        $balances = Currency::find()->where(['exchange_id' => $this->exchange->id])->indexBy('currency')->orderBy(['date_modified' => SORT_DESC])->all();
        /** @var Currency $first_balance_element */
        $first_balance_element = reset($balances);
        if (empty($balances) || !$first_balance_element || time() - $first_balance_element->date_modified >= 20) {
            $this->logger->saveMessage('Информация о балансах в системе отсутствует или устарела, получаем новую по апи');
            $result = $this->api_query('user_info');
            if (!$result) {
                $this->logger->saveMessage('Информация о балансах по апи не была получена');
                return [];
            }
            $this->logger->saveMessage('Информация о балансах по апи получена, сохраняем ' . count($result['balances']) . ' записей');
            foreach ($result['balances'] as $currency_name => $currency_balance) {
                if (array_key_exists($currency_name, $balances)) {
                    $balances[$currency_name]->deposit = $currency_balance;
                    $balances[$currency_name]->save();
                    continue;
                }
                $currency = new Currency();
                $currency->exchange_id = $this->exchange->id;
                $currency->currency = $currency_name;
                $currency->min_volume = 0;
                $currency->deposit = $currency_balance;
                $currency->status = 0;
                $currency->date_last_connected = time();
                $currency->save();
                $balances[$currency_name] = $currency;
            }
            $this->logger->saveMessage('Сохранение прошло успешно');
        }
        $this->logger->saveMessage('Возвращаем массив объектов Currency в количестве ' . count($balances) . ' объектов');
        return $balances;
    }

    /**
     * @param array $deal
     * @param PairChain $pattern
     * @param Logs $logger_trade
     * @return bool
     */
    public function playPattern($deal, $pattern, $logger_trade, $strategy)
    {
        $logger_trade->saveMessage('Запуск торгового модуля');
        $pattern_pairs = explode('-', $pattern->pattern);
        // order 0
        $logger_trade->saveMessage('Первая пара ' . $pattern_pairs[0]);
        $base_currency = explode('_', $pattern_pairs[0])[0];
        $logger_trade->saveMessage('Базовая валюта ' . $base_currency);
        $max_volume = $this->currencies[$base_currency]->deposit;
        $logger_trade->saveMessage('Максимальная ставка ' . $max_volume);
        if (!$max_volume) {
            $logger_trade->saveMessage('Ошибка получения размера депозита: ' . $max_volume);
            return false;
        }

        $pair = $pattern->base[0];
        $logger_trade->saveMessage('Базовая пара 1 ' . $pair);
        $base_volume = (float)$deal[0]['base_volume'];
        $logger_trade->saveMessage('Базовый объем пары ' . $base_volume);
        if ($base_volume > $max_volume) {
            $quantity = $max_volume;
        } else {
            $quantity = $base_volume;
        }
        $base_quantity = $quantity;
        $logger_trade->saveMessage('Входящий объем первой сделки ' . $base_quantity);
        $dir = $deal[0]['dir'];
        $price = 0;
        $logger_trade->saveMessage('Направление первой сделки ' . $dir);
        if ($pattern_pairs[0] !== $pair) {
            $type = $dir === 1 ? 'market_buy_total' : 'market_sell_total';
        } else {
            $type = $dir === 1 ? 'market_buy' : 'market_sell';
        }
        $logger_trade->saveMessage('Тип ордера первой сделки ' . $type);
        $trade_data = [
            'pair' => $pair,
            'quantity' => $quantity,
            'price' => $price,
            'type' => $type
        ];
        $return_data['result'] = false;
        $return_data = $this->api_query('order_create', $trade_data);
        $order_id = $return_data['order_id'];
        if (!$order_id) {
            $data = Json::encode($return_data);
            $logger_trade->saveMessage('Ошибка отправки ордера: ' . $data);
            if (empty($return_data['error']) || $return_data['error'] !== self::NOT_ENOUGH_DEPO) {
                $logger_trade->saveMessage('Неразрешимая ошибка отправки ордера: ' . $data);
                return false;
            }
            if ($return_data['error'] === self::NOT_ENOUGH_DEPO) {
                $this->currencies = $this->getBalances();
                $max_volume = $this->currencies[$base_currency]->deposit;
                $trade_data = [
                    'pair' => $pair,
                    'quantity' => $max_volume,
                    'price' => $price,
                    'type' => $type
                ];
                $return_data = $this->api_query('order_create', $trade_data);
                $order_id = $return_data['order_id'];
                if (!$order_id) {
                    $logger_trade->saveMessage('Неразрешимая ошибка отправки ордера: ' . $data);
                    return false;
                }
            }
        }
        $logger_trade->saveMessage('Запуск первой сделки, id на бирже ' . $order_id);
        $return_data_next = $this->api_query('order_trades', ['order_id' => $order_id]);
        $logger_trade->saveMessage('$return_data_next ' . Json::encode($return_data_next));

        // order 1
        $pair = $pattern->base[1];
        $logger_trade->saveMessage('Базовая пара 2 ' . $pair);
        $quantity = ($return_data_next['in_amount'] - $return_data_next['in_amount'] * $this->exchange->commission / 100);
        $logger_trade->saveMessage('Объем второй сделки ' . $quantity);
        $dir = $deal[1]['dir'];
        $logger_trade->saveMessage('Направление второй сделки ' . $dir);
        $price = 0;
        if ($pattern_pairs[1] !== $pair) {
            $type = $dir === 1 ? 'market_buy_total' : 'market_sell_total';
        } else {
            $type = $dir === 1 ? 'market_buy' : 'market_sell';
        }
        $logger_trade->saveMessage('Тип ордера второй сделки ' . $type);
        $trade_data = [
            'pair' => $pair,
            'quantity' => $quantity,
            'price' => $price,
            'type' => $type
        ];

        $return_data = $this->api_query('order_create', $trade_data);
        $order_id = $return_data['order_id'];
        if (!$order_id) {
            $data = Json::encode($return_data);
            $logger_trade->saveMessage('Ошибка отправки ордера: ' . $data);
            if (empty($return_data['error']) || $return_data['error'] !== self::NOT_ENOUGH_DEPO) {
                $logger_trade->saveMessage('Неразрешимая ошибка отправки ордера: ' . $data);
                return false;
            }
            if ($return_data['error'] === self::NOT_ENOUGH_DEPO) {
                $this->currencies = $this->getBalances();
                $max_volume = $this->currencies[$base_currency]->deposit;
                $trade_data = [
                    'pair' => $pair,
                    'quantity' => $max_volume,
                    'price' => $price,
                    'type' => $type
                ];
                $return_data = $this->api_query('order_create', $trade_data);
                $order_id = $return_data['order_id'];
                if (!$order_id) {
                    $logger_trade->saveMessage('Неразрешимая ошибка отправки ордера: ' . $data);
                    return false;
                }
            }
        }
        $logger_trade->saveMessage('Запуск второй сделки, id на бирже ' . $order_id);
        $return_data_next = $this->api_query('order_trades', ['order_id' => $order_id]);
        $logger_trade->saveMessage('$return_data_next ' . Json::encode($return_data_next));

        // order 2
        $pair = $pattern->base[2];
        $logger_trade->saveMessage('Базовая пара 3 ' . $pair);
        $quantity = ($return_data_next['in_amount'] - $return_data_next['in_amount'] * $this->exchange->commission / 100);
        $logger_trade->saveMessage('Объем третьей сделки ' . $quantity);
        $dir = $deal[2]['dir'];
        $logger_trade->saveMessage('Направление третьей сделки ' . $dir);
        $price = 0;

        if ($pattern_pairs[2] !== $pair) {
            $type = $dir === 1 ? 'market_buy_total' : 'market_sell_total';
        } else {
            $type = $dir === 1 ? 'market_buy' : 'market_sell';
        }
        $logger_trade->saveMessage('Тип ордера третьей сделки ' . $type);
        $trade_data = [
            'pair' => $pair,
            'quantity' => $quantity,
            'price' => $price,
            'type' => $type
        ];
        $return_data = $this->api_query('order_create', $trade_data);
        $order_id = $return_data['order_id'];
        if (!$order_id) {
            $data = Json::encode($return_data);
            $logger_trade->saveMessage('Ошибка отправки ордера: ' . $data);
            if (empty($return_data['error']) || $return_data['error'] !== self::NOT_ENOUGH_DEPO) {
                $logger_trade->saveMessage('Неразрешимая ошибка отправки ордера: ' . $data);
                return false;
            }
            if ($return_data['error'] === self::NOT_ENOUGH_DEPO) {
                $this->currencies = $this->getBalances();
                $max_volume = $this->currencies[$base_currency]->deposit;
                $trade_data = [
                    'pair' => $pair,
                    'quantity' => $max_volume,
                    'price' => $price,
                    'type' => $type
                ];
                $return_data = $this->api_query('order_create', $trade_data);
                $order_id = $return_data['order_id'];
                if (!$order_id) {
                    $logger_trade->saveMessage('Неразрешимая ошибка отправки ордера: ' . $data);
                    return false;
                }
            }
        }
        $logger_trade->saveMessage('Запуск третьей сделки, id на бирже ' . $order_id);
        $return_data_next = $this->api_query('order_trades', ['order_id' => $order_id]);
        $logger_trade->saveMessage('$return_data_next ' . Json::encode($return_data_next));

        if ($pattern->type === PairChain::TYPE_QUADRIPPLES) {
            // order 3
            $pair = $pattern->base[3];
            $logger_trade->saveMessage('Базовая пара 4 ' . $pair);
            $quantity = ($return_data_next['in_amount'] - $return_data_next['in_amount'] * $this->exchange->commission / 100);
            $logger_trade->saveMessage('Объем четвертой сделки ' . $quantity);
            $dir = $deal[3]['dir'];
            $logger_trade->saveMessage('Направление четвертой сделки ' . $dir);
            $price = 0;

            if ($pattern_pairs[3] !== $pair) {
                $type = $dir === 1 ? 'market_buy_total' : 'market_sell_total';
            } else {
                $type = $dir === 1 ? 'market_buy' : 'market_sell';
            }
            $logger_trade->saveMessage('Тип ордера четвертой сделки ' . $type);
            $trade_data = [
                'pair' => $pair,
                'quantity' => $quantity,
                'price' => $price,
                'type' => $type
            ];
            $return_data = $this->api_query('order_create', $trade_data);
            $order_id = $return_data['order_id'];
            if (!$order_id) {
                $data = Json::encode($return_data);
                $logger_trade->saveMessage('Ошибка отправки ордера: ' . $data);
                if (empty($return_data['error']) || $return_data['error'] !== self::NOT_ENOUGH_DEPO) {
                    $logger_trade->saveMessage('Неразрешимая ошибка отправки ордера: ' . $data);
                    return false;
                }
                if ($return_data['error'] === self::NOT_ENOUGH_DEPO) {
                    $this->currencies = $this->getBalances();
                    $max_volume = $this->currencies[$base_currency]->deposit;
                    $trade_data = [
                        'pair' => $pair,
                        'quantity' => $max_volume,
                        'price' => $price,
                        'type' => $type
                    ];
                    $return_data = $this->api_query('order_create', $trade_data);
                    $order_id = $return_data['order_id'];
                    if (!$order_id) {
                        $logger_trade->saveMessage('Неразрешимая ошибка отправки ордера: ' . $data);
                        return false;
                    }
                }
            }
            $logger_trade->saveMessage('Запуск четвертой сделки, id на бирже ' . $order_id);
            $return_data_next = $this->api_query('order_trades', ['order_id' => $order_id]);
            $logger_trade->saveMessage('$return_data_next ' . Json::encode($return_data_next));
        }
        $profit = (float)$return_data_next['in_amount'] - $base_quantity;
        $logger_trade->saveMessage('Профит в валюте составил ' . $profit);
        /** @var Currency $curruncy */
        $curruncy = $this->currencies[$base_currency];
        $last_depo = $curruncy->deposit;
        $curruncy->deposit += $profit;
        $new_depo = $curruncy->deposit;
        if (!$curruncy->save()) {
            $logger_trade->saveMessage('Ошибка сохранения валюты ' . $curruncy->errors);
            return false;
        }
        $logger_trade->saveMessage('Новое значение баланса ' . $curruncy->deposit . ' ' . $curruncy->currency);
        $time = time() - time() % 360;
        $hash = md5($pattern->pattern . $time);
        $order = new Deal();
        $order->hash = $hash;
        $order->exchange_id = $this->exchange->id;
        $order->strategy = $strategy;
        $order->type = Deal::TYPE_REAL;
        $order->deal = $deal;
        $order->pattern = $pattern;
        $order->profit_perc = ($new_depo / $last_depo - 1) * 100;
        $order->profit = $profit;
        if (!$order->save()) {
            $logger_trade->saveMessage('Ошибка сохранения ордера ' . $order->errors);
            return false;
        }
        return true;
    }

    private function createOrderGetLink(array $instruments): string
    {
        $link = 'https://api.exmo.me/v1/order_book/?limit=10&pair=';
        /** @var Instrument $instrument */
        foreach ($instruments as $instrument) {
            $link .= $instrument->name . ',';
        }
        return $link;
    }

    private function api_query($api_name, array $req = [])
    {
        $mt = explode(' ', microtime());
        $NONCE = $mt[1] . substr($mt[0], 2, 6);

        // API settings
        $key = "K-d385b508e284422fa0a9f80bbeab5c3034459468"; //TODO replace with your api key from profile page
        $secret = "S-9bb66a23c720eb2f2cf2212ae5d8fa73d07b5920"; //TODO replace with your api secret from profile page

        $url = "http://api.exmo.me/v1/$api_name";

        $req['nonce'] = $NONCE;

        // generate the POST data string
        $post_data = http_build_query($req, '', '&');

        $sign = hash_hmac('sha512', $post_data, $secret);

        // generate the extra headers
        $headers = array(
            'Sign: ' . $sign,
            'Key: ' . $key,
        );

        // our curl handle (initialize if required)
        static $ch = null;
        if ($ch === null) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; PHP client; ' . php_uname('s') . '; PHP/' . phpversion() . ')');
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

        // run the query
        $res = curl_exec($ch);
        return json_decode($res, true);
    }
}
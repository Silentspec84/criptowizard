<?php


namespace app\classes\strategies;


use app\classes\connectors\ApiConnectorInterface;
use app\models\Daemon;
use app\models\Exchange;
use app\models\Logs;
use app\models\PairChain;

class Strategytest_strategy
{
    public function __construct(Exchange $exchange, ApiConnectorInterface $api, int $type, Logs $logger)
    {
        $this->exchange = $exchange;
        $this->type = $type;
        $this->api = $api;
        $this->logger = $logger;
        $this->getCurrencies();
        $condition = $this->type === Daemon::TYPE_TRADE
            ? ['exchange_id' => $exchange->id, 'first_currency' => array_keys($this->currencies)]
            : ['exchange_id' => $exchange->id];
        $this->patterns = PairChain::find()->where($condition)->all();
        $this->getPrices();

        $this->ready = true;
    }
}
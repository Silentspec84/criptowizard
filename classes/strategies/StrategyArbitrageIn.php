<?php

namespace app\classes\strategies;

use app\classes\connectors\ApiConnectorInterface;
use app\models\Currency;
use app\models\Daemon;
use app\models\Deal;
use app\models\Exchange;
use app\models\Instrument;
use app\models\Logs;
use app\models\PairChain;
use yii\helpers\Json;

class StrategyArbitrageIn implements StrategyInterface
{
    private $patterns = [];

    private $prices = [];

    private $currencies = [];

    private $exchange;

    private $api;

    public $ready;

    private $type;

    private $logger;

    public function __construct(Exchange $exchange, ApiConnectorInterface $api, int $type, Logs $logger)
    {
        $this->exchange = $exchange;
        $this->type = $type;
        $this->api = $api;
        $this->logger = $logger;
        $this->getCurrencies();
        $condition = $this->type === Daemon::TYPE_TRADE
            ? ['exchange_id' => $exchange->id, 'first_currency' => array_keys($this->currencies)]
            : ['exchange_id' => $exchange->id];
        $this->patterns = PairChain::find()->where($condition)->all();
        $this->getPrices();

        $this->ready = true;
    }

    private function getCurrencies()
    {
        if ($this->type === Daemon::TYPE_TRADE) {
            $this->currencies = Currency::find()->where(['exchange_id' => $this->exchange->id, 'status' => 1])->asArray()->indexBy('currency')->all();
            if (!$this->currencies) {
                $this->api->getBalances();
                $this->ready = false;
            }
        } else {
            $this->currencies = Currency::getBalancesForTest();
        }
        $this->logger->saveMessage('Получили валюты');
    }

    public function getPrices()
    {
        $instruments = Instrument::find()->where(['exchange_id' => $this->exchange->id, 'status' => Instrument::STATUS_ON])->all();

        $prices = $this->api->getPairsOrdersArray($instruments);
        if (!$prices) {
            $this->ready = false;
            $this->logger->saveMessage('При получении цен что-то пошло не так');
            return;
        }

        foreach ($prices as $price_name => $price) {
            if (empty($price['ask']) || empty($price['bid'])) {
                continue;
            }
            $asks = array_slice($price['ask'], 0, 10);
            $bids = array_slice($price['bid'], 0, 10);
            $this->prices[$price_name]['ask'] = $asks;
            $this->prices[$price_name]['bid'] = $bids;
        }

        $this->logger->saveMessage('Получили цены');
    }

    public function calculate()
    {
        /** @var PairChain $pattern */
        foreach ($this->patterns as $pattern) {
            $trade = [
                ['dir' => $pattern->dir[0]],
                ['dir' => $pattern->dir[1]],
                ['dir' => $pattern->dir[2]],
            ];
            $last_index = 2;
            if ($pattern->type === PairChain::TYPE_QUADRIPPLES) {
                $trade[] =  ['dir' => $pattern->dir[3]];
                $last_index = 3;
            }
            // deal 0
            $trade = $this->fillFirstTrade($trade, $pattern);
            if ($trade[0] === -1) {
                unset($trade);
                continue;
            }

            // deal 1
            $trade = $this->fillNextTrade($trade, 1, $pattern);
            if ($trade[0] === -1) {
                unset($trade);
                continue;
            }

            // deal 2
            $trade = $this->fillNextTrade($trade, 2, $pattern);
            if ($trade[0] === -1) {
                unset($trade);
                continue;
            }

            if ($pattern->type === PairChain::TYPE_QUADRIPPLES) {
                $trade = $this->fillNextTrade($trade, 3, $pattern);
                if ($trade[0] === -1) {
                    unset($trade);
                    continue;
                }
            }

            $trade['profit'] = $trade[$last_index]['volume'] - $trade[0]['base_volume'];
            $trade['profit_perc'] = (1 - $trade[0]['base_volume'] / $trade[$last_index]['volume']) * 100;

            if ($trade['profit_perc'] > 0.1) {
                $time = time() - time() % 360;
                $hash = md5($pattern->pattern . $time);
                $orders[] = [
                    $hash, $this->exchange->id, 'StrategyArbitrageIn', Deal::TYPE_TEST, Json::encode($trade), Json::encode($pattern),
                    $trade['profit_perc'], $trade['profit'], time(), time()
                ];
                if ($this->type === Daemon::TYPE_TRADE) {
                    $this->logger->saveMessage('Найдена сделка с профитом ' . $trade['profit_perc'] . ', в базовой валюте ' . $trade['profit']);
                    $result = $this->api->playPattern($trade, $pattern, $this->logger, 'StrategyArbitrageInTrade');
                    if ($result) {
                        $this->logger->saveMessage('Сделка проведена успешно');
                        return;
                    }
                    $this->logger->saveMessage('Что-то пошло не так!');
                    return;
                }
            }
            unset($trade);
        }

        Deal::saveDeals($orders);
        $this->logger->clearMessage();
        return;
    }

    /**
     * @param array $trade
     * @param PairChain $pattern
     * @return array|int
     */
    private function fillFirstTrade(array $trade, PairChain $pattern): array
    {
        if ($trade[0]['dir'] === BaseStrategy::BUY) {
            if (empty($this->prices[$pattern->base[0]]['ask'])) {
                return [-1];
            }
            $price_index = $this->getPricesStartIndex($this->prices[$pattern->base[0]]['ask'], $pattern);
            if ($price_index === -1) {
                return [$price_index];
            }
            $trade[0]['price'] = (float)$this->prices[$pattern->base[0]]['ask'][$price_index][0];
            $trade[0]['volume'] =
                $this->prices[$pattern->base[0]]['ask'][$price_index][1]
                - $this->prices[$pattern->base[0]]['ask'][$price_index][1] * $this->exchange->commission / 100;
            $trade[0]['base_volume'] = $this->prices[$pattern->base[0]]['ask'][$price_index][1] * $trade[0]['price'];
        } else {
            if (empty($this->prices[$pattern->base[0]]['bid'])) {
                return [-1];
            }
            $price_index = $this->getPricesStartIndex($this->prices[$pattern->base[0]]['bid'], $pattern);
            if ($price_index === -1) {
                return [$price_index];
            }
            $trade[0]['price'] = (float)$this->prices[$pattern->base[0]]['bid'][$price_index][0];
            $trade[0]['base_volume'] = (float)$this->prices[$pattern->base[0]]['bid'][$price_index][1];
            $trade[0]['volume'] =
                $trade[0]['base_volume'] * $trade[0]['price']
                - $trade[0]['base_volume'] * $trade[0]['price'] * $this->exchange->commission / 100;
        }
        return $trade;
    }

    /**
     * @param array $prices
     * @param PairChain $data
     * @return int
     */
    private function getPricesStartIndex(array $prices, PairChain $data): int
    {
        $pattern = $data->pattern;
        $base = $data->base;
        $currencies = explode('_', $base[0]);
        $first_trade_pair = explode('-', $pattern)[0];
        $first_base_currency = explode('_', $first_trade_pair)[0];
        $next_base_currency = explode('_', $first_trade_pair)[1];
        if ($currencies[0] === $first_base_currency && $currencies[1] === $next_base_currency) {
            if (!empty($this->currencies[$next_base_currency])) {
                $min_volume = $this->currencies[$next_base_currency]['min_volume'];
            } else {
                $min_volume = 0;
            }
            $index = 1;
        }
        if ($currencies[0] === $next_base_currency && $currencies[1] === $first_base_currency) {
            if (!empty($this->currencies[$first_base_currency])) {
                $min_volume = $this->currencies[$first_base_currency]['min_volume'];
            } else {
                $min_volume = 0;
            }
            $index = 2;
        }
        foreach ($prices as $price_index => $price) {
            if ($price[$index] < $min_volume) {
                continue;
            }
            return $price_index;
        }
        return -1;
    }

    /**
     * @param array $trade
     * @param int $index
     * @param PairChain $pattern
     * @return array
     */
    private function fillNextTrade(array $trade, int $index, PairChain $pattern): array
    {
        if (empty($this->prices[$pattern->base[$index]]['ask']) || empty($this->prices[$pattern->base[$index]]['bid'])) {
            return [-1];
        }
        if ($trade[$index]['dir'] === BaseStrategy::BUY) {
            $price_index = $this->getFittingIndex($this->prices[$pattern->base[$index]]['ask'], $trade[$index - 1]['volume'], $pattern, $index);
            if ($price_index === -1) {
                // Если не нашли такого объема, берем новую цепочку ордеров
                return [$price_index];
            }
            $trade[$index]['price'] = $this->prices[$pattern->base[$index]]['ask'][$price_index][0];
            if (!isset($trade[$index]['price'])) {
                return [-1];
            }
            $trade[$index]['base_volume'] = $trade[$index - 1]['volume'];
            $trade[$index]['volume'] =
                ($trade[$index]['base_volume'] / $trade[$index]['price'])
                - ($trade[$index]['base_volume'] / $trade[$index]['price']) * $this->exchange->commission / 100;
        } else {
            $price_index = $this->getFittingIndex($this->prices[$pattern->base[$index]]['bid'], $trade[$index - 1]['volume'], $pattern, $index);
            $trade[$index]['price'] = $this->prices[$pattern->base[$index]]['bid'][$price_index][0];
            if (!isset($trade[$index]['price'])) {
                return [-1];
            }
            $trade[$index]['base_volume'] = $trade[$index - 1]['volume'];
            $trade[$index]['volume'] =
                ($trade[$index]['base_volume'] * $trade[$index]['price'])
                - ($trade[$index]['base_volume'] * $trade[$index]['price']) * $this->exchange->commission / 100;
        }
        return $trade;
    }

    /**
     * @param array $prices
     * @param float $min_volume
     * @return int
     */
    private function getFittingIndex(array $prices, float $min_volume, PairChain $pattern, int $index) : int
    {
        $pair_base = $pattern->base[$index];
        $pairs_trade = explode('-', $pattern->pattern);
        if ($pair_base === $pairs_trade[$index]) {
            $dir_index = 1;
        } else {
            $dir_index = 2;
        }
        foreach ($prices as $price_index => $price) {
            if ($price[$dir_index] < $min_volume) {
                continue;
            }
            if ($price[$dir_index] >= $min_volume) {
                return $price_index;
            }
            return -1;
        }
        return -1;
    }
}
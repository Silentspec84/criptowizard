<?php


namespace app\classes\strategies;


interface StrategyInterface
{
    public function calculate();
}
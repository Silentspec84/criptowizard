<?php

namespace app\classes\strategies;

class BaseStrategy
{
    public const BUY = 1;
    public const SELL = 0;
}
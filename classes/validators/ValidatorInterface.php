<?php

namespace app\classes\validators;

interface ValidatorInterface {
    public function validate(array $config, array $data);
}
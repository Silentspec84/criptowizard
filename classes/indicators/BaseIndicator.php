<?php

namespace app\classes\indicators;

class BaseIndicator
{
    public const MODE_MAIN = 'MAIN';
    public const MODE_UPPER = 'UPPER';
    public const MODE_LOWER = 'LOWER';
    public const MODE_PLUS_DI = 'PLUS_DI';
    public const MODE_MINUS_DI = 'MINUS_DI';

    public const MODE_SMA = 'SMA';
    public const MODE_EMA = 'EMA';
    public const MODE_SMMA = 'SMMA';
    public const MODE_LWMA = 'LWMA';

    public const MODE_GATORJAW = 'GATORJAW';
    public const MODE_GATORTEETH = 'GATORTEETH';
    public const MODE_GATORLIPS = 'GATORLIPS';

    public const MODE_TENKANSEN = 'TENKANSEN';
    public const MODE_KIJUNSEN = 'KIJUNSEN';
    public const MODE_SENKOUSPANA = 'SENKOUSPANA';
    public const MODE_SENKOUSPANB = 'SENKOUSPANB';
    public const MODE_CHIKOUSPAN = 'CHIKOUSPAN';

    public $ma_methods = [
        ['sma', self::MODE_SMA],
        ['ema', self::MODE_EMA],
        ['smma', self::MODE_SMMA],
        ['lwma', self::MODE_LWMA],
    ];
}
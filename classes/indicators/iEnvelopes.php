<?php

namespace app\classes\indicators;

class iEnvelopes extends BaseIndicator
{
    public $env_modes = [
        ['main', self::MODE_MAIN],
        ['upper', self::MODE_UPPER],
        ['lower', self::MODE_LOWER]
    ];

    private $symbol;        // Инструмент, будь то EURUSD, BTCETH или AFLT

    private $timeframe;     // Период

    private $period;        // Период усреднения

    private $data_type;     // Тип ценовых данных (OHLC)

    private $shift;         // Сдвиг

    private $deviation;     // Отклонение в % от цены

    private $env_mode;       // Индекс линии ($env_modes)

    private $ma_method;      //  Метод усреднения ($ma_methods)

    public function __construct()
    {

    }
}
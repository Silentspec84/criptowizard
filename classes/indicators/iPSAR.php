<?php

namespace app\classes\indicators;

class iPSAR extends BaseIndicator
{

    private $symbol;        // Инструмент, будь то EURUSD, BTCETH или AFLT

    private $timeframe;     // Период

    private $data_type;     // Тип ценовых данных (OHLC)

    private $step;          // Шаг изменения цены

    private $max_step;        // Максимальный шаг

    private $shift;         // Сдвиг

    public function __construct()
    {

    }
}
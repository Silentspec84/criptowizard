<?php

namespace app\classes\indicators;

// Average Directional Movement Index

class iADX extends BaseIndicator
{
    public $adx_modes = [
        ['main', self::MODE_MAIN],
        ['+DI', self::MODE_PLUS_DI],
        ['-DI', self::MODE_MINUS_DI]
    ];

    private $symbol;        // Инструмент, будь то EURUSD, BTCETH или AFLT

    private $timeframe;     // Период

    private $period;        // Период усреднения

    private $data_type;     // Тип ценовых данных (OHLC)

    private $adx_mode;       // Индекс линии ($adx_modes)

    private $shift;         // Сдвиг

    public function __construct()
    {

    }
}
<?php

namespace app\classes\indicators;

class iMA extends BaseIndicator
{

    private $symbol;        // Инструмент, будь то EURUSD, BTCETH или AFLT

    private $timeframe;     // Период

    private $data_type;     // Тип ценовых данных (OHLC)

    private $period;        // Период усреднения

    private $ma_method;      //  Метод усреднения ($ma_methods)

    private $shift;         // Сдвиг

    public function __construct()
    {

    }
}
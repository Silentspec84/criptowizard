<?php

namespace app\classes\indicators;

class iIchimoku extends BaseIndicator
{
    public $gator_modes = [
        ['Tenkan-sen', self::MODE_TENKANSEN],
        ['Kijun-sen', self::MODE_KIJUNSEN],
        ['Senkou Span A', self::MODE_SENKOUSPANA],
        ['Senkou Span B', self::MODE_SENKOUSPANB],
        ['Chikou Span', self::MODE_CHIKOUSPAN],
    ];

    private $symbol;        // Инструмент, будь то EURUSD, BTCETH или AFLT

    private $timeframe;     // Период

    private $data_type;     // Тип ценовых данных (OHLC)

    private $period_ts;        // Период усреднения Tenkan Sen

    private $period_ks;        // Период усреднения Kijun Sen

    private $period_ss;        // Период усреднения Senkou Span B

    private $ichi_mode;       // Индекс линии ($ichi_modes)

    private $shift;         // Сдвиг

    public function __construct()
    {

    }
}
<?php

namespace app\classes\indicators;

class iAlligator extends BaseIndicator
{
    public $gator_modes = [
        ['GatorJaws', self::MODE_GATORJAW],
        ['GatorTeeth', self::MODE_GATORTEETH],
        ['GatorLips', self::MODE_GATORLIPS]
    ];

    private $symbol;        // Инструмент, будь то EURUSD, BTCETH или AFLT

    private $timeframe;     // Период

    private $gator_mode;       // Индекс линии ($gator_modes)

    private $data_type;     // Тип ценовых данных (OHLC)

    private $period_jaws;        // Период усреднения челюстей

    private $shift_jaws;         // Сдвиг челюстей

    private $period_teeth;        // Период усреднения зубов

    private $shift_teeth;         // Сдвиг зубов

    private $period_lips;        // Период усреднения губ

    private $shift_lips;         // Сдвиг губ

    private $ma_method;      //  Метод усреднения ($ma_methods)

    public function __construct()
    {

    }
}
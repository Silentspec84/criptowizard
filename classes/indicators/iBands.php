<?php

namespace app\classes\indicators;

// Bollinger Bands

class iBands extends BaseIndicator
{
    public $bb_modes = [
        ['main', self::MODE_MAIN],
        ['upper', self::MODE_UPPER],
        ['lower', self::MODE_LOWER]
    ];

    private $symbol;        // Инструмент, будь то EURUSD, BTCETH или AFLT

    private $timeframe;     // Период

    private $bb_mode;       // Индекс линии ($bb_modes)

    private $data_type;     // Тип ценовых данных (OHLC)

    private $period;        // Период усреднения

    private $up_std_dev;    // Стандартное отклонение верхнего канала

    private $dn_std_dev;    // Стандартное отклонение нижнего канала

    private $shift;         // Сдвиг

    public function __construct()
    {

    }
}
sudo apt-get update
    sudo apt-get install -y software-properties-common curl
    sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
    sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
    sudo apt-get update
    sudo apt-get install -y docker-engine
    sudo apt-get install -y docker-compose
    sudo mkdir /var/data
    cd /var/www/criptowizard/
    sudo docker-compose -f docker-compose.yml up -d
    echo "Waiting..."
    sleep 20
    sudo docker exec anyport_php_1 cd /app && composer install
    sudo docker exec anyport_mysql_1 mysql -u root -p123123 -e "CREATE DATABASE cripto;"
    sudo docker exec anyport_php_1 php /app/yii migrate/up --interactive=0


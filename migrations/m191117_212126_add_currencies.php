<?php

use app\models\Daemon;
use app\models\Exchange;
use app\models\TopCurrency;
use yii\db\Migration;

/**
 * m191117_212126_add_currencies
 */
class m191117_212126_add_currencies extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $currencies = ['BTC', 'ETH', 'XRP', 'BCH', 'USDT', 'LTC', 'EOS', 'BNB', 'BSV', 'XLM',
            'TRX', 'ADA', 'XMR', 'XMR', 'LEO', 'HT', 'NEO', 'XTZ', 'ATOM', 'MIOTA', 'MKR', 'DASH',
            'ETC', 'ONT', 'USDC', 'CRO', 'VET', 'XEM', 'BAT', 'DOGE', 'ZEC', 'DCR', 'PAX', 'QTUM',
            'HEDG', 'CENNZ', 'ZRX', 'TUSD', 'HOT', 'SNX', 'OMG', 'VSYS', 'BTG', 'RVN', 'ZB', 'REP',
            'NANO', 'ABBC', 'ALGO', 'KMD', 'EKT', 'LUNA', 'DAI', 'BTM', 'KCS', 'LSK', 'BCD', 'BTT',
            'DGB', 'SLV', 'SEELE', 'ICX', 'SC', 'SXP', 'IOST', 'THETA', 'FTT', 'WAVES', 'HC',
            'XVG', 'DX', 'QNT', 'BTS', 'MONA', 'BCN', 'MCO', 'RLC', 'AE', 'NEXO', 'MAID', 'ZIL',
            'BTMX', 'ARDR', 'STEEM', 'AOA', 'CHZ', 'NRG', 'RIF', 'ELF', 'REN', 'SNT', 'GNT', 'UNI',
            'ZEN', 'ILC', 'NEW', 'NPXS', 'GRIN', 'XZC'];

        foreach ($currencies as $currency) {
            $top_currency = new TopCurrency();
            $top_currency->name = $currency;
            $top_currency->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
<?php

use app\models\User;
use yii\db\Migration;

/**
 * m190919_084756_add_admin_to_users_table
 */
class m190919_084756_add_admin_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $admin = new User();
        $admin->email = 'silentspec84@mail.ru';
        $admin->login_name = 'silentspec';
        $admin->password = 'bedad78fc92f6b8c4a27d4bfbbe254dd'; //пароль - password
        $admin->status = User::STATUS_ACTIVE;
        $admin->role = User::ROLE_ADMIN;
        $admin->hash = 'bedad78fc92f6b8c4a27d4bfbbe254dd';
        $admin->avatar = '../web/images/avatar/ava.jpg';

        $admin->save();

        $admin = new User();
        $admin->email = 'asterio.me@gmail.com';
        $admin->login_name = 'asterio';
        $admin->password = 'bedad78fc92f6b8c4a27d4bfbbe254dd'; //пароль - password
        $admin->status = User::STATUS_ACTIVE;
        $admin->role = User::ROLE_ADMIN;
        $admin->hash = 'bedad78fc92f6b8c4a27d4bfbbe254dd';
        $admin->avatar = '../web/images/avatar/ava.jpg';

        $admin->save();
    }



    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
<?php

use yii\db\Migration;

/**
 * m191109_165102_create_table_currencies
 */
class m191111_224004_create_table_logs extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('logs', [
            'id' => $this->primaryKey(),
            'exchange_id' => $this->integer()->notNull(),
            'procedure' => $this->string(255),
            'message' => $this->text(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('logs_exchange_id', 'logs', 'exchange_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('logs');
    }
}
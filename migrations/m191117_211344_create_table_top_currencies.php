<?php

use yii\db\Migration;

/**
 * m191117_211344_create_table_top_currencies
 */
class m191117_211344_create_table_top_currencies extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('top_currencies', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('top_currencies');
    }
}
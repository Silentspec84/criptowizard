<?php

use app\models\Daemon;
use app\models\Exchange;
use yii\db\Migration;

/**
 * m191030_113220_add_daemons_to_daemons_table
 */
class m191030_113220_add_daemons_to_daemons_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $exchanges = Exchange::find()->all();
        /** @var Exchange $exchange */
        foreach ($exchanges as $exchange) {
            $daemon = new Daemon();
            $daemon->exchange_id = $exchange->id;
            $daemon->command = 'daemon-trade/run';
            $daemon->strategy = 'app\classes\strategies\StrategyArbitrageIn';
            $daemon->job_type = Daemon::TYPE_TEST;
            $daemon->enabled = 0;
            $daemon->timer = 15;
            $daemon->log = '';
            $daemon->date_modified = time();
            $daemon->date_created = time();
            $daemon->date_last_connected = time();
            $daemon->save();

            $daemon = new Daemon();
            $daemon->exchange_id = $exchange->id;
            $daemon->command = 'daemon-trade/run';
            $daemon->strategy = 'app\classes\strategies\StrategyArbitrageIn';
            $daemon->job_type = Daemon::TYPE_TRADE;
            $daemon->enabled = 0;
            $daemon->timer = 15;
            $daemon->log = '';
            $daemon->date_modified = time();
            $daemon->date_created = time();
            $daemon->date_last_connected = time();
            $daemon->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
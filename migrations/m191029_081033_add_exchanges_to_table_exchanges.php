<?php

use app\models\Exchange;
use yii\db\Migration;

/**
 * m191029_081033_add_exchanges_to_table_exchanges
 */
class m191029_081033_add_exchanges_to_table_exchanges extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $exmo = new Exchange();
        $exmo->name = 'exmo';
        $exmo->class = 'app\classes\connectors\ExmoApiConnector';
        $exmo->descr = '';
        $exmo->img = '';
        $exmo->link = 'https://exmo.me/';
        $exmo->commission = 0.2;
        $exmo->save();

        $binance = new Exchange();
        $binance->name = 'Binance';
        $binance->class = 'app\classes\connectors\BinanceApiConnector';
        $binance->descr = '';
        $binance->img = '';
        $binance->link = 'https://www.binance.com';
        $binance->commission = 0.1;
        $binance->save();

        $yobit = new Exchange();
        $yobit->name = 'Yobit';
        $yobit->class = 'app\classes\connectors\YobitApiConnector';
        $yobit->descr = '';
        $yobit->img = '';
        $yobit->link = 'https://yobit.net';
        $yobit->commission = 0.2;
        $yobit->save();

        $poloniex = new Exchange();
        $poloniex->name = 'Poloniex';
        $poloniex->class = 'app\classes\connectors\PoloniexApiConnector';
        $poloniex->descr = '';
        $poloniex->img = '';
        $poloniex->link = 'https://poloniex.com/';
        $poloniex->commission = 0.25;
        $poloniex->save();

        $bittrex = new Exchange();
        $bittrex->name = 'Bittrex';
        $bittrex->class = 'app\classes\connectors\BittrexApiConnector';
        $bittrex->descr = '';
        $bittrex->img = '';
        $bittrex->link = 'https://international.bittrex.com/';
        $bittrex->commission = 0.25;
        $bittrex->save();

        $hitbtc = new Exchange();
        $hitbtc->name = 'HitBTC';
        $hitbtc->class = 'app\classes\connectors\HitbtcApiConnector';
        $hitbtc->descr = '';
        $hitbtc->img = '';
        $hitbtc->link = 'https://hitbtc.com/';
        $hitbtc->commission = 0.1;
        $hitbtc->save();

        $hotbit = new Exchange();
        $hotbit->name = 'Hotbit';
        $hotbit->class = 'app\classes\connectors\HotbitApiConnector';
        $hotbit->descr = '';
        $hotbit->img = '';
        $hotbit->link = 'https://www.hotbit.io/';
        $hotbit->commission = 0.1;
        $hotbit->save();

        $huobi = new Exchange();
        $huobi->name = 'Huobi Global';
        $huobi->class = 'app\classes\connectors\HuobiApiConnector';
        $huobi->descr = '';
        $huobi->img = '';
        $huobi->link = 'https://www.hbg.com/';
        $huobi->commission = 0.2;
        $huobi->save();

        $okex = new Exchange();
        $okex->name = 'OKex';
        $okex->class = 'app\classes\connectors\OkexApiConnector';
        $okex->descr = '';
        $okex->img = '';
        $okex->link = 'https://www.okex.com';
        $okex->commission = 0.2;
        $okex->save();

        $kucoin = new Exchange();
        $kucoin->name = 'KuCoin';
        $kucoin->class = 'app\classes\connectors\KucoinApiConnector';
        $kucoin->descr = '';
        $kucoin->img = '';
        $kucoin->link = 'https://www.kucoin.com/';
        $kucoin->commission = 0.1;
        $kucoin->save();

        $bitfinex = new Exchange();
        $bitfinex->name = 'Bitfinex';
        $bitfinex->class = 'app\classes\connectors\BitfinexApiConnector';
        $bitfinex->descr = '';
        $bitfinex->img = '';
        $bitfinex->link = 'https://www.bitfinex.com';
        $bitfinex->commission = 0.2;
        $bitfinex->save();
    }



    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
<?php

use yii\db\Migration;

/**
 * m191109_165102_create_table_currencies
 */
class m191109_165102_create_table_currencies extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('currencies', [
            'id' => $this->primaryKey(),
            'exchange_id' => $this->integer()->notNull(),
            'currency' => $this->text(),
            'min_volume' => $this->float(),
            'deposit' => $this->float(),
            'status' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
            'date_last_connected' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('currencies_exchange_id', 'currencies', 'exchange_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('currencies');
    }
}
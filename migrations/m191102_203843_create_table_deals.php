<?php

use yii\db\Migration;

/**
 * m191102_203843_create_table_deals
 */
class m191102_203843_create_table_deals extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('deals', [
            'id' => $this->primaryKey(),
            'hash' => $this->string(255),
            'exchange_id' => $this->integer(),
            'strategy' => $this->string(255),
            'type' => $this->integer(),
            'deal' => $this->text(),
            'pattern' => $this->text(),
            'profit_perc' => $this->float(),
            'profit' => $this->float(),
            'log' => $this->text(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('deals_hash', 'deals', 'hash');
        $this->createIndex('deals_type', 'deals', 'type');
        $this->createIndex('deals_strategy', 'deals', 'strategy');
        $this->createIndex('deals_exchange_id', 'deals', 'exchange_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('deals');
    }
}
<?php

use yii\db\Migration;

/**
 * m191104_134030_create_table_pair_chains
 */
class m191104_134030_create_table_pair_chains extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('pair_chains', [
            'id' => $this->primaryKey(),
            'exchange_id' => $this->integer()->notNull(),
            'type' => $this->integer(),
            'pattern' => $this->text(),
            'base' => $this->text(),
            'dir' => $this->text(),
            'first_currency' => $this->string(255),
            'status' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('pair_chains_status', 'pair_chains', 'status');
        $this->createIndex('pair_chains_type', 'pair_chains', 'type');
        $this->createIndex('pair_chains_first_currency', 'pair_chains', 'first_currency');
        $this->createIndex('pair_chains_exchange_id', 'pair_chains', 'exchange_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('pair_chains');
    }
}
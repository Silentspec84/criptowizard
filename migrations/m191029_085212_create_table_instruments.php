<?php

use yii\db\Migration;

/**
 * m191029_085212_create_table_instruments
 */

class m191029_085212_create_table_instruments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('instruments', [
            'id' => $this->primaryKey(),
            'exchange_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'status' => $this->integer(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('instrument_name', 'instruments', 'name');
        $this->createIndex('instrument_exchange_id', 'instruments', 'exchange_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('instruments');
    }
}
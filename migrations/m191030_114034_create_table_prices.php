<?php

use yii\db\Migration;

/**
 * m191030_114034_create_table_prices
 */
class m191030_114034_create_table_prices extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('prices', [
            'id' => $this->primaryKey(),
            'instrument_id' => $this->integer()->notNull(),
            'ask' => $this->text(),
            'bid' => $this->text(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('instrument_id', 'prices', 'instrument_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('prices');
    }
}
<?php

use yii\db\Migration;

/**
 * m191030_113030_create_daemons_table
 */
class m191030_113030_create_daemons_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('daemons', [
            'id' => $this->primaryKey(),
            'exchange_id' => $this->integer()->notNull(),
            'command' => $this->string(255),
            'strategy' => $this->string(255),
            'job_type' => $this->integer()->notNull(),
            'enabled' => $this->integer(),
            'timer' => $this->integer(),
            'log' => $this->text(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
            'date_last_connected' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('daemons_exchange_id', 'daemons', 'exchange_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('daemons');
    }
}
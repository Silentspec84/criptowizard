<?php

use yii\db\Migration;

/**
 * m191029_075503_create_table_exchanges
 */
class m191029_075503_create_table_exchanges extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        $this->createTable('exchanges', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->unique()->notNull(),
            'class' => $this->string(255)->unique()->notNull(),
            'descr' => $this->text(),
            'img' => $this->string(255),
            'link' => $this->string(255),
            'commission' => $this->float(),
            'date_created' => $this->integer(),
            'date_modified' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('exchange_name', 'exchanges', 'name');
        $this->createIndex('exchange_class', 'exchanges', 'class');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('exchanges');
    }
}